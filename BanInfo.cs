﻿using System;
using System.Text;
using System.Collections.Generic;
using Chipz.IRC.Collections;

namespace Chipz.IRC
{
	/// <summary>
	/// Ban information
	/// </summary>
	public class BanInfo
	{
	    /// <summary>
	    /// BanMask. (nick!user@host)
	    /// </summary>
	    public string BanMask
	    {
	        get { return m_banMask; }
	    }
	    string m_banMask;
	
	    /// <summary>
	    /// The nickname of the user that created this ban.
	    /// </summary>
	    public string SetBy
	    {
	        get { return m_setBy; }
	    }
	    string m_setBy;
	
	    /// <summary>
	    /// Date when this ban was set.
	    /// </summary>
	    public DateTime TimeStamp
	    {
	        get { return m_timeStamp; }
	    }
	    DateTime m_timeStamp;
	
	    /// <summary>
	    /// Unix timestamp.
	    /// </summary>
	    public long UnixTimeStamp
	    {
	        get { return m_unixTimeStamp; }
	    }
	    long m_unixTimeStamp;
	
	    /// <summary>
	    /// Creates a new instance of the BanInfo class.
	    /// </summary>
	    /// <param name="ban">Ban string (banmask setby timestamp).</param>
	    public BanInfo(string ban)
	    {
	        string[] parts = new string[3] { "", "", "0" };
	        ban.Split(new char[] { ' ' }, 3).CopyTo(parts, 0);
	
	        m_banMask = parts[0];
	        m_setBy = parts[1];
	        m_unixTimeStamp = 0;
	
	        long.TryParse(parts[2], out m_unixTimeStamp);
	
	        m_timeStamp = Helper.UnixTimeToDateTime(m_unixTimeStamp);
	    }
	
	    /// <summary>
	    /// Creates a new instance of the BanInfo class.
	    /// </summary>
	    /// <param name="banMask">Banmask.</param>
	    /// <param name="setBy">User who set this ban.</param>
	    /// <param name="timeStamp">DateTime.</param>
	    public BanInfo(string banMask, string setBy, DateTime timeStamp)
	    {
	        m_banMask = banMask;
	        m_setBy = setBy;
	        m_timeStamp = timeStamp;
	        m_unixTimeStamp = Helper.DateTimeToUnixTime(timeStamp);
	    }
	    
	    /// <summary>
	    /// Creates a new instance of the BanInfo class.
	    /// </summary>
	    /// <param name="banMask">Banmask.</param>
	    /// <param name="setBy">User who set this ban.</param>
	    /// <param name="unixTimeStamp">UNIT timestamp.</param>
	    public BanInfo(string banMask, string setBy, long unixTimeStamp)
	    {
	        m_banMask = banMask;
	        m_setBy = setBy;
	        m_timeStamp = Helper.UnixTimeToDateTime(unixTimeStamp);
	        m_unixTimeStamp = unixTimeStamp;
	    }
	}
}
