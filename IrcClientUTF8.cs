using System;
using System.Text;
using Chipz.IRC;

namespace ChipzIRC
{
    /// <summary>
    /// IrcClientEx
    /// 
    /// This IrcClient will decode UTF-8 encoded text.
    /// UTF-8 formatted text can be sent through 8-bit encodings like ISO-8859.
    /// ASCII usually destroys the UTF-8 formatting (7-bit, all values > 127 is nulled).
    ///
    /// Tip: You can override GetEncoder() to send UTF-8 formatted text.
    /// 
    /// Example:
    ///         protected override Encoding GetEncoder(string target)
    ///         {
    ///             IrcChannel chan = GetChannel(target);
    ///             if (chan != null)
    ///                 return chan.Tag as Encoding;
    ///             return base.GetEncoder(target);
    ///         }
    /// 
    /// TODO: Fix this?
    /// </summary>
    public class IrcClientUTF8 : Chipz.IRC.IrcClient
    {
        /// <summary>
        /// Sets or gets UTF-8 auto-detection.
        /// </summary>
        public virtual bool AutoDetectUTF8
        {
            get { return m_autoDetectUTF8; }
            set { m_autoDetectUTF8 = value; }
        }
        bool m_autoDetectUTF8 = true;

        /// <summary>
        /// Returns the Encoder that will encode the message.
        /// </summary>
        /// <param name="target">Target; channel-name or nickname.</param>
        /// <returns>An Encoding or null if default should be used.</returns>
        protected virtual Encoding GetEncoder(string target)
        {
            return null;
        }

        /// <summary>
        /// Returns the decoder.
        /// </summary>
        /// <param name="target">Target; channel-name or nickname.</param>
        /// <returns>An Encoding or null if default should be used.</returns>
        protected virtual Encoding GetDecoder(string target)
        {
            return null;
        }

        // Override incoming message-parser.
        protected override MessageData ParseMessage(string message)
        {
            MessageData data = base.ParseMessage(message);

            if (data.Type != Chipz.IRC.MessageType.Unknown || data.ReplyCode == ReplyCode.Topic)
            {
                if (data.Message != null && data.Message.Length > 0 && data.Params.Length > 0)
                {
                    string target = data.Channel != null ? data.Channel : data.From;
                    if (target != null && target.Length > 0)
                        data.Params[data.Params.Length - 1] = DecodeMessage(data.Params[data.Params.Length - 1], target);
                }
            }

            return data;
        }

		// FIXME!
        // Override all methods that are dealing with messages.
        /*protected override void OnLineWrite(WriteLineEventArgs e)
        {
        	var line = e.Line;
        	var index = line.IndexOf(':');
        	
        	if(index > -1 && index + 1 < line.Length)
        	{
        		e.Line = line.Substring(0, index + 1) + EncodeMessage(guesstarget... meh);
        	}
        	
        	base.OnWriteLine(e);
        }*/
        
        // TODO: This (or using UTF8 as stream encoding), didn't work with many servers a while back... maybe now?
        protected override void OnLineWrite(WriteLineEventArgs e)
        {
        	e.Line = EncodeMessage(e.Line, null);
        }
        
        protected override MessageData CreateFakeMessage(string target, string message)
        {
            return base.CreateFakeMessage(target, DecodeMessage(message, target));
        }

        /*public override void SendMessage(string target, string message, bool silent)
        {
            base.SendMessage(target, EncodeMessage(message, target), silent);
        }

        public override void SendNotice(string target, string message, bool silent)
        {
            base.SendNotice(target, EncodeMessage(message, target), silent);
        }

        public override void SendAction(string target, string action, bool silent)
        {
            base.SendAction(target, EncodeMessage(action, target), silent);
        }

        public override void Kick(string channel, string user, string comment)
        {
            base.Kick(channel, user, EncodeMessage(comment, channel));
        }

        public override void Kick(string[] channels, string[] users, string comment)
        {
            base.Kick(channels, users, EncodeMessage(comment, ""));     //FIXME?
        }

        public override void SetTopic(string channel, string topic)
        {
            base.SetTopic(channel, EncodeMessage(topic, channel));
        }*/

        /// <summary>
        /// Encodes a message.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public string EncodeMessage(string text, string target)
        {
            Encoding encoder = GetEncoder(target);

            if (encoder == null || encoder == Encoding)
                return text;
            
            byte[] bytes = encoder.GetBytes(text);
            return Encoding.GetString(bytes);
        }

        /// <summary>
        /// Decodes a message.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public string DecodeMessage(string text, string target)
        {
            if (Encoding == null)
                return text;

            Encoding decoder = GetDecoder(target);

            byte[] bytes = null;

            if (AutoDetectUTF8 && decoder != Encoding.UTF8)
            {
                bytes = Encoding.GetBytes(text);
                if (IsUTF8(bytes))
                    return Encoding.UTF8.GetString(bytes);
            }

            if (decoder != null)// && decoder != Encoding) -- FIXME?
            {
                if (bytes == null)
                    bytes = Encoding.GetBytes(text);
                return decoder.GetString(bytes);
            }
            return text;
        }

        /// <summary>
        /// IsUTF8.
        /// </summary>
        /// <param name="bytes">Bytes to check.</param>
        /// <returns>True if bytes is UTF-8 formatted.</returns>
        public static bool IsUTF8(byte[] bytes)
        {
            int good = 0;
            int bad = 0;

            for (int i = 1; i < bytes.Length; ++i)
            {
                byte current = bytes[i];
                byte previous = bytes[i - 1];

                if ((current & 0xC0) == 0x80)
                {
                    if ((previous & 0xC0) == 0xC0)
                        good++;
                    else if ((previous & 0x80) == 0x00)
                        bad++;
                }
                else if ((previous & 0xC0) == 0xC0)
                    bad++;
            }

            return good > bad;
        }
    }
}
