using System;
using System.Collections.Generic;
using System.Text;

namespace Chipz.IRC
{
    /// <summary>
    /// Helper class.
    /// </summary>
    public static class Helper
    {
        /// <summary>
        /// Converts a DateTime object to a UNIX timestamp.
        /// </summary>
        /// <param name="date">DateTime.</param>
        /// <returns>A UNIX timestamp.</returns>
        public static long DateTimeToUnixTime(DateTime date)
        {
            DateTime timeBase = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            TimeSpan span = date.ToUniversalTime() - timeBase;
            return (long)span.TotalSeconds;
        }

        /// <summary>
        /// Converts a UNIX timestamp to a DateTime object.
        /// </summary>
        /// <param name="timeStamp">UNIX timestamp.</param>
        /// <returns>DateTime.</returns>
        public static DateTime UnixTimeToDateTime(long timeStamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0);
            dateTime = dateTime.AddSeconds(timeStamp);
            return dateTime;
        }
    }
}
