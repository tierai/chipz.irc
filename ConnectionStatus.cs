﻿using System;

namespace Chipz.IRC
{
	/// <summary>
	/// Connection Status.
	/// </summary>
	public enum ConnectionStatus
	{
	    /// <summary>
	    /// Disconnected.
	    /// </summary>
	    Disconnected,
	
	    /// <summary>
	    /// Disconnecting.
	    /// </summary>
	    Disconnecting,
	
	    /// <summary>
	    /// Connecting.
	    /// </summary>
	    Connecting,
	    
	    /// <summary>
	    /// Connected.
	    /// </summary>
	    Connected,
	}
}
