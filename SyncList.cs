using System;
using System.Collections;
using System.Collections.Generic;

namespace Chipz.IRC.Collections
{
    /// <summary>
    /// SyncList.
    /// </summary>
    /// <typeparam name="T">List item type.</typeparam>
    public class SyncList<T> : SyncCollection<T>, IList<T>
    {
        List<T> list
        {
            get { return (List<T>)base.collection; }
        }

        /// <summary>
        /// Creates a new instance of the SyncList class.
        /// </summary>
        /// <param name="list">List object.</param>
        public SyncList(List<T> list)
            : base(list)
        {
        }

        /// <summary>
        /// Converts this list to an array.
        /// </summary>
        /// <returns>An array of the items in this list.</returns>
        public T[] ToArray()
        {
            lock (SyncRoot)
                return list.ToArray();
        }

        #region IList<T> Members

        /// <summary>
        /// Return an index of an item.
        /// </summary>
        /// <param name="item">Item to find.</param>
        /// <returns>Index of the item.</returns>
        public int IndexOf(T item)
        {
            lock (SyncRoot)
                return list.IndexOf(item);
        }
        
        /// <summary>
        /// Inserts an item into the list.
        /// </summary>
        /// <param name="index">Insert index.</param>
        /// <param name="item">Item to insert.</param>
        public void Insert(int index, T item)
        {
            lock (SyncRoot)
                list.Insert(index, item);
        }

        /// <summary>
        /// Removes an item from the list.
        /// </summary>
        /// <param name="index">Item index.</param>
        public void RemoveAt(int index)
        {
            lock (SyncRoot)
                list.RemoveAt(index);
        }

        /// <summary>
        /// Sets or gets an item in the list.
        /// </summary>
        /// <param name="index">Index.</param>
        /// <returns>Item a index.</returns>
        public T this[int index]
        {
            get
            {
                lock (SyncRoot)
                    return list[index];
            }
            set
            {
                lock (SyncRoot)
                    list[index] = value;
            }
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An System.Collections.IEnumerator object that can be used to iterate through the collection.</returns>
        public IEnumerator<T> GetEnumerator()
        {
            lock (SyncRoot)
                return new SyncEnumerator<T>(this, list.GetEnumerator());
        }

        #endregion

        #region IEnumerable Members

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (SyncRoot)
                return (IEnumerator)GetEnumerator();
        }

        #endregion
    }
}
