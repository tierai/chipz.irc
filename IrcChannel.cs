using System;
using System.Text;
using System.Collections.Generic;
using Chipz.IRC.Collections;

namespace Chipz.IRC
{
    /// <summary>
    /// Summary description for Channel.
    /// </summary>
    public sealed class IrcChannel
    {
        #region Properties

        /// <summary>
        /// Users that are in this channel.
        /// </summary>
        public IrcChannelUser[] Users
        {
            get
            {
            	lock(SyncRoot)
            	{
            		IrcChannelUser[] result = new IrcChannelUser[m_users.Count];
            		m_users.Values.CopyTo(result, 0);
            		return result;
            	}
            }
        }
        
        public int NumUsers
        {
        	get
        	{
        		lock(SyncRoot)
        		{
        			return m_users.Count;
        		}
        	}
        }

        /// <summary>
        /// The name of this channel.
        /// </summary>
        public string Name
        {
            get { return m_name; }
        }
        string m_name;

        /// <summary>
        /// IrcClient.
        /// </summary>
        public IrcClient IrcClient
        {
            get { return m_ircClient; }
        }
        IrcClient m_ircClient;
        
        public object SyncRoot
        {
        	get { return IrcClient.SyncRoot; }
        }

        /// <summary>
        /// Bans in this channel.
        /// </summary>
        public BanInfo[] Bans
        {
            get
            {
                lock(SyncRoot)
                {
                    BanInfo[] result = new BanInfo[m_bans.Values.Count];
                    m_bans.Values.CopyTo(result, 0);
                    return result;
                }
            }
        }
        
        
        public bool IsUpdatingBanList
        {
        	get { return m_isUpdatingBanList; }
        	internal set { m_isUpdatingBanList = value; }
        }
        bool m_isUpdatingBanList = false;

        /// <summary>
        /// True if channel is synced.
        /// </summary>
        public bool IsSynced
        {
            get { return m_isSynced; }
            internal set { m_isSynced = value; }
        }
        bool m_isSynced = false;

        /// <summary>
        /// Channel mode as a string.
        /// </summary>
        public string ChannelMode
        {
            get
            {
                string modes = "+";
                string param = "";

                lock(SyncRoot)
                {
                    foreach(KeyValuePair<char, string> kv in m_modes)
                    {
                        modes += kv.Key;
                        if(kv.Value.Length > 0)
                            param += " " + kv.Value;
                    }
                }
                return modes + param;
            }
        }

        /// <summary>
        /// Channel topic.
        /// </summary>
        public string Topic
        {
            get { return m_topic; }
            internal set { m_topic = value; }
        }
        string m_topic = string.Empty;

        #endregion //Properties

        #region Variables
        
       	Dictionary<char, string> m_modes = new Dictionary<char, string>();
       	Dictionary<string, IrcChannelUser> m_users = new Dictionary<string, IrcChannelUser>(StringComparer.InvariantCultureIgnoreCase);
        Dictionary<string, BanInfo> m_bans = new Dictionary<string, BanInfo>(StringComparer.InvariantCultureIgnoreCase);

        #endregion //Variables

        internal IrcChannel(IrcClient client, string name)
        {
            m_ircClient = client;
            m_name = name;
        //    m_client.Rfc.Mode(name);         // Get channel modes
        //    m_client.Rfc.Mode(name, "+b");   // Get channel banlist
        }

        /// <summary>
        /// Get an IrcChannelUser.
        /// </summary>
        /// <param name="nick">Nickname.</param>
        /// <returns>Null if no user is found.</returns>
        public IrcChannelUser GetUser(string nick)
        {
            lock(SyncRoot)
            {
            	IrcChannelUser result = null;
            	m_users.TryGetValue(nick, out result);
            	return result;
            }
        }

        /// <summary>
        /// Checks if a banmask is set.
        /// </summary>
        /// <param name="banMask">Banmask.</param>
        /// <returns>True if banmask is set.</returns>
        public bool IsBanMaskSet(string banMask)
        {
            lock(SyncRoot)
                return m_bans.ContainsKey(banMask);
        }

        /// <summary>
        /// GetMode.
        /// </summary>
        /// <param name="mode">Mode char.</param>
        /// <returns>True if mode is set.</returns>
        public bool GetMode(char mode)
        {
            lock(SyncRoot)
            	return m_modes.ContainsKey(mode);
        }

        /// <summary>
        /// Get a mode-value.
        /// </summary>
        /// <param name="mode">Mode char.</param>
        /// <param name="param">[out] Result.</param>
        /// <returns>True if mode exists.</returns>
        public bool GetMode(char mode, ref string param)
        {
            lock(SyncRoot)
            {
            	return m_modes.TryGetValue(mode, out param);
            }
        }

        /// <summary>
        /// Resync the channel.
        /// Should only be called externally if the channel is messed up.
        /// </summary>
        public void Resync()
        {
        	lock(SyncRoot)
        	{
	            m_isSynced = false;
	            m_ircClient.Rfc.Names(Name);        // Names
	            m_ircClient.Rfc.Mode(Name);         // Get channel modes
	            m_ircClient.Rfc.Mode(Name, "+b");   // Get channel banlist
	    	}
        }

        internal void SetMode(string mode)
        {
            m_modes.Clear();
            
            if(mode.Length < 1)
                return;

            bool add = false;
            string[] tmp = mode.Split(new char[] { ' ' });

            if (tmp.Length < 1)
                return;

            int paramIndex = 1;
            
            for (int i = 0; i < tmp[0].Length; ++i)
            {
                char c = mode[i];

                if (c == '+')
                    add = true;
                else if (c == '-')
                    add = false;
                else
                {
                    if (add)
                    {
                        if (c == 'k' || c == 'l')
                        {
                            if (paramIndex >= tmp.Length)
                                throw new Exception("Invalid mode, param expected for mode '" + c + "'");

                            m_modes[c] = tmp[paramIndex];
                            paramIndex++;
                        }
                        else
                        {
                            m_modes[c] = string.Empty;
                        }
                    }
                    else if (m_modes.ContainsKey(c))
                    {
                        m_modes.Remove(c);
                    }
                }
            }
        }

        internal void AddMode(char mode, string param)
        {
            m_modes[mode] = param;
        }

        internal void RemoveMode(char mode)
        {
            if(m_modes.ContainsKey(mode))
                m_modes.Remove(mode);
        }

        internal void AddUser(IrcUser user, string mode)
        {
            if(m_users.ContainsKey(user.Nick))
            	throw new InvalidOperationException("User already exists in this channel");

        	m_users[user.Nick] = new IrcChannelUser(this, user, mode);
        	user.AddChannel(this);
        }

        internal void RemoveUser(string nick)
        {
			IrcChannelUser user = GetUser(nick);
			
            if(user != null)
            {
                user.IrcUser.RemoveChannel(this);
                m_users.Remove(nick);
            }
        }
        
        internal void RenameUser(string oldNick, string newNick)
        {
        	if(m_users.ContainsKey(oldNick))
        	{
        		m_users[newNick] = m_users[oldNick];
        		m_users.Remove(oldNick);
        	}
        	else
        	{
        		//FIXME: This occurs if a user changes nick directly after joining a channel
                IrcClient.Trace("Cannot find user " + oldNick + " in channel " + Name);
                //    channel.Resync();
        	}
        }

        internal void Clear()
        {
            foreach(IrcChannelUser user in m_users.Values)
            {
                user.IrcUser.RemoveChannel(this);
            }
            m_users.Clear();
            m_isSynced = false;
        }

        internal BanInfo AddBan(string banMask, string who, DateTime date)
        {
            BanInfo banInfo = new BanInfo(banMask, who, date);
            AddBan(banInfo);
            return banInfo;
        }

        internal void AddBan(BanInfo banInfo)
        {
            m_bans[banInfo.BanMask] = banInfo;
        }

        internal bool RemoveBan(string banMask)
        {
           return m_bans.Remove(banMask);
        }

        internal void ClearBans()
        {
        	m_bans.Clear();
        }
    }
}
