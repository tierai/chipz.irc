using System;
using System.Collections.Generic;
using System.Net;

namespace Chipz.IRC
{
    /// <summary>
    /// LoginInfo.
    /// </summary>
    public class LoginInfo
    {
        /// <summary>
        /// Gets or sets nicknames.
        /// </summary>
        public string[] Nicknames
        {
            get { return m_nicknames; }
            set { m_nicknames = value; }
        }
        string[] m_nicknames;

        /// <summary>
        /// Gets or sets username.
        /// </summary>
        public string Username
        {
            get { return m_username; }
            set { m_username = value; }
        }
        string m_username;

        /// <summary>
        /// Gets or sets real name.
        /// </summary>
        public string Realname
        {
            get { return m_realname; }
            set { m_realname = value; }
        }
        string m_realname;

        /// <summary>
        /// Gets or sets server password.
        /// </summary>
        public string Password
        {
            get { return m_password; }
            set { m_password = value; }
        }
        string m_password;

        /// <summary>
        /// Gets or sets usermode.
        /// </summary>
        public int Usermode
        {
            get { return m_usermode; }
            set { m_usermode = value; }
        }
        int m_usermode;

        /// <summary>
        /// Creates a new instance of LoginInfo.
        /// </summary>
        /// <param name="nickname">Nickname.</param>
        /// <param name="username">Username.</param>
        /// <param name="realname">Real name.</param>
        /// <param name="usermode">Usermode.</param>
        /// <param name="password">Password.</param>
        public LoginInfo(string nickname, string username, string realname, int usermode, string password)
            : this(new string[] { nickname }, username, realname, usermode, password)
        {}

        /// <summary>
        /// Creates a new instance of LoginInfo.
        /// </summary>
        /// <param name="nicknames">Nicknames.</param>
        /// <param name="username">Username.</param>
        /// <param name="realname">Real name.</param>
        /// <param name="usermode">Usermode.</param>
        /// <param name="password">Password.</param>
        public LoginInfo(string[] nicknames, string username, string realname, int usermode, string password)
        {
            m_nicknames = nicknames;
            m_username = username;
            m_realname = realname;
            m_usermode = usermode;
            m_password = password;
        }
    }

    /// <summary>
    /// HostInfo.
    /// </summary>
    public class HostInfo
    {
        /// <summary>
        /// Gets or sets the hostname, (address or ip).
        /// </summary>
        public string Hostname
        {
            get { return m_hostname; }
            set { m_hostname = value; }
        }
        string m_hostname;

        /// <summary>
        /// Gets or sets host ports.
        /// </summary>
        public int[] Ports
        {
            get { return m_ports; }
            set { m_ports = value; }
        }
        int[] m_ports;

        /// <summary>
        /// Gets or sets wheter SSL should be used.
        /// </summary>
        public bool Ssl
        {
            get { return m_ssl; }
            set { m_ssl = value; }
        }
        bool m_ssl;

        /// <summary>
        /// Creates a new instance of HostInfo.
        /// </summary>
        /// <param name="hostname">Hostname.</param>
        /// <param name="port">Port.</param>
        /// <param name="ssl">Use SSL.</param>
        public HostInfo(string hostname, int port, bool ssl)
            : this(hostname, new int[] { port }, ssl)
        { }

        /// <summary>
        /// Creates a new instance of HostInfo.
        /// </summary>
        /// <param name="hostname">Hostname.</param>
        /// <param name="ports">Ports.</param>
        /// <param name="ssl">Use SSL.</param>
        public HostInfo(string hostname, int[] ports, bool ssl)
        {
            m_hostname = hostname;
            m_ports = ports;
            m_ssl = ssl;
        }
    }

    /// <summary>
    /// ConnectInfo.
    /// </summary>
    public class ConnectInfo
    {
        /// <summary>
        /// Reconnect when not disconnected by user.
        /// </summary>
        public bool Keep
        {
            get { return m_keep; }
            set { m_keep = value; }
        }
        bool m_keep;

        /// <summary>
        /// Delay in seconds before reconnecting.
        /// </summary>
        public int ReconnectDelay
        {
            get { return m_reconnectDelay; }
            set { m_reconnectDelay = value; }
        }
        int m_reconnectDelay = 10;

        /// <summary>
        /// Max connection-failures in a row.
        /// </summary>
        public int ReconnectLimit
        {
            get { return m_connectLimit; }
            set { m_connectLimit = value; }
        }
        int m_connectLimit = 0;

        /// <summary>
        /// Get or set LoginInfo.
        /// </summary>
        public LoginInfo LoginInfo
        {
            get { return m_loginInfo; }
            set { m_loginInfo = value; }
        }
        LoginInfo m_loginInfo = null;

        /// <summary>
        /// Hosts.
        /// </summary>
        public HostInfo[] Hosts
        {
            get { return m_hosts; }
            set { m_hosts = value; }
        }
        HostInfo[] m_hosts;

        /// <summary>
        /// Creates a new instance of ConnectInfo.
        /// </summary>
        /// <param name="keep">Auto-reconnect.</param>
        public ConnectInfo(bool keep)
            : this(new HostInfo[] { }, keep, null)
        { }

        /// <summary>
        /// Creates a new instance of ConnectInfo.
        /// </summary>
        /// <param name="host">Host.</param>
        /// <param name="keep">Auto-reconnect.</param>
        /// <param name="loginInfo">Login information.</param>
        public ConnectInfo(HostInfo host, bool keep, LoginInfo loginInfo)
            : this(new HostInfo[] { host }, keep, loginInfo)
        { }

        /// <summary>
        /// Creates a new instance of ConnectInfo.
        /// </summary>
        /// <param name="hosts">Host list.</param>
        /// <param name="keep">Auto-reconnect.</param>
        /// <param name="loginInfo">Login information.</param>
        public ConnectInfo(HostInfo[] hosts, bool keep, LoginInfo loginInfo)
        {
            m_hosts = hosts;
            m_keep = keep;
            m_loginInfo = loginInfo;
        }
    }
}
