using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.ComponentModel;

namespace Chipz.IRC
{
    public partial class IrcClient
    {
        /// <summary>
        /// Gets or sets the string that is sent to a VERSION ctcp request. Default is "Chipz.IRC " + Platform + " (" + AssemblyVersion + ")".
        /// </summary>
        [Category("CTCP")]
        public string VersionReply
        {
            get { return m_versionReply; }
            set { m_versionReply = value; }
        }
        string m_versionReply = "Chipz.IRC " + Environment.OSVersion.Platform
                + " (" + Assembly.GetExecutingAssembly().GetName().ToString() + ")";

        void CreateInternalCtcpHandlers()
        {
            SpecificCtcpRequest["ACTION"] += Client_OnCtcpRequest_Action;
            SpecificCtcpRequest["VERSION"] += Client_OnCtcpRequest_Version;
            SpecificCtcpRequest["PING"] += Client_OnCtcpRequest_Ping;
            SpecificCtcpRequest["CLIENTINFO"] += Client_OnCtcpRequest_ClientInfo;
            SpecificCtcpRequest["TIME"] += Client_OnCtcpRequest_Time;
        }

        void Client_OnCtcpRequest_Version(object sender, CtcpEventArgs e)
        {
            SendCtcpReply(e.Who, "VERSION " + VersionReply);
        }

        void Client_OnCtcpRequest_Ping(object sender, CtcpEventArgs e)
        {
            SendCtcpReply(e.Who, "PONG " + e.Message);
        }

        void Client_OnCtcpRequest_ClientInfo(object sender, CtcpEventArgs e)
        {
            string info = "";
            foreach (string s in SpecificCtcpRequest.GetKeys())
                info += " " + s;
            SendCtcpReply(e.Who, "CLIENTINFO" + info);
        }

        void Client_OnCtcpRequest_Time(object sender, CtcpEventArgs e)
        {
            SendCtcpReply(e.Who, "TIME " + DateTime.Now.ToString() + " (" + TimeZone.CurrentTimeZone.StandardName + ")");
        }

        void Client_OnCtcpRequest_Action(object sender, CtcpEventArgs e)
        {
            if (e.Data.Channel == null)
            {
                OnQueryAction(new QueryActionEventArgs(e.Data, e.Message));
            }
            else
            {
                OnChannelAction(new ChannelActionEventArgs(e.Data, e.Message));
            }
        }
    }
}
