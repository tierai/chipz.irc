using System;
using System.ComponentModel;
using System.Collections.Generic;

namespace Chipz.IRC
{
    public partial class IrcClient
    {
        /// <summary>
        /// List.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelListEventArgs> ChannelList;

        /// <summary>
        /// End of list.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<IrcEventArgs> ChannelListEnd;

        /// <summary>
        /// This event is called when the irc-client needs host information.
        /// This event is only used when using the internal connection handler.
        /// </summary>
        [Category("Connection")]
        public event HostInfoCallback GetHostInfo;        

        /// <summary>
        /// This event is called when the irc-client needs to login.
        /// This event is only used when using the internal connection handler.
        /// </summary>
        [Category("Connection")]
        public event LoginInfoCallback GetLoginInfo;

        /// <summary>
        /// Occurs when a line has been read and before it's been parsed.
        /// </summary>
        [Category("Advanced")]
        public event EventHandler<ReadLineEventArgs> LineRead;

        /// <summary>
        /// Occurs before a line has been places in the send queue.
        /// </summary>
        [Category("Advanced")]
        public event EventHandler<WriteLineEventArgs> LineWrite;

        /// <summary>
        /// Occurs when the client is attempting to connect.
        /// </summary>
        [Category("Status")]
        public event EventHandler Connecting;

        /// <summary>
        /// Occurs when the client is connected.
        /// </summary>
        [Category("Status")]
        public event EventHandler Connected;

        /// <summary>
        /// Occurs before the client tries to disconnect.
        /// </summary>
        [Category("Status")]
        public event EventHandler Disconnecting;

        /// <summary>
        /// Occurs when the client has been disconnected.
        /// </summary>
        [Category("Status")]
        public event EventHandler Disconnected;

        /// <summary>
        /// Occurs when the connection status has changed.
        /// </summary>
        [Category("Status")]
        public event EventHandler StatusChanged;
        
        /// <summary>
        /// Connection Error
        /// </summary>
        [Category("Connection")]
        public event EventHandler<ConnectionErrorEventArgs> ConnectionError;

        /// <summary>
        /// Override the default message event handler.
        /// </summary>
        [Category("Advanced")]
        public event EventHandler<HandleMessageEventArgs> HandleMessage;

        /// <summary>
        /// Override the default reply code handler.
        /// </summary>
        [Category("Advanced")]
        public event EventHandler<HandleReplyCodeEventArgs> HandleReplyCode;

        /// <summary>
        /// Triggered when a message has been received.
        /// </summary>
        [Category("Advanced")]
        public event EventHandler<IrcEventArgs> RawMessage;
        
        /// <summary>
        /// Custom exception handler.
        /// </summary>
        [Category("Advanced")]
        public event EventHandler<ExceptionEventArgs> Exception;

        /// <summary>
        /// ServerMessage
        /// </summary>
        [Category("Connection")]
        public event EventHandler<IrcEventArgs> ServerMessage;

        /// <summary>
        /// Error
        /// </summary>
        [Category("Global")]
        public event EventHandler<ErrorEventArgs> Error;

        /// <summary>
        /// A user has joined a channel.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<JoinEventArgs> ChannelJoin;

        /// <summary>
        /// A user has left a channel.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<PartEventArgs> ChannelPart;

        /// <summary>
        /// A user has been kicked from a channel.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<KickEventArgs> ChannelKick;

        /// <summary>
        /// Contains the timestamp when the channels was created.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelCreatedEventArgs> ChannelCreated;

        /// <summary>
        /// Numeric reply.
        /// </summary>
        [Category("Global")]
        public event EventHandler<IrcEventArgs> NumericReply;

        /// <summary>
        /// A user has quit IRC.
        /// </summary>
        [Category("Global")]
        public event EventHandler<QuitEventArgs> IrcQuit;

        /// <summary>
        /// Ping.
        /// </summary>
        [Category("Global")]
        public event EventHandler<PingEventArgs> IrcPing;

        /// <summary>
        /// Pong.
        /// </summary>
        [Category("Global")]
        public event EventHandler<PongEventArgs> IrcPong;

        /// <summary>
        /// Channel names.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<NamesEventArgs> Names;

        /// <summary>
        /// End of channel namelist.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<NamesEndEventArgs> NamesEnd;

        /// <summary>
        /// Registered, login successful.
        /// </summary>
        [Category("Global")]
        public event EventHandler<RegisteredEventArgs> Registered;

        /// <summary>
        /// Banlist for a channel.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<BanlistEndEventArgs> Banlist;

        /// <summary>
        /// End of banlist.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<BanlistEndEventArgs> BanlistEnd;

        /// <summary>
        /// Who
        /// </summary>
        [Category("User")]
        public event EventHandler<WhoEventArgs> Who;

        /// <summary>
        /// UserMode
        /// </summary>
        [Category("User")]
        public event EventHandler<UserModeEventArgs> UserMode;

        /// <summary>
        /// Mode changed.
        /// </summary>
        [Category("User")]
        public event EventHandler<ModeChangedEventArgs> ModeChanged;

        /// <summary>
        /// Someone changed nickname.
        /// </summary>
        [Category("User")]
        public event EventHandler<NickChangedEventArgs> NickChanged;
        
        /// <summary>
        /// Channel topic changed.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelTopicChangedEventArgs> ChannelTopicChanged;
        
        /// <summary>
        /// Channel topic.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelTopicEventArgs> ChannelTopic;
        
        /// <summary>
        /// No channel topic.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<IrcEventArgs> ChannelTopicNotSet;

        /// <summary>
        /// Channel topic set info.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelTopicSetByEventArgs> ChannelTopicSetBy;
        
        /// <summary>
        /// Channel mode (full).
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelModeIsEventArgs> ChannelModeIs;

        /// <summary>
        /// Channel synced, all channel users known.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<IrcEventArgs> ChannelSynced;

        /// <summary>
        /// Channel mode changed.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelModeChangedEventArgs> ChannelModeChanged;

        /// <summary>
        /// Channel action.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelActionEventArgs> ChannelAction;

        /// <summary>
        /// Channel notice.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelNoticeEventArgs> ChannelNotice;

        /// <summary>
        /// Channel message.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelMessageEventArgs> ChannelMessage;

        /// <summary>
        /// Channel usermode changed.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelUserModeChangedEventArgs> ChannelUserModeChanged;

        /// <summary>
        /// Query action.
        /// </summary>
        [Category("Private")]
        public event EventHandler<QueryActionEventArgs> QueryAction;

        /// <summary>
        /// Query notice.
        /// </summary>
        [Category("Private")]
        public event EventHandler<QueryNoticeEventArgs> QueryNotice;

        /// <summary>
        /// Query message.
        /// </summary>
        [Category("Private")]
        public event EventHandler<QueryMessageEventArgs> QueryMessage;

        /// <summary>
        /// Message. This event gets triggered whenever a Query or Channel Message/Notice/Action has arrived.
        /// </summary>
        [Category("Message")]
        public event EventHandler<MessageEventArgs> Message;

        /// <summary>
        /// OutgoingMessage. Works the same way as the Message event, but for outgoing messages.
        /// </summary>
        [Category("Message")]
        public event EventHandler<MessageEventArgs> OutgoingMessage;

        /// <summary>
        /// A ban has been added to a channel.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<BanEventArgs> BanAdded;

        /// <summary>
        /// A ban has been removed from a channel.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<BanEventArgs> BanRemoved;

        /// <summary>
        /// Message of the day received.
        /// </summary>
        [Category("Global")]
        public event EventHandler<MotdEventArgs> Motd;
        
        /// <summary>
        /// User whois info received.
        /// </summary>
        [Category("User")]
        public event EventHandler<WhoIsEventArgs> WhoIs;

        /// <summary>
        /// A ctcp request has been received.
        /// </summary>
        [Category("CTCP")]
        public event EventHandler<CtcpEventArgs> IncomingCtcpRequest;

        /// <summary>
        /// A ctcp reply has been received.
        /// </summary>
        [Category("CTCP")]
        public event EventHandler<CtcpEventArgs> IncomingCtcpReply;
        
        /// <summary>
        /// Bounce, (Support?) message.
        /// </summary>
        [Category("Global")]
        public event EventHandler<SupportEventArgs> Support;

        /// <summary>
        /// Away.
        /// </summary>
        [Category("User")]
        public event EventHandler<AwayEventArgs> Away;

        /// <summary>
        /// UnAway
        /// </summary>
        [Category("User")]
        public event EventHandler<IrcEventArgs> UnAway;

        /// <summary>
        /// NowAway
        /// </summary>
        [Category("User")]
        public event EventHandler<IrcEventArgs> NowAway;

        /// <summary>
        /// Outgoing channel message.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelMessageEventArgs> OutgoingChannelMessage;

        /// <summary>
        /// Outgoing channel action.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelActionEventArgs> OutgoingChannelAction;
        
        /// <summary>
        /// Outgoing channel notice.
        /// </summary>
        [Category("Channel")]
        public event EventHandler<ChannelNoticeEventArgs> OutgoingChannelNotice;

        /// <summary>
        /// Outgoing query message.
        /// </summary>
        [Category("Private")]
        public event EventHandler<QueryMessageEventArgs> OutgoingQueryMessage;

        /// <summary>
        /// Outgoing query action.
        /// </summary>
        [Category("Private")]
        public event EventHandler<QueryActionEventArgs> OutgoingQueryAction;

        /// <summary>
        /// Outgoing query notice.
        /// </summary>
        [Category("Private")]
        public event EventHandler<QueryNoticeEventArgs> OutgoingQueryNotice;

        /// <summary>
        /// Outgoing ctcp request.
        /// </summary>
        [Category("CTCP")]
        public event EventHandler<CtcpEventArgs> OutgoingCtcpRequest;

        /// <summary>
        /// Outgoing ctcp reply.
        /// </summary>
        [Category("CTCP")]
        public event EventHandler<CtcpEventArgs> OutgoingCtcpReply;

        /// <summary>
        /// Network property has been set.
        /// </summary>
        [Category("Global")]
        public event EventHandler<EventArgs> NetworkSet;

        /// <summary>
        /// Validate remote SSL cerificate.
        /// </summary>
        [Category("SSL")]
        public event EventHandler<RemoteCertificateEventArgs> RemoteCertificateValidation;

        /// <summary>
        /// Select local SSL certificate.
        /// </summary>
        [Category("SSL")]
        public event EventHandler<LocalCertificateEventArgs> LocalCertificateValidation;

        /// <summary>
        /// Gets the CTCP-request handler.
        /// </summary>
        [Category("CTCP")]
        [Browsable(false)] //TODO: Editor
        public MultiEventHandler<CtcpEventArgs> SpecificCtcpRequest
        {
            get { return m_specificCtcpRequest; }
        }

        /// <summary>
        /// Gets the CTCP-reply handler.
        /// </summary>
        [Category("CTCP")]
        [Browsable(false)] //TODO: Editor
        public MultiEventHandler<CtcpEventArgs> SpecificCtcpReply
        {
            get { return m_specificCtcpReply; }
        }

        /// <summary>
        /// Adds a specific CTCP request-handler.
        /// </summary>
        /// <param name="cmd">CTCP command.</param>
        /// <param name="h">EventHandler.</param>
        public void AddSpecificCtcpRequestHandler(string cmd, EventHandler<CtcpEventArgs> h)
        {
            m_specificCtcpRequest[cmd] += h;
        }

        /// <summary>
        /// Removes a specific CTCP request-handler.
        /// </summary>
        /// <param name="cmd">CTCP command.</param>
        /// <param name="h">EventHandler.</param>
        public void RemoveSpecificCtcpRequestHandler(string cmd, EventHandler<CtcpEventArgs> h)
        {
            m_specificCtcpRequest[cmd] -= h;
        }

        /// <summary>
        /// Adds a specific CTCP reply-handler.
        /// </summary>
        /// <param name="cmd">CTCP command.</param>
        /// <param name="h">EventHandler.</param>
        public void AddSpecificCtcpReplyHandler(string cmd, EventHandler<CtcpEventArgs> h)
        {
            m_specificCtcpReply[cmd] += h;
        }

        /// <summary>
        /// Removes a specific CTCP reply-handler.
        /// </summary>
        /// <param name="cmd">CTCP command.</param>
        /// <param name="h">EventHandler.</param>
        public void RemoveSpecificCtcpReplyHandler(string cmd, EventHandler<CtcpEventArgs> h)
        {
            m_specificCtcpReply[cmd] -= h;
        }

        MultiEventHandler<CtcpEventArgs> m_specificCtcpRequest = new MultiEventHandler<CtcpEventArgs>();
        MultiEventHandler<CtcpEventArgs> m_specificCtcpReply = new MultiEventHandler<CtcpEventArgs>();

        protected virtual void OnChannelList(ChannelListEventArgs e)
        {
            TriggerEvent(ChannelList, e);
        }

        protected virtual void OnChannelListEnd(IrcEventArgs e)
        {
            TriggerEvent(ChannelListEnd, e);
        }

        /// <summary>
        /// OnGetLoginInfo.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected LoginInfo OnGetLoginInfo(LoginInfo def)
        {
            if (GetLoginInfo != null)
                return GetLoginInfo(this, def);
            return def;
        }
        
        /// <summary>
        /// OnGetHostInfo.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected HostInfo[] OnGetHostInfo(HostInfo[] def)
        {
            if (GetHostInfo != null)
                return GetHostInfo(this, def);
            return def;
        }

        /// <summary>
        /// OnLineRead.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnLineRead(ReadLineEventArgs e)
        {
            TriggerEvent(LineRead, e);

            MessageData msg = ParseMessage(e.Line);
            
            if (msg == null)
                return;

            if (msg.ReplyCode == ReplyCode.Null)
            {
                OnHandleMessage(msg);
            }
            else
            {
                OnHandleReplyCode(msg);
            }
        }

        /// <summary>
        /// OnLineWrite.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnLineWrite(WriteLineEventArgs e)
        {
            TriggerEvent(LineWrite, e);
        }

        /// <summary>
        /// OnNumericReply
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnNumericReply(IrcEventArgs e)
        {
            TriggerEvent(NumericReply, e);
        }

        /// <summary>
        /// OnConnectionError.
        /// </summary>
        /// <param name="error">Error message.</param>
        protected virtual void OnConnectionError(string error)
        {
            OnConnectionError(new ConnectionErrorEventArgs(error));
        }

        /// <summary>
        /// OnConnectionError.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnConnectionError(ConnectionErrorEventArgs e)
        {
            try
            {
                Trace("Connection error: " + e.Error);
                TriggerEvent(ConnectionError, e);
            }
            finally
            {
                if (Status != ConnectionStatus.Disconnected)
                    OnDisconnected(null);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnConnecting(EventArgs e)
        {
            OnConnecting_Internal(e);
			TriggerEvent(Connecting, e);
            OnStatusChanged(null);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnConnected(EventArgs e)
        {
        	OnConnected_Internal(e);
			TriggerEvent(Connected, e);
            OnStatusChanged(null);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnDisconnecting(EventArgs e)
        {
        	OnDisconnecting_Internal(e);
        	TriggerEvent(Disconnecting, e);
            OnStatusChanged(null);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnDisconnected(EventArgs e)
        {
            try
            {
                OnDisconnected_Internal(e);
            	TriggerEvent(Disconnected, e);
                OnStatusChanged(null);
            }
            finally
            {
            	OnDisconnected_Finally(e);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnStatusChanged(EventArgs e)
        {
            TriggerEvent(StatusChanged, e);
        }

        /// <summary>
        /// OnHandleReplyCode.
        /// </summary>
        /// <param name="data">MessageData.</param>
        protected virtual void OnHandleReplyCode(MessageData data)
        {
            try
            {
            	HandleReplyCodeEventArgs e = new HandleReplyCodeEventArgs(data);
            	TriggerEvent(HandleReplyCode, e);
                if (e.Cancel)
                    return;
            }
            finally
            {
                OnHandleReplyCode_Internal(data);
            }
        }

        /// <summary>
        /// OnHandleMessage
        /// </summary>
        /// <param name="data">MessageData.</param>
        protected virtual void OnHandleMessage(MessageData data)
        {
            try
            {
            	HandleMessageEventArgs e = new HandleMessageEventArgs(data);
            	TriggerEvent(HandleMessage, e);
                if (e.Cancel)
                    return;
            }
            finally
            {
                OnHandleMessage_Internal(data);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnRawMessage(IrcEventArgs e)
        {
            TriggerEvent(RawMessage, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnError(ErrorEventArgs e)
        {
            TriggerEvent(Error, e);
        }

        /// <summary>
        /// OnJoin.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnJoin(JoinEventArgs e)
        {
        	OnJoin_Internal(e);
    		TriggerEvent(ChannelJoin, e);
        }

        /// <summary>
        /// OnPart.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnPart(PartEventArgs e)
        {
            try
            {
        		TriggerEvent(ChannelPart, e);
            }
            finally
            {
            	OnPart_Internal(e);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnQuit(QuitEventArgs e)
        {
            try
            {
        		TriggerEvent(IrcQuit, e);
            }
            finally
            {
            	OnQuit_Internal(e);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnKick(KickEventArgs e)
        {
            try
            {
        		TriggerEvent(ChannelKick, e);
            }
            finally
            {
                UserLeft(e.Channel, e.Whom);
            }
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnPing(PingEventArgs e)
        {
            string server = e.Data.Params[1];
            m_rfc.Pong(server);
        	TriggerEvent(IrcPing, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnPong(PongEventArgs e)
        {
        	TriggerEvent(IrcPong, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnNames(NamesEventArgs e)
        {
        	TriggerEvent(Names, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnNamesEnd(NamesEndEventArgs e)
        {
        	TriggerEvent(NamesEnd, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnRegistered(RegisteredEventArgs e)
        {
        	TriggerEvent(Registered, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnBanlist(BanlistEventArgs e)
        {
        	TriggerEvent(Banlist, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnBanlistEnd(BanlistEndEventArgs e)
        {
        	TriggerEvent(BanlistEnd, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnWho(WhoEventArgs e)
        {
        	TriggerEvent(Who, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnUserMode(UserModeEventArgs e)
        {
        	TriggerEvent(UserMode, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnModeChanged(ModeChangedEventArgs e)
        {
            OnModeChanged_Internal(e);
        	TriggerEvent(ModeChanged, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnNickChanged(NickChangedEventArgs e)
        {
        	OnNickChanged_Internal(e);
        	TriggerEvent(NickChanged, e);
        }

        /// <summary>
        /// OnChannelTopicChanged.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelTopicChanged(ChannelTopicChangedEventArgs e)
        {
            OnChannelTopicChanged_Internal(e);
        	TriggerEvent(ChannelTopicChanged, e);
        }

        /// <summary>
        /// OnChannelTopic.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelTopic(ChannelTopicEventArgs e)
        {
        	OnChannelTopic_Internal(e);
        	TriggerEvent(ChannelTopic, e);
        }

        /// <summary>
        /// OnChannelTopicNotSet.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelTopicNotSet(IrcEventArgs e)
        {
			OnChannelTopicNotSet_Internal(e);
        	TriggerEvent(ChannelTopicNotSet, e);
        }

        /// <summary>
        /// OnChannelTopicSetBy.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelTopicSetBy(ChannelTopicSetByEventArgs e)
        {
        	TriggerEvent(ChannelTopicSetBy, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelModeIs(ChannelModeIsEventArgs e)
        {
        	TriggerEvent(ChannelModeIs, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelSynced(IrcEventArgs e)
        {
        	TriggerEvent(ChannelSynced, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelModeChanged(ChannelModeChangedEventArgs e)
        {
        	TriggerEvent(ChannelModeChanged, e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelAction(ChannelActionEventArgs e)
        {
        	TriggerEvent(ChannelAction, e);
            OnMessage(e);
        }

        /// <summary>
        /// .
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelNotice(ChannelNoticeEventArgs e)
        {
        	TriggerEvent(ChannelNotice, e);
            OnMessage(e);
        }

        /// <summary>
        /// OnChannelMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelMessage(ChannelMessageEventArgs e)
        {
        	TriggerEvent(ChannelMessage, e);
            OnMessage(e);
        }

        /// <summary>
        /// OnChannelUserModeChanged.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelUserModeChanged(ChannelUserModeChangedEventArgs e)
        {
        	TriggerEvent(ChannelUserModeChanged, e);
        }

        /// <summary>
        /// OnQueryAction.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnQueryAction(QueryActionEventArgs e)
        {
        	TriggerEvent(QueryAction, e);
            OnMessage(e);
        }

        /// <summary>
        /// OnQueryNotice.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnQueryNotice(QueryNoticeEventArgs e)
        {
        	TriggerEvent(QueryNotice, e);
            OnMessage(e);
        }

        /// <summary>
        /// OnQueryMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnQueryMessage(QueryMessageEventArgs e)
        {
        	TriggerEvent(QueryMessage, e);
            OnMessage(e);
        }

        /// <summary>
        /// OnIncomingCtcpRequest.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnIncomingCtcpRequest(CtcpRequestEventArgs e)
        {
        	TriggerEvent(IncomingCtcpRequest, e);
        }

        /// <summary>
        /// OnIncomingCtcpReply.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnIncomingCtcpReply(CtcpReplyEventArgs e)
        {
        	TriggerEvent(IncomingCtcpReply, e);
        }

        /// <summary>
        /// OnSpecificCtcpRequest.
        /// </summary>
        /// <param name="name">CTCP command.</param>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnSpecificCtcpRequest(string name, CtcpRequestEventArgs e)
        {
        	TriggerEvent(SpecificCtcpRequest[name], e);
        }

        /// <summary>
        /// OnSpecificCtcpReply.
        /// </summary>
        /// <param name="name">CTCP command.</param>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnSpecificCtcpReply(string name, CtcpReplyEventArgs e)
        {
        	TriggerEvent(SpecificCtcpReply[name], e);
        }

        /// <summary>
        /// OnBanAdded.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnBanAdded(BanEventArgs e)
        {
			OnBanAdded_Internal(e);
        	TriggerEvent(BanAdded, e);
        }

        /// <summary>
        /// OnBanRemoved.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnBanRemoved(BanEventArgs e)
        {
            OnBanRemoved_Internal(e);
        	TriggerEvent(BanRemoved, e);
        }

        /// <summary>
        /// OnMotd.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnMotd(MotdEventArgs e)
        {
        	TriggerEvent(Motd, e);
        }

        /// <summary>
        /// OnWhoIs.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnWhoIs(WhoIsEventArgs e)
        {
        	TriggerEvent(WhoIs, e);
        }

        /// <summary>
        /// OnSupport.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnSupport(SupportEventArgs e)
        {
        	TriggerEvent(Support, e);
        }

        /// <summary>
        /// OnOutgoingQueryAction.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingQueryAction(QueryActionEventArgs e)
        {
        	TriggerEvent(OutgoingQueryAction, e);
            OnOutgoingMessage(e);
        }

        /// <summary>
        /// OnOutgoingQueryNotice.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingQueryNotice(QueryNoticeEventArgs e)
        {
            TriggerEvent(OutgoingQueryNotice, e);
            OnOutgoingMessage(e);
        }

        /// <summary>
        /// OnOutgoingQueryMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingQueryMessage(QueryMessageEventArgs e)
        {
            TriggerEvent(OutgoingQueryMessage, e);
            OnOutgoingMessage(e);
        }

        /// <summary>
        /// OnOutgoingChannelAction.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingChannelAction(ChannelActionEventArgs e)
        {
            TriggerEvent(OutgoingChannelAction, e);
            OnOutgoingMessage(e);
        }

        /// <summary>
        /// OnOutgoingChannelNotice.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingChannelNotice(ChannelNoticeEventArgs e)
        {
            TriggerEvent(OutgoingChannelNotice, e);
            OnOutgoingMessage(e);
        }

        /// <summary>
        /// OnOutgoingChannelMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingChannelMessage(ChannelMessageEventArgs e)
        {
            TriggerEvent(OutgoingChannelMessage, e);
            OnOutgoingMessage(e);
        }

        /// <summary>
        /// OnOutgoingCtcpRequest.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingCtcpRequest(CtcpEventArgs e)
        {
            TriggerEvent(OutgoingCtcpRequest, e);
        }

        /// <summary>
        /// OnOutgoingCtcpReply.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingCtcpReply(CtcpEventArgs e)
        {
            TriggerEvent(OutgoingCtcpReply, e);
        }

        /// <summary>
        /// OnOutgoingMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnOutgoingMessage(MessageEventArgs e)
        {
            TriggerEvent(OutgoingMessage, e);
        }

        /// <summary>
        /// OnMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnMessage(MessageEventArgs e)
        {
            TriggerEvent(Message, e);
        }

        /// <summary>
        /// OnChannelCreated.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnChannelCreated(ChannelCreatedEventArgs e)
        {
            TriggerEvent(ChannelCreated, e);
        }

        /// <summary>
        /// OnServerMessage.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnServerMessage(IrcEventArgs e)
        {
            TriggerEvent(ServerMessage, e);
        }

        /// <summary>
        /// OnAway.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnAway(AwayEventArgs e)
        {
            TriggerEvent(Away, e);
        }

        /// <summary>
        /// OnUnAway.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnUnAway(IrcEventArgs e)
        {
            TriggerEvent(UnAway, e);
        }

        /// <summary>
        /// OnNowAway.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnNowAway(IrcEventArgs e)
        {
            TriggerEvent(NowAway, e);
        }

        /// <summary>
        /// OnNetworkSet.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnNetworkSet(EventArgs e)
        {
            TriggerEvent(NetworkSet, e);
        }

        /// <summary>
        /// OnRemoteCertificateValidation.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnRemoteCertificateValidation(RemoteCertificateEventArgs e)
        {
            TriggerEvent(RemoteCertificateValidation, e);
        }

        /// <summary>
        /// OnLocalCertificateValidation.
        /// </summary>
        /// <param name="e">EventArgs.</param>
        protected virtual void OnLocalCertificateValidation(LocalCertificateEventArgs e)
        {
            TriggerEvent(LocalCertificateValidation, e);
        }
        
        /// <summary>
        /// OnException.
        /// </summary>
        /// <param name="e">ExceptionEventArgs.</param>
        protected virtual void OnException(ExceptionEventArgs e)
        {
        	if(Exception != null)
        		Exception(this, e);
        }
        
        /// <summary>
        /// Event dispatcher.
        /// Chipz.IRC was originally built not to trust hooked events, this handler will supress any user exceptions
        /// if SupressExceptions is set to true.
        /// </summary>
        /// <param name="eventHandler">EventHandler delegate.</param>
        /// <param name="eventArgs">EventArgs.</param>
        protected virtual void TriggerEvent<T>(Delegate eventHandler, T eventArgs) where T : EventArgs
        {
        	if(eventHandler == null)
        		return;
        		
    		try
    		{
    			eventHandler.DynamicInvoke(this, eventArgs);
    		}
    		catch(Exception ex)
    		{
    			ExceptionEventArgs e = new ExceptionEventArgs(ex, false);
    			
    			OnException(e);
    			
    			if(!e.Handled)
    			{
    				if(SupressExceptions)
    					Trace(ex);
    				else
    					throw;
    			}
    		}
        }
        
        #region Internal handlers
        
        void OnConnecting_Internal(EventArgs e)
        {
            m_silence = 0;
            m_modes = 0;
            m_maxChannels = 0;
            m_maxBans = 0;
            m_maxNickLength = 0;
            m_maxTopicLength = 0;
            m_maxAwayLength = 0;
            m_maxKickLength = 0;
            m_channelTypes = new char[] { '#', '&' };
            m_channelUserPrefixes.Clear();
            m_channelModesA.Clear();
            m_channelModesB.Clear();
            m_channelModesC.Clear();
            m_channelModesD.Clear();
            m_caseMapping = string.Empty;
            m_network = string.Empty;
            lock (m_syncRoot)
            	m_status = ConnectionStatus.Connecting;
		}
		
        void OnConnected_Internal(EventArgs e)
        {
            lock (m_syncRoot)
            	m_status = ConnectionStatus.Connected;
        }
        
        void OnDisconnecting_Internal(EventArgs e)
        {
            lock (m_syncRoot)
            	m_status = ConnectionStatus.Disconnecting;
        }
        
        void OnDisconnected_Internal(EventArgs e)
        {
            lock (m_syncRoot)
            	m_status = ConnectionStatus.Disconnected;

            lock (m_channels.SyncRoot)
            {
                foreach (IrcChannel channel in m_channels.Values)
                    channel.Clear();
            }

            ClearUsers();
        }
        
        void OnDisconnected_Finally(EventArgs e)
        {
            DestroyThreads();
            DestroySocket();

            m_isRegistered = false;
            m_isConnectionError = false;

            m_sendQueue = null;
            m_readQueue = null;
        }
        
        void OnChannelTopicChanged_Internal(ChannelTopicChangedEventArgs e)
        {
            IrcChannel channel = GetChannel(e.Channel);
            if (channel != null)
                channel.Topic = e.Topic;
        }
        
        void OnChannelTopic_Internal(ChannelTopicEventArgs e)
        {
            IrcChannel channel = GetChannel(e.Channel);
            if (channel != null)
                channel.Topic = e.Topic;
        }
        
        void OnChannelTopicNotSet_Internal(IrcEventArgs e)
        {
            IrcChannel channel = GetChannel(e.Data.Channel);
            if (channel != null)
                channel.Topic = null;
        }
        
        void OnBanAdded_Internal(BanEventArgs e)
        {
            IrcChannel channel = GetChannel(e.Channel);
            if (channel != null)
            {
                channel.AddBan(e.BanMask, e.Who, DateTime.Now);
            }
            else
                Trace("Channel " + e.Channel + " doesn't exist");
        }
        
        void OnBanRemoved_Internal(BanEventArgs e)
        {
            IrcChannel channel = GetChannel(e.Channel);
            if (channel != null)
                channel.RemoveBan(e.BanMask);
            else
                Trace("Channel " + e.Channel + " doesn't exist");
        }
        
        void OnPart_Internal(PartEventArgs e)
        {
       		UserLeft(e.Channel, e.Who);
        }
        
		void OnQuit_Internal(QuitEventArgs e)
		{
            IrcUser user = GetUser(e.Who);
            if (user != null)
            {
            	user.Clear();
            }
		}
        
        void OnJoin_Internal(JoinEventArgs e)
        {
            IrcChannel channel = GetChannel(e.Channel);
            if (channel == null)
            {
                if (!IsMe(e.Who))
                    return;
                channel = new IrcChannel(this, e.Channel);
                m_channels[e.Channel] = channel;
                channel.Resync();
            }
            else if (channel.IsSynced && IsMe(e.Who))
                channel.Resync();   // Something went wrong, we shouldn't be able to join a synced channel. (can occur after a netsplit + bnc).

            IrcUser ircUser = GetUser(e.Who, e.Data);
            channel.AddUser(ircUser, string.Empty);
        }
        
		void OnNickChanged_Internal(NickChangedEventArgs e)
        {
        	RenameUser(e.OldNick, e.NewNick);
		}

        void OnModeChanged_Internal(ModeChangedEventArgs e)
        {
            if (e.Channel == null)
                return;

            IrcChannel channel = GetChannel(e.Channel);

            if (channel == null)
            {
                Trace("OnModeChanged_Internal: Channel " + e.Channel + " does not exist");
                return;
            }

            int paramIndex = 0;
            string userModes = string.Empty;

            lock (m_channelUserPrefixes.SyncRoot)
            {
                foreach (char c in m_channelUserPrefixes.Keys)
                    userModes += c;
            }

            for (int i = 0; i < e.Modes.Length; ++i)
            {
                char op = e.Modes[i][0];
                char mode = e.Modes[i][1];

                if (mode == IrcChannelModes.Ban)
                {
                    if (paramIndex >= e.ModeParams.Length)
                    {
                        Trace("OnModeChanged_Internal: Mode/Param error");
                        break;
                    }

                    if (op == '+')
                        OnBanAdded(new BanEventArgs(e.ModeParams[paramIndex], e.Data));
                    else
                        OnBanRemoved(new BanEventArgs(e.ModeParams[paramIndex], e.Data));

                    paramIndex++;
                }
                else if (userModes.IndexOf(mode) > -1) // usermode
                {
                    if (paramIndex >= e.ModeParams.Length)
                    {
                        Trace("OnModeChanged_Internal: Mode/Param error");
                        break;
                    }

                    string target = e.ModeParams[paramIndex];
                    IrcChannelUser user = channel.GetUser(target);
                    if (user == null)
                    {
                        Trace("OnModeChanged_Internal: User " + target + " does not exist in channel " + channel.Name + ", op=" + op + ", mode=" + mode);
                        channel.Resync();
                        paramIndex++;
                        continue;
                    }

                    user.SetModeEnabled(mode.ToString(), op == '+');

                    OnChannelUserModeChanged(new ChannelUserModeChangedEventArgs(user.Nick, e.Modes[i], user.Mode, e.Data));

                    paramIndex++;
                }
                else
                {
                    if (op == '+')
                    {
                        if (m_channelModesB.Contains(mode) || m_channelModesC.Contains(mode))
                        {
                            if (paramIndex >= e.ModeParams.Length)
                            {
                                Trace("OnModeChanged_Internal: Mode/Param error");
                                break;
                            }
                            channel.AddMode(mode, e.ModeParams[paramIndex]);
                            paramIndex++;
                        }
                        else
                            channel.AddMode(mode, string.Empty);
                    }
                    else
                    {
                        channel.RemoveMode(mode);
                    }
                }
            }

            OnChannelModeChanged(new ChannelModeChangedEventArgs(e.Data));
        }

        #endregion // Internal handlers
    }
}