using System;
using System.Collections;
using System.Collections.Generic;

namespace Chipz.IRC.Collections
{
    /// <summary>
    /// Synchronized Enumerable Interface.
    /// </summary>
    /// <typeparam name="T">Type.</typeparam>
    public interface ISyncEnumerable<T> : IEnumerable<T>
    {
        /// <summary>
        /// Sync object.
        /// </summary>
        object SyncRoot { get; }
    }

    /// <summary>
    /// Synchronized Enumerator.
    /// </summary>
    /// <typeparam name="T">Type.</typeparam>
    public class SyncEnumerator<T> : IEnumerator<T>
    {
        ISyncEnumerable<T> enumerable;
        IEnumerator<T> enumerator;

        /// <summary>
        /// Creates a new instance of the SyncEnumerator class.
        /// </summary>
        /// <param name="enumerable">Enumerable.</param>
        /// <param name="enumerator">Enumerator.</param>
        public SyncEnumerator(ISyncEnumerable<T> enumerable, IEnumerator<T> enumerator)
        {
            this.enumerable = enumerable;
            this.enumerator = enumerator;
        }

        #region IEnumerator<KeyValuePair<KeyT,ValueT>> Members

        /// <summary>
        /// Current
        /// </summary>
        public T Current
        {
            get
            {
                lock (enumerable.SyncRoot)
                    return enumerator.Current;
            }
        }

        #endregion

        #region IDisposable Members

        /// <summary>
        /// Dispose.
        /// </summary>
        public void Dispose()
        {
            enumerator.Dispose();
            enumerator = null;
        }

        #endregion

        #region IEnumerator Members

        /// <summary>
        /// Current
        /// </summary>
        object IEnumerator.Current
        {
            get
            {
                lock (enumerable.SyncRoot)
                    return enumerator.Current;
            }
        }

        /// <summary>
        /// MoveNext.
        /// </summary>
        /// <returns>Bool</returns>
        public bool MoveNext()
        {
            lock (enumerable.SyncRoot)
                return enumerator.MoveNext();
        }

        /// <summary>
        /// Reset.
        /// </summary>
        public void Reset()
        {
            lock (enumerable.SyncRoot)
                enumerator.Reset();
        }

        #endregion
    }
}
