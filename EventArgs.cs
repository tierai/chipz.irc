using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Net.Security;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

//
// This file is a mess.
// TODO: Clean-up all classes that inherit IrcEventArgs.
// TODO: Interfaces!
// TODO: Mode eventargs...
namespace Chipz.IRC
{

	#region Core
	
    

    /// <summary>
    /// SupportEventArgs.
    /// </summary>
    public class SupportEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Support string.
        /// </summary>
        public string Support
        {
            get
            {
                string result = "";
                for (int i = 3; i < Data.Params.Length; ++i)
                    result += Data.Params[i] + " ";
                return result;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public SupportEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    public class HandleMessageEventArgs : IrcEventArgs
    {
        bool m_cancel = false;
        public bool Cancel
        {
            get { return m_cancel; }
            set { m_cancel = value; }
        }

        public HandleMessageEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    public class HandleReplyCodeEventArgs : IrcEventArgs
    {
        bool m_cancel = false;
        public bool Cancel
        {
            get { return m_cancel; }
            set { m_cancel = value; }
        }

        public HandleReplyCodeEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    #endregion
    
    /// <summary>
    /// QuitEventArgs.
    /// </summary>
    public class QuitEventArgs : IrcEventArgs, IUserEventArgs, IMessageEventArgs
	{
        /// <summary>
        /// Nickname of the user that quit.
        /// </summary>
		public string Who
		{
            get { return Data.Nick; }
		}

        /// <summary>
        /// Quit message.
        /// </summary>
        public string Message
        {
            get { return Data.Message; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
		public QuitEventArgs(MessageData data) : base(data)
		{
		}
	}
	
    /// <summary>
    /// PingEventArgs.
    /// </summary>
	public class PingEventArgs : IrcEventArgs, IMessageEventArgs
	{
        /// <summary>
        /// Ping message.
        /// </summary>
		public string Message { get { return Data.Message; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
		public PingEventArgs(MessageData data) : base(data) {}
	}

    /// <summary>
    /// PongEventArgs.
    /// </summary>
    public class PongEventArgs : IrcEventArgs, IMessageEventArgs
    {
        /// <summary>
        /// Pong message.
        /// </summary>
		public string Message { get { return Data.Message; } }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public PongEventArgs(MessageData data) : base(data) {}
    }

	#region NamesEventArgs

    /// <summary>
    /// NamesEventArgs.
    /// </summary>
    public class NamesEventArgs : IrcEventArgs
    {
        private string[] m_names;

        /// <summary>
        /// Get names.
        /// </summary>
        public string[] Names
        {
            get { return m_names; }
        }

        /// <summary>
        /// Get channel name.
        /// </summary>
        public string Channel
        {
            get { return Data.Channel; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public NamesEventArgs(MessageData data)
            : base(data)
        {
            m_names = data.Message.Split(' ');
        }
    }

	#endregion // NamesEventArgs

    #region NamesEndEventArgs

    /// <summary>
    /// NamesEndEventArgs.
    /// </summary>
    public class NamesEndEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get channel name.
        /// </summary>
        public string Channel
        {
            get { return Data.Channel; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public NamesEndEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    #endregion
	
	#region ModeChangedEventArgs

    /// <summary>
    /// ModeChangedEventArgs.
    /// </summary>
	public class ModeChangedEventArgs : ChannelEventArgs
	{
        /// <summary>
        /// Get mode string.
        /// </summary>
		public string Mode
		{
            get { return Data.Params[2]; }
		}
        
        /// <summary>
        /// Get modes.
        /// </summary>
        public string[] Modes
		{
            get { return m_modes; }
        }
        string[] m_modes;
        
        /// <summary>
        /// Get mode params.
        /// </summary>
        public string[] ModeParams
        {
            get { return m_modeParams; }
        }
        string[] m_modeParams;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
		public ModeChangedEventArgs(MessageData data) : base(data)
        {
            // targets
            m_modeParams = new string[Data.Params.Length - 3];
            for (int i = 0; i < m_modeParams.Length; ++i)
                m_modeParams[i] = Data.Params[i + 3];

            // modes
            char prefix = '?';
            List<string> result = new List<string>();

            foreach (char c in Mode)
            {
                if (c == '+' || c == '-')
                    prefix = c;
                else
                    result.Add(prefix.ToString() + c);
            }
            m_modes = result.ToArray();
		}
	}

	#endregion // ModeChangedEventArgs

    #region NickChangedEventArgs

    /// <summary>
    /// NickChangedEventArgs.
    /// </summary>
    public class NickChangedEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get old nickname.
        /// </summary>
        public string OldNick
        {
            get { return Data.Nick; }
        }

        /// <summary>
        /// Get new nickname.
        /// </summary>
        public string NewNick
        {
            get { return Data.Message; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public NickChangedEventArgs(MessageData data)
            : base(data)
        {
        }
    }

	#endregion // ModeChangedEventArgs

    /// <summary>
    /// ErrorEventArgs.
    /// </summary>
    public class ErrorEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get error string.
        /// </summary>
        public string Error { get { return m_error; } }
        string m_error;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ErrorEventArgs(MessageData data) : this(data, data.Message) {}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        /// <param name="error">Error string.</param>
        public ErrorEventArgs(MessageData data, string error) : base(data)
        {
            m_error = error;
        }
    }

	#region RegisteredEventArgs

    /// <summary>
    /// RegisteredEventArgs.
    /// </summary>
    public class RegisteredEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get your nickname.
        /// </summary>
        public string Nickname
        {
            get { return Data.Params[1]; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public RegisteredEventArgs(MessageData data)
            : base(data)
        {
        }
    }

	#endregion // RegisteredEventArgs	
	
	#region WhoEventArgs

    /// <summary>
    /// WhoEventArgs.
    /// </summary>
    public class WhoEventArgs : IrcEventArgs
    {
        string m_server;
        string m_realname;
        string m_usermode;
        int m_hopCount;

        /// <summary>
        /// Get server.
        /// </summary>
        public string Server
        {
            get { return m_server; }
        }

        /// <summary>
        /// Get real name.
        /// </summary>
        public string Realname
        {
            get { return m_realname; }
        }

        /// <summary>
        /// Get usermode.
        /// </summary>
        public string Usermode
        {
            get { return m_usermode; }
        }

        /// <summary>
        /// Get hop count.
        /// </summary>
        public int HopCount
        {
            get { return m_hopCount; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public WhoEventArgs(MessageData data)
        {
            m_data = new MessageData(data.IrcClient);
            m_data.Channel = data.Channel;
            m_data.Ident = data.Params[3];
            m_data.Host = data.Params[4];
            m_server = data.Params[5];
            m_data.Nick = data.Params[6];
            m_usermode = data.Params[7];
            m_hopCount = 0;

            if (data.Params.Length > 7)
            {
                string[] tmp = data.Params[8].Split(new char[] { ' ' }, 2);

                int.TryParse(tmp[0], out m_hopCount);
                m_realname = tmp.Length > 1 ? tmp[1] : string.Empty;
            }
            else
                m_realname = string.Empty;

            m_data.Params = data.Params;
            m_data.RawMessage = data.RawMessage;
        }
    }

	#endregion // WhoEventArgs	

	#region BanlistEventArgs

    /// <summary>
    /// BanlistEventArgs.
    /// </summary>
	public class BanlistEventArgs : IrcEventArgs
	{
		string m_ban;

        /// <summary>
        /// Get ban.
        /// </summary>
		public string Ban
		{
            get { return m_ban; }
        }

        BanInfo m_banInfo;

        /// <summary>
        /// Get ban info.
        /// </summary>
        public BanInfo BanInfo
        {
            get { return m_banInfo; }
        }

        /// <summary>
        /// Get channel name.
        /// </summary>
        public string Channel
        {
            get { return Data.Channel; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
		public BanlistEventArgs(MessageData data) : base(data)
		{
            StringBuilder ban = new StringBuilder();

            for (int i = 3; i < data.Params.Length; ++i)
            {
                ban.Append(data.Params[i]);

                if (i + 1 < data.Params.Length)
                    ban.Append(' ');
            }

            m_ban = ban.ToString();

            m_banInfo = new BanInfo(m_ban);
		}
	}

	#endregion

    #region BanlistEndEventArgs

    /// <summary>
    /// BanlistEndEventArgs.
    /// </summary>
    public class BanlistEndEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get channel name.
        /// </summary>
        public string Channel
        {
            get { return Data.Channel; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public BanlistEndEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    #endregion

	#region UserModeEventArgs

    /// <summary>
    /// UserModeEventArgs.
    /// </summary>
    public class UserModeEventArgs : IrcEventArgs, IUserEventArgs
    {
        /// <summary>
        /// Get user nickname.
        /// </summary>
        public string Who
        {
            get { return Data.Nick; }
        }

        /// <summary>
        /// Get user mode.
        /// </summary>
        public string Mode
        {
            get { return Data.Message; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public UserModeEventArgs(MessageData data)
            : base(data)
        {
        }
    }

	#endregion // UserModeEventArgs    

	#region ReadLineEventArgs

    /// <summary>
    /// ReadLineEventArgs.
    /// </summary>
	public class ReadLineEventArgs : EventArgs
	{
        string m_line;

        /// <summary>
        /// Get or set text.
        /// </summary>
		public string Line
		{
            get { return m_line; }
            set { m_line = value; }
		}

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="line">Text.</param>
		public ReadLineEventArgs(string line)
		{
			m_line = line;
		}
	}

	#endregion // ReadLineEventArgs

	#region WriteLineEventArgs

    /// <summary>
    /// WriteLineEventArgs.
    /// </summary>
	public class WriteLineEventArgs : EventArgs
	{
		string m_line;

        /// <summary>
        /// Get or set text.
        /// </summary>
		public string Line
		{
            get { return m_line; }
            set { m_line = value; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="line">Text.</param>
		public WriteLineEventArgs(string line)
		{
			m_line = line;
		}
	}

	#endregion // WriteLineEventArgs

    #region CtcpEventArgs

    /// <summary>
    /// CtcpEventArgs.
    /// </summary>
    public class CtcpEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get CTCP command.
        /// </summary>
        public string Command
        {
            get { return m_command; }
        }

        /// <summary>
        /// Get CTCP message.
        /// </summary>
        public string Message
        {
            get { return m_message; }
        }

        /// <summary>
        /// Get user nickname.
        /// </summary>
        public string Who
        {
            get { return Data.Nick; }
        }

        string m_command;
        string m_message;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="command">Command string.</param>
        /// <param name="message">Message.</param>
        /// <param name="data">MessageData.</param>
        public CtcpEventArgs(string command, string message, MessageData data)
            : base(data)
        {
            m_command = command;
            m_message = message;
        }
    }

    public class CtcpReplyEventArgs : CtcpEventArgs
    {
        public CtcpReplyEventArgs(string command, string message, MessageData data) : base(command, message, data) {}
    }
    
    public class CtcpRequestEventArgs : CtcpEventArgs
    {
        public CtcpRequestEventArgs(string command, string message, MessageData data) : base(command, message, data) {}
    }
    
    #endregion

    #region MotdEventArgs

    /// <summary>
    /// MotdEventArgs.
    /// </summary>
    public class MotdEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get motd lines.
        /// </summary>
        public string[] Motd
        {
            get { return m_motd; }
        }

        string[] m_motd;

        /// <summary>
        /// Contstructor.
        /// </summary>
        /// <param name="motd">Motd.</param>
        /// <param name="data">Data.</param>
        public MotdEventArgs(List<string> motd, MessageData data)
            : base(data)
        {
            m_motd = motd.ToArray();
        }
    }

    #endregion

    #region WhoIsEventArgs

    /// <summary>
    /// WhoIsInfo.
    /// </summary>
    public class WhoIsInfo
    {
        string m_nick;

        /// <summary>
        /// Get nickname.
        /// </summary>
        public string Nick
        {
            get { return m_nick; }
        }

        /// <summary>
        /// Get username.
        /// </summary>
        public string User
        {
            get { return m_user; }
            set { m_user = value; }
        }
        string m_user = "";

        /// <summary>
        /// Get server.
        /// </summary>
        public string Server
        {
            get { return m_server; }
            set { m_server = value; }
        }
        string m_server = "";

        /// <summary>
        /// Get hostname.
        /// </summary>
        public string Hostname
        {
            get { return m_hostname; }
            set { m_hostname = value; }
        }
        string m_hostname = "0.0.0.0";

        /// <summary>
        /// Get channels.
        /// </summary>
        public string Channels
        {
            get { return m_channels; }
            set { m_channels = value; }
        }
        string m_channels = "";

        /// <summary>
        /// Get operator status.
        /// </summary>
        public bool IsOperator
        {
            get { return m_isOperator; }
            set { m_isOperator = value; }
        }
        bool m_isOperator = false;

        /// <summary>
        /// Get idle time.
        /// </summary>
        public int Idle
        {
            get { return m_idle; }
            set { m_idle = value; }
        }
        int m_idle = 0;

        /// <summary>
        /// Get signon timestamp.
        /// </summary>
        public long SignOn
        {
            get { return m_signOn; }
            set { m_signOn = value; }
        }
        long m_signOn = 0;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="nick">Nickname.</param>
        public WhoIsInfo(string nick)
        {
            m_nick = nick;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class WhoIsEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Get nickname.
        /// </summary>
        public string Nick { get { return Data.Params[2]; }}

        /// <summary>
        /// Get username.
        /// </summary>
        public string User { get { return m_info.User; } }

        /// <summary>
        /// Get server.
        /// </summary>
        public string Server { get { return m_info.Server; } }

        /// <summary>
        /// Get hostname.
        /// </summary>
        public string Hostname { get { return m_info.Hostname; } }

        /// <summary>
        /// Get channels.
        /// </summary>
        public string Channels { get { return m_info.Channels; } }

        /// <summary>
        /// Get operator status.
        /// </summary>
        public bool IsOperator { get { return m_info.IsOperator; } }

        /// <summary>
        /// Get idle time.
        /// </summary>
        public int Idle { get { return m_info.Idle; } }

        /// <summary>
        /// Get signon timestamp.
        /// </summary>
        public long SignOn { get { return m_info.SignOn; } }

        WhoIsInfo m_info;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="info">Info.</param>
        /// <param name="data">MessageData.</param>
        public WhoIsEventArgs(WhoIsInfo info, MessageData data)
            : base(data)
        {
            m_info = info;
        }
    }

    #endregion

    #region AwayEventArgs

    /// <summary>
    /// AwayEventArgs.
    /// </summary>
    public class AwayEventArgs : IrcEventArgs, IUserEventArgs, IMessageEventArgs
    {
        /// <summary>
        /// Get message.
        /// </summary>
        public string Message
        {
            get
            {
                if(Data.Message != null)
                    return Data.Message;
                return string.Empty;
            }
        }

        /// <summary>
        /// Get nickname.
        /// </summary>
        public string Who
        {
            get { return Data.Nick; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public AwayEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    #endregion


    #region Channel
    
    public class ChannelEventArgs : IrcEventArgs, IChannelEventArgs
    {
        public string Channel
        {
            get { return m_data.Channel; }
        }

        public ChannelEventArgs(MessageData data) : base(data) {}
    }
    
    /// <summary>
    /// BanEventArgs.
    /// </summary>
    public class BanEventArgs : ChannelEventArgs, IUserEventArgs
    {
        /// <summary>
        /// Get banmask.
        /// </summary>
        public string BanMask
        {
            get { return m_banMask; }
        }

        /// <summary>
        /// Get user nickname.
        /// </summary>
        public string Who
        {
            get { return Data.Nick; }
        }

        private string m_banMask;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="banMask">Banmask.</param>
        /// <param name="data">MessageData.</param>
        public BanEventArgs(string banMask, MessageData data)
            : base(data)
        {
            m_banMask = banMask;
        }
    }
    
    public class JoinEventArgs : ChannelEventArgs, IUserEventArgs
    {
        public string Who
        {
            get { return m_data.Nick; }
        }

        public JoinEventArgs(MessageData data) : base(data) {}
    }

    public class PartEventArgs : ChannelEventArgs, IUserEventArgs, IMessageEventArgs
    {
        public string Message
        {
            get { return m_data.Message; }
        }

        public string Who
        {
            get { return m_data.Nick; }
        }

        public PartEventArgs(MessageData data) : base(data) {}
    }

    public class KickEventArgs : ChannelEventArgs, IUserEventArgs, IMessageEventArgs
    {
    	/// <summary>
    	/// User that's performing the kick.
    	/// </summary>
        public string Who
        {
            get { return m_data.Nick; }
        }

        /// <summary>
        /// The user that's been kicked
        /// </summary>
        public string Whom
        {
            get { return Data.Params[2]; }
        }

        /// <summary>
        /// Kick message
        /// </summary>
        public string Message
        {
            get { return Data.Message; }
        }

        public KickEventArgs(MessageData data) : base(data) {}
    }
    
    /// <summary>
    /// ChannelCreatedEventArgs.
    /// </summary>
    public class ChannelCreatedEventArgs : ChannelEventArgs
    {
        /// <summary>
        /// Get the timestamp when this channel was created.
        /// </summary>
        public DateTime Created
        {
            get { return m_created; }
        }
        DateTime m_created;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelCreatedEventArgs(MessageData data)
            : base(data)
        {
            long timeStamp = 0;
            long.TryParse(data.Params[data.Params.Length-1], out timeStamp);
            m_created = new System.DateTime(1970, 1, 1, 0, 0, 0, 0);
            m_created = m_created.AddSeconds(timeStamp);
        }
    }
    
    public class ChannelUserModeChangedEventArgs : ChannelEventArgs, IUserEventArgs
    {
        public string Who
        {
            get { return m_who; }
        }

        public string ChangedMode
        {
            get { return m_changedMode; }
        }

        public string FullMode
        {
            get { return m_mode; }
        }

        private string m_who;
        private string m_changedMode;
        private string m_mode;

        public ChannelUserModeChangedEventArgs(string who, string cmode, string mode, MessageData data)
            : base(data)
        {
            m_who = who;
            m_changedMode = cmode;
            m_mode = mode;
        }
    }

    /// <summary>
    /// ChannelTopicEventArgs.
    /// </summary>
    public class ChannelTopicEventArgs : ChannelEventArgs
    {
        /// <summary>
        /// Get the topic.
        /// </summary>
        public string Topic
        {
            get { return Data.Message; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelTopicEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    /// <summary>
    /// ChannelTopicChangedEventArgs.
    /// </summary>
    public class ChannelTopicChangedEventArgs : ChannelTopicEventArgs, IUserEventArgs
    {
        /// <summary>
        /// Get the nickname that set the topic.
        /// </summary>
        public string Who
        {
            get { return Data.Nick; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelTopicChangedEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    /// <summary>
    /// ChannelTopicSetByEventArgs.
    /// </summary>
    public class ChannelTopicSetByEventArgs : ChannelEventArgs, IUserEventArgs
    {
        /// <summary>
        /// Get the nickname that set the topic.
        /// </summary>
        public string Who
        {
            get { return Data.Params[3]; }
        }

        /// <summary>
        /// TimeStamp.
        /// </summary>
        public DateTime TimeStamp
        {
            get
            {
                long time = 0;
                long.TryParse(Data.Message, out time);
                return Helper.UnixTimeToDateTime(time);
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelTopicSetByEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    /// <summary>
    /// ChannelModeChangedEventArgs.
    /// </summary>
    public class ChannelModeChangedEventArgs : ChannelEventArgs, IUserEventArgs
    {
        /// <summary>
        /// Get mode as a string.
        /// </summary>
        public string Mode
        {
            get
            {
                string result = "";
                for (int i = 2; i < Data.Params.Length; ++i)
                    result += Data.Params[i] + " ";
                return result;
            }
        }

        /// <summary>
        /// Get the nickname who set this mode.
        /// </summary>
        public string Who
        {
            get
            {
                if (!string.IsNullOrEmpty(Data.Nick))
                    return Data.Nick;
                return Data.From;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelModeChangedEventArgs(MessageData data)
            : base(data)
        {
        }
    }


    /// <summary>
    /// ChannelModeIsEventArgs.
    /// </summary>
    public class ChannelModeIsEventArgs : ChannelEventArgs, IUserEventArgs
    {
        /// <summary>
        /// Get mode as a string.
        /// </summary>
        public string Mode
        {
            get
            {
                string result = "";
                for (int i = 3; i < Data.Params.Length; ++i)
                    result += Data.Params[i] + " ";
                return result;
            }
        }

        /// <summary>
        /// Who.
        /// </summary>
        public string Who
        {
            //TODO: Is this needed?
            get { return Data.Nick; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelModeIsEventArgs(MessageData data)
            : base(data)
        {
        }
    }
    
    #endregion // Channel
    
    #region Messages

    /// <summary>
    /// MessageEventArgs.
    /// </summary>
    public class MessageEventArgs : IrcEventArgs, IUserEventArgs, IMessageEventArgs, ITargetEventArgs
    {
        /// <summary>
        /// Target, channel-name or nickname.
        /// </summary>
        public virtual string Target
        {
            get
            {
                if (m_data.Channel != null && m_data.Channel.Length > 0)
                    return m_data.Channel;
                return Who;
            }
        }

        /// <summary>
        /// The user that's performing this event
        /// </summary>
        public virtual string Who
        {
            get
            {
                if (!string.IsNullOrEmpty(m_data.Nick))
                    return m_data.Nick;
                return m_data.From;
            }
        }

        /// <summary>
        /// Message.
        /// </summary>
        public virtual string Message
        {
            get { return m_data.Message; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public MessageEventArgs(MessageData data)
            : base(data)
        {
        }
    }
    
    #region Messages.Channel

    /// <summary>
    /// ChannelMessageEventArgs.
    /// </summary>
    public class ChannelMessageEventArgs : MessageEventArgs, IChannelEventArgs
    {
        /// <summary>
        /// Get channel.
        /// </summary>
        public string Channel
        {
            get { return m_data.Channel; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelMessageEventArgs(MessageData data) : base(data) {}
    }
    
    /// <summary>
    /// ChannelActionEventArgs. 
    /// </summary>
    public class ChannelActionEventArgs : ChannelMessageEventArgs
    {
        /// <summary>
        /// Get action message.
        /// </summary>
        public override string Message
        {
            get { return m_action; }
        }
        string m_action;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        /// <param name="action">Action.</param>
        public ChannelActionEventArgs(MessageData data, string action) : base(data)
        {
            m_action = action;
        }
    }

    /// <summary>
    /// ChannelNoticeEventArgs.
    /// </summary>
    public class ChannelNoticeEventArgs : ChannelMessageEventArgs
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public ChannelNoticeEventArgs(MessageData data) : base(data) {}
    }
    
    #endregion // Messages.Channel
    
    #region Messages.Query
    
    /// <summary>
    /// QueryActionEventArgs.
    /// </summary>
    public class QueryActionEventArgs : QueryMessageEventArgs
    {
        /// <summary>
        /// Get message (action).
        /// </summary>
        public override string Message
        {
            get { return m_action; }
        }
        string m_action;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        /// <param name="action">Action.</param>
        public QueryActionEventArgs(MessageData data, string action)
            : base(data)
        {
            m_action = action;
        }
    }
    
    /// <summary>
    /// QueryNoticeEventArgs.
    /// </summary>
    public class QueryNoticeEventArgs : QueryMessageEventArgs
    {
        /// <summary>
        /// Get notice. (Same as Message).
        /// </summary>
        public string Notice
        {
            get { return Data.Message; }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public QueryNoticeEventArgs(MessageData data)
            : base(data)
        {
        }
    }

    /// <summary>
    /// QueryMessageEventArgs.
    /// </summary>
    public class QueryMessageEventArgs : MessageEventArgs
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data">MessageData.</param>
        public QueryMessageEventArgs(MessageData data)
            : base(data)
        {
        }
    }
    
    #endregion // Messages.Query

    #endregion // Messages
    
    #region SSL

    /// <summary>
    /// RemoteCertificateEventArgs.
    /// </summary>
    public class RemoteCertificateEventArgs : EventArgs
    {
        X509Certificate m_certificate;

        /// <summary>
        /// Get the remote certificate.
        /// </summary>
        public X509Certificate RemoteCertificate
        {
            get { return m_certificate; }
        }

        X509Chain m_chain;

        /// <summary>
        /// Get the certificate chain.
        /// </summary>
        public X509Chain Chain
        {
            get { return m_chain; }
        }

        SslPolicyErrors m_sslPolicyErrors;

        /// <summary>
        /// Get policy errors.
        /// </summary>
        public SslPolicyErrors SslPolicyErrors
        {
            get { return m_sslPolicyErrors; }
        }

        bool m_accept;

        /// <summary>
        /// Get or set if this certificate should be accepted.
        /// </summary>
        public bool Accept
        {
            get { return m_accept; }
            set
            {
                m_accept = value;
                m_acceptSet = true;
            }
        }

        bool m_acceptSet = false;

        /// <summary>
        /// Get if Accept has been set.
        /// </summary>
        public bool AcceptSet
        {
            get { return m_acceptSet; }
        }

        /// <summary>
        /// Creates a new instance of RemoteCertificateEventArgs.
        /// </summary>
        /// <param name="certificate">Certificate.</param>
        /// <param name="chain">Chain.</param>
        /// <param name="sslPolicyErrors">Errors.</param>
        public RemoteCertificateEventArgs(X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            m_certificate = certificate;
            m_chain = chain;
            m_sslPolicyErrors = sslPolicyErrors;
        }
    }

    /// <summary>
    /// LocalCertificateEventArgs.
    /// </summary>
    public class LocalCertificateEventArgs : EventArgs
    {
        X509Certificate m_certificate;

        /// <summary>
        /// Get or set the certificate.
        /// </summary>
        public X509Certificate Certificate
        {
            get { return m_certificate; }
            set { m_certificate = value; }
        }

        string m_targetHost;

        /// <summary>
        /// Get target host.
        /// </summary>
        public string TargetHost
        {
            get { return m_targetHost; }
        }

        X509CertificateCollection m_localCertificates;

        /// <summary>
        /// Get local certificates.
        /// </summary>
        public X509CertificateCollection LocalCertificates
        {
            get { return m_localCertificates; }
        }

        X509Certificate m_remoteCertificate;

        /// <summary>
        /// Get remote certificates.
        /// </summary>
        public X509Certificate RemoteCertificate
        {
            get { return m_remoteCertificate; }
        }

        string[] m_acceptableIssuers;

        /// <summary>
        /// Get acceptable issuers.
        /// </summary>
        public string[] AcceptableIssuers
        {
            get { return m_acceptableIssuers; }
        }

        /// <summary>
        /// Creates a new instance of LocalCertificateEventArgs.
        /// </summary>
        /// <param name="targetHost">Target host.</param>
        /// <param name="localCertificates">Local certificates.</param>
        /// <param name="remoteCertificate">Remove certificates.</param>
        /// <param name="acceptableIssuers">Acceptable issuers.</param>
        public LocalCertificateEventArgs(string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            m_targetHost = targetHost;
            m_localCertificates = localCertificates;
            m_remoteCertificate = remoteCertificate;
            m_acceptableIssuers = acceptableIssuers;
        }
    }

    #endregion // SSL
    
    #region Errors

    /// <summary>
    /// ConnectionErrorEventArgs
    /// </summary>
    public class ConnectionErrorEventArgs : EventArgs
    {
        /// <summary>
        /// Error message.
        /// </summary>
        public string Error { get { return m_error; } }
        string m_error;

        /// <summary>
        /// Creates a new instance of ConnectionErrorEventArgs.
        /// </summary>
        /// <param name="error">Error message.</param>
        public ConnectionErrorEventArgs(string error)
        {
            m_error = error;
        }
    }
    
    public class ExceptionEventArgs : EventArgs
    {
    	public bool Handled
    	{
    		get { return m_handled; }
    		set { m_handled = value; }
    	}
    	bool m_handled;
    	
    	public Exception Exception { get { return m_exception; } }
    	Exception m_exception;
    	
    	public ExceptionEventArgs(Exception ex, bool handled)
    	{
    		m_exception = ex;
    		m_handled = handled;
    	}
    }
    
    #endregion // ExceptionEventArgs
    
    
    /// <summary>
    /// ChannelListEventArgs.
    /// </summary>
    public class ChannelListEventArgs : IrcEventArgs
    {
        /// <summary>
        /// Channel.
        /// </summary>
        public string Channel
        {
            get { return Data.Params[2]; }
        }

        /// <summary>
        /// Users.
        /// </summary>
        public int Users
        {
            get
            {
                int users = 0;
                int.TryParse(Data.Params[3], out users);
                return users;
            }
        }

        /// <summary>
        /// Channel topic.
        /// </summary>
        public string Topic
        {
            get { return Data.Message; }
        }

        /// <summary>
        /// Creates a new instance of the ChannelListEventArgs class.
        /// </summary>
        /// <param name="data">Message data.</param>
        public ChannelListEventArgs(MessageData data) : base(data) {}
    }
}
