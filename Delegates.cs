using System;

// Disable missing XML comment warning.
#pragma warning disable 1591

// TODO: Replace with generics
namespace Chipz.IRC
{
    /// <summary>
    /// HostInfoCallback.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="def">Default value.</param>
    /// <returns>Host information.</returns>
    public delegate HostInfo[] HostInfoCallback(object sender, HostInfo[] def);

    /// <summary>
    /// LoginInfoCallback.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="def">Default value.</param>
    /// <returns>Login information.</returns>
    public delegate LoginInfo LoginInfoCallback(object sender, LoginInfo def);

	// TODO: Remove
	/*public delegate void ReadLineEventHandler(object sender,ReadLineEventArgs e);
	public delegate void WriteLineEventHandler(object sender,WriteLineEventArgs e);

    public delegate void BanEventHandler(object sender, BanEventArgs e);

	public delegate void IrcEventHandler(object sender,IrcEventArgs e);
	public delegate void ErrorEventHandler(object sender,ErrorEventArgs e);
	public delegate void JoinEventHandler(object sender,JoinEventArgs e);
	public delegate void PartEventHandler(object sender,PartEventArgs e);
	public delegate void QuitEventHandler(object sender,QuitEventArgs e);
	public delegate void KickEventHandler(object sender,KickEventArgs e);
    public delegate void PingEventHandler(object sender, PingEventArgs e);
    public delegate void PongEventHandler(object sender, PongEventArgs e);
	public delegate void NamesEventHandler(object sender,NamesEventArgs e);
	public delegate void RegisteredEventHandler(object sender,RegisteredEventArgs e);
	public delegate void BanlistEventHandler(object sender,BanlistEventArgs e);
	public delegate void WhoEventHandler(object sender,WhoEventArgs e);
	public delegate void UserModeEventHandler(object sender,UserModeEventArgs e);
	public delegate void ModeChangedEventHandler(object sender,ModeChangedEventArgs e);
	public delegate void NickChangedEventHandler(object sender,NickChangedEventArgs e);

    public delegate void ChannelModeIsEventHandler(object sender, ChannelModeIsEventArgs e);
	public delegate void ChannelSyncedEventHandler(object sender,IrcEventArgs e);
    public delegate void ChannelModeChangedEventHandler(object sender, ChannelModeChangedEventArgs e);
	public delegate void ChannelMessageEventHandler(object sender,ChannelMessageEventArgs e);
	public delegate void ChannelNoticeEventHandler(object sender,ChannelNoticeEventArgs e);
	public delegate void ChannelActionEventHandler(object sender,ChannelActionEventArgs e);
    public delegate void ChannelUserModeChangedEventHandler(object sender, ChannelUserModeChangedEventArgs e);

	public delegate void QueryMessageEventHandler(object sender,QueryMessageEventArgs e);
	public delegate void QueryNoticeEventHandler(object sender,QueryNoticeEventArgs e);
	public delegate void QueryActionEventHandler(object sender,QueryActionEventArgs e);

    public delegate void CtcpEventHandler(object sender, CtcpEventArgs e);

    public delegate void MotdEventHandler(object sender, MotdEventArgs e);
    public delegate void WhoIsEventHandler(object sender, WhoIsEventArgs e);*/
}

#pragma warning restore 1591
