using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Text;
using Chipz.IRC.Collections;

namespace Chipz.IRC
{
    /// <summary>
    /// MultiEventHandler.
    /// Event names are case insensitive.
    /// </summary>
    /// <typeparam name="T">EventHandler type.</typeparam>
    public class MultiEventHandler<T> where T : EventArgs
    {
        SyncDictionary<string, EventHandler<T>> m_events = new SyncDictionary<string, EventHandler<T>>(new Dictionary<string, EventHandler<T>>(StringComparer.InvariantCultureIgnoreCase));

        /// <summary>
        /// Gets or sets an event.
        /// </summary>
        /// <param name="index">Event name.</param>
        /// <returns>EventHandler.</returns>
        public EventHandler<T> this[string index]
        {
            get
            {
                if (!m_events.ContainsKey(index))
                    m_events[index] = default(EventHandler<T>);
                return m_events[index];
            }
            set
            {
                m_events[index] = value;
            }
        }

        /// <summary>
        /// Get a list of all event names.
        /// </summary>
        /// <returns>Event names.</returns>
        public string[] GetKeys()
        {
            lock (m_events.SyncRoot)
            {                
                string[] result = new string[m_events.Count];
                m_events.Keys.CopyTo(result, 0);
                return result;
            }
        }
    }

}
