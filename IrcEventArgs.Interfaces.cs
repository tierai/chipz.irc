﻿
using System;

namespace Chipz.IRC
{
	public interface IIrcEventArgs
	{
		IrcClient IrcClient { get; }
		MessageData Data { get; }
	}

	public interface IChannelEventArgs : IIrcEventArgs
	{
		string Channel { get; }
	}
	
	public interface ITargetEventArgs : IIrcEventArgs
	{
		string Target { get; }
	}
	
	public interface IUserEventArgs : IIrcEventArgs
	{
		string Who { get; }
	}
	
	public interface IMessageEventArgs : IIrcEventArgs
	{
		string Message { get; }
	}
	
	public interface IModeEventArgs : IIrcEventArgs
	{
		string Mode { get; }
	}
}
