using System;
using System.IO;
using System.Text;
using System.Threading;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Chipz.IRC.Collections;

namespace Chipz.IRC
{
    /// <summary>
    /// Summary description for Client.
    /// </summary>
    public partial class IrcClient : IDisposable, IComponent
    {
        #region Properties & Variables

        /// <summary>
        /// Current index in the nickname array
        /// </summary>
        int m_nicknameIndex;

        /// <summary>
        /// Nicknames (stored from Login(...))
        /// </summary>
        string[] m_nicknames;

        /// <summary>
        /// Username (stored from Login(...))
        /// </summary>
        string m_username;

        /// <summary>
        /// Server password (stored from Login(...))
        /// </summary>
        string m_password;

        /// <summary>
        /// Real name (stored from Login(...))
        /// </summary>
        string m_realname; 
        
        /// <summary>
        /// Usermode (stored from Login(...))
        /// </summary>
        int m_usermode;

        /// <summary>
        /// Encoding to use. All incoming and outgoing text is encoded with this encoding.
        /// Note: You should leave this to the default latin1, which will work with most servers. See IrcClient2 for more information.
        /// </summary>
        [Category("Connection")]
        [DefaultValue(typeof(Encoding), "latin1")]
        public System.Text.Encoding Encoding    //FIXME: This property is not editable in the designer.
        {
            get { return m_encoding; }
            set
            {
                if (m_encoding != value)
                {
                    m_encoding = value;
                    if (Status != ConnectionStatus.Disconnected)  //This will prolly not work.
                        ResetStreamWriters();
                }
            }
        }
        Encoding m_encoding = Encoding.GetEncoding("latin1");

        /// <summary>
        /// The client will accept an invalid SSL certificate if true.
        /// </summary>
        [Category("SSL")]
        [DefaultValue(false)]
        public bool AcceptInvalidSslCertificate
        {
            get { return m_acceptInvalidSslCertificate; }
            set { m_acceptInvalidSslCertificate = value; }
        }
        bool m_acceptInvalidSslCertificate = false;

        /// <summary>
        /// The remote address that the client is connected to.
        /// </summary>
        [Obsolete("Use RemoteEndPoint", true)]
        [Browsable(false)]
        public string Address
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// The remote port that the client is connected to.
        /// </summary>
        [Obsolete("Use RemoteEndPoint", true)]
        [Browsable(false)]
        public int Port
        {
            get { return 0; }
        }

        /// <summary>
        /// Remote EndPoint.
        /// </summary>
        [Browsable(false)]
        public IPEndPoint RemoteEndPoint
        {
            get { return m_remoteEndPoint; }
        }
        IPEndPoint m_remoteEndPoint = null;

        /// <summary>
        /// True if the client is connected.
        /// </summary>
        [Browsable(false)]
        public bool IsConnected
        {
            get { return Status == ConnectionStatus.Connected; }
        }
        //bool m_isConnected = false;
        
        /// <summary>
        /// True if the client is connecting.
        /// </summary>
        [Browsable(false)]
        public bool IsConnecting
        {
            get { return Status == ConnectionStatus.Connecting; }
        }

        /// <summary>
        /// True if the client is disconnecting.
        /// </summary>
        [Browsable(false)]
        public bool IsDisconnecting
        {
            get { return Status == ConnectionStatus.Disconnecting; }
        }
        //bool m_isDisconnecting = false;

        /// <summary>
        /// True if the client is disconnected.
        /// </summary>
        [Browsable(false)]
        public bool IsDisconnected
        {
            get { return Status == ConnectionStatus.Disconnected; }
        }
        
        /// <summary>
        /// Client connection status.
        /// </summary>
        [Browsable(false)]
        public ConnectionStatus Status
        {
            get { lock(m_syncRoot) return m_status; }
        }
        ConnectionStatus m_status = ConnectionStatus.Disconnected;
        
        /// <summary>
        /// Gets or sets wether we should supress exceptions caused by user events or not.
        /// </summary>
        public bool SupressExceptions
        {
        	get { return m_supressExceptions; }
        	set { m_supressExceptions = value; }
        }
        bool m_supressExceptions = true;
        
        /// <summary>
        /// Returns the sync object for this IrcClient.
        /// </summary>
        public object SyncRoot
        {
        	get { return m_syncRoot; }
        }
        object m_syncRoot = new object();

        /// <summary>
        /// Socket send timeout in seconds.
        /// </summary>
        [Category("Connection")]
        [DefaultValue(120)]
        public int SocketSendTimeout
        {
            get { return m_socketSendTimeout; }
            set { m_socketSendTimeout = value; }
        }
        int m_socketSendTimeout = 120;

        /// <summary>
        /// Socket receive timeout in seconds.
        /// Should NOT be less than server PING intervals.
        /// </summary>
        [Category("Connection")]
        [DefaultValue(300)]
        public int SocketReceiveTimeout
        {
            get { return m_socketReceiveTimeout; }
            set { m_socketReceiveTimeout = value; }
        }
        int m_socketReceiveTimeout = 300;

        /// <summary>
        /// Get the socket stream.
        /// </summary>
        [Browsable(false)]
        public Stream Stream
        {
            get { return m_stream; }
        }

        /// <summary>
        /// Get the SSL stream when connected to a SSL server.
        /// </summary>
        [Browsable(false)]
        public SslStream SslStream
        {
            get { return m_sslStream; }
        }
        SslStream m_sslStream = null;

        IrcSocket m_socket;
        Thread m_readThread;
        Thread m_writeThread;
        Thread m_connectionKeeper;
        StreamWriter m_writer;
        StreamReader m_reader;
        Stream m_stream;
        Queue<string> m_sendQueue;
        Queue<string> m_readQueue;
        bool m_isConnectionError = false;
        bool m_useSSL = false;
        //bool m_debugReadQueue = false;
        //string m_debugReadQueueStart = "$$$DEBUG(";
        //string m_debugReadQueueEnd = ")END";

        /// <summary>
        /// Joined channels. SyncRoot should be locked when iterated.
        /// </summary>
        SyncDictionary<string, IrcChannel> m_channels;

        /// <summary>
        /// Known IRC users.
        /// This list will be populated as users join/part/quit channels, queries doesn't affect this list.
        /// SyncRoot should be locked when iterated.
        /// </summary>
        SyncDictionary<string, IrcUser> m_ircUsers;

        /// <summary>
        /// Hashtable containing all joined channels
        /// SyncRoot should be locked when iterated.
        /// </summary>
        [Browsable(false)]
        public SyncDictionary<string, IrcChannel> Channels { get { return m_channels; } }

        /// <summary>
        /// Hashtable containing all known IrcUsers
        /// SyncRoot should be locked when iterated.
        /// </summary>
        [Browsable(false)]
        public SyncDictionary<string, IrcUser> IrcUsers { get { return m_ircUsers; } }

        /// <summary>
        /// Returns the current nickname
        /// </summary>
        [Browsable(false)]
        public string Nickname { get { return m_nickname; } }
        string m_nickname = string.Empty;

        /// <summary>
        /// IsRegistered
        /// </summary>
        [Browsable(false)]
        public bool IsRegistered
        {
            get { return m_isRegistered; }
        }
        bool m_isRegistered = false;

        /// <summary>
        /// Rfc2812 commands 
        /// </summary>
        [Browsable(false)]
        public Rfc2812 Rfc { get { return m_rfc; } }
        Rfc2812 m_rfc;

        /// <summary>
        /// Get or sets client Name.
        /// </summary>
        public string Name
        {
            get { return m_name; }
            set { m_name = value; }
        }
        string m_name;

        /// <summary>
        /// Gets or sets userdata object.
        /// </summary>
        [DefaultValue(null)]
        public object Tag
        {
            get { return m_tag; }
            set { m_tag = value; }
        }
        object m_tag;

        /// <summary>
        /// Gets or sets internal flood-control.
        /// </summary>
        [Category("Connection")]
        [DefaultValue(true)]
        public bool FloodControl
        {
            get { return m_floodControl; }
            set { m_floodControl = value; }
        }
        bool m_floodControl = true;

        [Browsable(false)]
        public int Silence { get { return m_silence; } }
        int m_silence;

        [Browsable(false)]
        public int Modes { get { return m_modes; } }
        int m_modes;

        [Browsable(false)]
        public int MaxChannels { get { return m_maxChannels; } }
        int m_maxChannels;

        [Browsable(false)]
        public int MaxBans { get { return m_maxBans; } }
        int m_maxBans;

        [Browsable(false)]
        public int MaxNickLength { get { return m_maxNickLength; } }
        int m_maxNickLength;

        [Browsable(false)]
        public int MaxTopicLength { get { return m_maxTopicLength; } }
        int m_maxTopicLength;

        [Browsable(false)]
        public int MaxAwayLength { get { return m_maxAwayLength; } }
        int m_maxAwayLength;

        [Browsable(false)]
        public int MaxKickLength { get { return m_maxKickLength; } }
        int m_maxKickLength;

        [Browsable(false)]
        public char[] ChannelTypes { get { return m_channelTypes; } }
        char[] m_channelTypes;

        [Browsable(false)]
        public string CaseMapping { get { return m_caseMapping; } }
        string m_caseMapping;

        /// <summary>
        /// Gets the name of the network that's reported by the server.
        /// Will return an empty string if no network is set.
        /// </summary>
        [Browsable(false)]
        public string Network { get { return m_network; } }
        string m_network;

        /// <summary>
        /// Gets channel user-prefixes, ex. @ for op.
        /// The indices in this array is linked to ChannelUserModes.
        /// </summary>
        [Browsable(false)]
        public char[] ChannelUserPrefixes
        {
            get
            {
                lock (m_channelUserPrefixes.SyncRoot)
                {
                    char[] result = new char[m_channelUserPrefixes.Count];
                    m_channelUserPrefixes.Values.CopyTo(result, 0);
                    return result;
                }
            }
        }
        internal SyncDictionary<char, char> m_channelUserPrefixes = new SyncDictionary<char, char>(new Dictionary<char, char>());

        /// <summary>
        /// Gets available channel user-modes, ex 'o' for op.
        /// The modes are sorted after how powerful they are. (0='o', 1='v').
        /// The indices in this array is linked to ChannelUserPrefixes.
        /// </summary>
        [Browsable(false)]
        public char[] ChannelUserModes
        {
            get
            {
                lock (m_channelUserPrefixes.SyncRoot)
                {
                    char[] result = new char[m_channelUserPrefixes.Count];
                    m_channelUserPrefixes.Keys.CopyTo(result, 0);
                    return result;
                }
            }
        }

        /// <summary>
        /// Get a list of A modes.
        /// A = Mode that adds or removes a nick or address to a list. Always has a parameter.
        /// </summary>
        [Browsable(false)]
        public char[] ChannelModesA { get { return m_channelModesA.ToArray(); } }
        SyncList<char> m_channelModesA = new SyncList<char>(new List<char>());

        /// <summary>
        /// Get a list of B modes.
        // B = Mode that changes a setting and always has a parameter.
        /// </summary>
        [Browsable(false)]
        public char[] ChannelModesB { get { return m_channelModesB.ToArray(); } }
        SyncList<char> m_channelModesB = new SyncList<char>(new List<char>());

        /// <summary>
        /// Get a list of C modes.
        /// C = Mode that changes a setting and only has a parameter when set.
        /// </summary>
        [Browsable(false)]
        public char[] ChannelModesC { get { return m_channelModesC.ToArray(); } }
        SyncList<char> m_channelModesC = new SyncList<char>(new List<char>());

        /// <summary>
        /// Get a list of D modes.
        /// D = Mode that changes a setting and never has a parameter.
        /// </summary>
        [Browsable(false)]
        public char[] ChannelModesD { get { return m_channelModesD.ToArray(); } }
        SyncList<char> m_channelModesD = new SyncList<char>(new List<char>());

        /// <summary>
        /// Gets or sets wether IrcClient trace should be enabled.
        /// </summary>
        [Category("Connection")]
        [DefaultValue(true)]
        public bool TraceEnabled
        {
            get { return m_traceEnabled; }
            set { m_traceEnabled = value; }
        }
        bool m_traceEnabled = true;

        /// <summary>
        /// Gets or sets a list of enabled SSL protocols.
        /// </summary>
        [Category("SSL")]
       // [DefaultValue(typeof(SslProtocols), "Default")]
        public SslProtocols EnabledSslProtocols
        {
            get { return m_enabledProtocols; }
            set { m_enabledProtocols = value; }
        }
        SslProtocols m_enabledProtocols = SslProtocols.Default;

        /// <summary>
        /// Gets or sets available client certificates.
        /// </summary>
        [Category("SSL")]
        public X509CertificateCollection ClientCertificates
        {
            get { return m_clientCertificates; }
            set { m_clientCertificates = value; }
        }
        X509CertificateCollection m_clientCertificates = new X509CertificateCollection();

        /// <summary>
        /// Gets or sets if the clients should use OpenSSL.
        /// NOTE: OpenSSL should only be used if the server doesn't work with SAPI.
        /// NOTE: SslStream will always be null. Local certificates & cert callbacks doesn't work.
        /// </summary>
        [Category("SSL")]
        [DefaultValue(false)]
        public bool UseOpenSsl
        {
            get { return m_useOpenSsl; }
            set { m_useOpenSsl = value; }
        }
        bool m_useOpenSsl = false;

        /// <summary>
        /// Gets or sets wether clients should use OpenSSL if normal SSL fails.
        /// </summary>
        [Category("SSL")]
        [DefaultValue(false)]
        public bool OpenSslFallback
        {
            get { return m_openSslFallback; }
            set { m_openSslFallback = value; }
        }
        bool m_openSslFallback = false;

        /// <summary>
        /// Gets or sets CheckCertificateRevocation.
        /// </summary>
        [Category("SSL")]
        [DefaultValue(true)]
        public bool CheckCertificateRevocation
        {
            get { return m_checkCertificateRevocation; }
            set { m_checkCertificateRevocation = value; }
        }
        bool m_checkCertificateRevocation = true;

        /// <summary>
        /// Returns true if the read-queue is not empty.
        /// </summary>
        public bool DataAvailable
        {
            get { lock (m_readQueue) return m_readQueue.Count > 0; }
        }

        #endregion //Properties

        #region Threads

        /// <summary>
        /// Gets or sets the priority the reader and writer threads should have.
        /// Priorities will not change if already connected.
        /// You really shouldn't change this unless you really know what you're doing.
        /// </summary>
        [Category("Threads")]
        [DefaultValue(ThreadPriority.Normal)]
        public ThreadPriority ThreadPriority
        {
            get { return m_threadPriority; }
            set { m_threadPriority = value; }
        }
        ThreadPriority m_threadPriority = ThreadPriority.Normal;

        /// <summary>
        /// Sets or gets wheter a read thread should be used.
        /// Must be set before connecting.
        /// </summary>
        [Category("Threads")]
        [DefaultValue(true)]
        public bool UseReadThread
        {
            get { return m_useReadThread; }
            set { m_useReadThread = value; }
        }
        bool m_useReadThread = true;

        /// <summary>
        /// Sets or gets wheter a write thread should be used.
        /// Must be set before connecting.
        /// </summary>
        [Category("Threads")]
        [DefaultValue(true)]
        public bool UseWriteThread
        {
            get { return m_useWriteThread; }
            set { m_useWriteThread = value; }
        }
        bool m_useWriteThread = true;

        #endregion

        #region WriteThread properties

        /// <summary>
        /// Time to before processing the next item in queue.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(10)]
        int WriteThreadActiveSleep
        {
            get { return m_writeSleep; }
            set { m_writeSleep = value; }
        }
        int m_writeSleep = 10;

        /// <summary>
        /// Time to sleep when queue is empty.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(100)]
        int WriteThreadIdleSleep
        {
            get { return m_writeIdleSleep; }
            set { m_writeIdleSleep = value; }
        }
        int m_writeIdleSleep = 100;

        /// <summary>
        /// Time before flood control is reset.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(60000)]
        int WriteThreadFloodControlTimeout
        {
            get { return m_writeFloodControlTimeout; }
            set { m_writeFloodControlTimeout = value; }
        }
        int m_writeFloodControlTimeout = 60000;

        /// <summary>
        /// Flood control multipler (sleep = bytes * mult).
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(15)]
        int WriteThreadFloodControlMult
        {
            get { return m_writeFloodControlMult; }
            set { m_writeFloodControlMult = value; }
        }
        int m_writeFloodControlMult = 15; 

        #endregion

        #region Read properties

        /// <summary>
        /// Time to sleep before reading next line from the server.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(20)]
        public int ReadThreadSleep
        {
            get { return m_readSleep; }
            set { m_readSleep = value; }
        }
        int m_readSleep = 20;

        /// <summary>
        /// Time to sleep while waiting for data.
        /// Applies to ListenOnce(true) only.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(10)]
        [Obsolete("ListenOnce should not be used anymore, see ProcessReadQueue or ProcessReadQueueSleep.")]
        public int ReadLinesBlockingSleep
        {
            get { return m_readLinesBlockingSleep; }
            set { m_readLinesBlockingSleep = value; }
        }
        int m_readLinesBlockingSleep = 10;

        /// <summary>
        /// Time to sleep before triggering the next OnLineRead event.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(10)]
        public int ProcessReadQueueSleep
        {
            get { return m_processReadQueueSleep; }
            set { m_processReadQueueSleep = value; }
        }
        int m_processReadQueueSleep = 10;

        #endregion

        #region ConnectionKeeper properties

        /// <summary>
        /// True if AutoReconnect is enabled.
        /// </summary>
        [Browsable(false)]
        public bool IsAutoConnecting
        {
            get; protected set;
        }
        
        /// <summary>
        /// Milliseconds to wait when there are messages waiting.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(20)]
        [Obsolete("Not in use anymore")]
        public int ConnectionKeeperActiveSleep
        {
            get { return m_connectionKeeperActiveSleep; }
            set { m_connectionKeeperActiveSleep = value; }
        }
        int m_connectionKeeperActiveSleep = 20;

        /// <summary>
        /// Milliseconds to wait when the ReadQueue is empty.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(50)]
        public int ConnectionKeeperIdleSleep
        {
            get { return m_connectionKeeperIdleSleep; }
            set { m_connectionKeeperIdleSleep = value; }
        }
        int m_connectionKeeperIdleSleep = 50;

        /// <summary>
        /// Number of items to process before sleeping.
        /// </summary>
        [Category("Advanced")]
        [DefaultValue(10)]
        public int ConnectionKeeperDequeueCount
        {
            get { return m_connectionKeeperDequeueCount; }
            set { m_connectionKeeperDequeueCount = value; }
        }
        int m_connectionKeeperDequeueCount = 10;

        #endregion

        #region Constructor

        /// <summary>
        /// Creates a new instance of the IrcClient class.
        /// </summary>
        public IrcClient()
            : this(null)
        { }

        /// <summary>
        /// Creates a new instance of the IrcClient class.
        /// </summary>
        /// <param name="name">IrcClient name.</param>
        public IrcClient(string name)
            : this(name, false)
        { }

        static uint _Counter = 0;

        /// <summary>
        /// Creates a new instance of the IrcClient class.
        /// </summary>
        /// <param name="name">IrcClient name.</param>
        /// <param name="noInternalCtcpHandlers">Sets wether internal CTCP handlers should be used.</param>
        public IrcClient(string name, bool noInternalCtcpHandlers)
        {
            if (name == null)
                name = "IrcClient #" + _Counter++;

            m_name = name;
            m_channels = new SyncDictionary<string, IrcChannel>(new Dictionary<string, IrcChannel>(StringComparer.InvariantCultureIgnoreCase));
            m_ircUsers = new SyncDictionary<string, IrcUser>(new Dictionary<string, IrcUser>(StringComparer.InvariantCultureIgnoreCase));
            m_rfc = new Rfc2812(this);

            if (!noInternalCtcpHandlers)
                CreateInternalCtcpHandlers();

            System.Reflection.AssemblyName aname = System.Reflection.Assembly.GetExecutingAssembly().GetName();
            Trace("Version = " + aname.Version.ToString());
        }

        #endregion

        #region Trace

        internal void Trace(object obj)
        {
            if (m_traceEnabled)
                System.Diagnostics.Trace.WriteLine(obj, m_name);
        }

        internal void Trace(string text)
        {
            if (m_traceEnabled)
                System.Diagnostics.Trace.WriteLine(text, m_name);
        }

        #endregion

        #region Start

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connectInfo"></param>
        public void Start(bool keep)
        {
            Start(new ConnectInfo(keep), false);
        }

        /// <summary>
        /// Connect to an IRC server and let the IrcClient handle reconnects.
        /// You should NEVER call Connect when using this method.
        /// </summary>
        /// <param name="connectInfo">Connection information.</param>
        public void Start(ConnectInfo connectInfo)
        {
            Start(connectInfo, false);
        }

        /// <summary>
        /// Connect to an IRC server and let the IrcClient handle reconnects.
        /// You should NEVER call Connect when using this method.
        /// </summary>
        /// <param name="connectInfo"></param>
        /// <param name="useSeparateThread"></param>
        public void Start(ConnectInfo connectInfo, bool useSeparateThread)
        {
            if (Status != ConnectionStatus.Disconnected)
                throw new AlreadyConnectedException();

            try
            {
                IsAutoConnecting = true;
                
                if (useSeparateThread)
                {
                    SafeAbort(ref m_connectionKeeper);
    
                    m_connectionKeeper = new Thread(new ParameterizedThreadStart(ConnectionKeeper));
                    m_connectionKeeper.IsBackground = true;
                    m_connectionKeeper.Name = Name + "_ConnectionKeeperThread";
                    m_connectionKeeper.Start(connectInfo);
                }
                else
                {
                    ConnectionKeeper(connectInfo);
                }
            }
            catch (Exception)
            {
                IsAutoConnecting = false;
                throw;
            }
        }

        #endregion

        #region ConnectionKeeper

        /// <summary>
        /// Connection main loop
        /// TODO: Allow user to specify if we should reconnect or cancel? (in OnConnectionError or other event).
        /// </summary>
        /// <param name="param">A ConnectInfo instance</param>
        protected void ConnectionKeeper(object param)
        {
            Random rand = new Random();
            int reconnectCount = 0;
            int hostIdx = 0;
            int portIdx = 0;
            ConnectInfo connectInfo = param as ConnectInfo;

            Trace("ConnectionKeeper: Start");
            
            Debug.Assert(IsAutoConnecting);

            while (true)
            {
                try
                {
                    HostInfo[] hosts = OnGetHostInfo(connectInfo.Hosts);
                    
                    if (hosts.Length < 1)
                    	throw new InvalidOperationException("OnGetHostInfo should return one or more HostInfo objects.");
                    
                    if (hostIdx >= hosts.Length)
                    {
                        hostIdx = 0;
                        portIdx = 0;
                    }

                    HostInfo host = hosts[hostIdx];
                    int port = Rfc2812.DefaultPort;

                    if (host.Ports.Length > 0)
                    {
	                    if (portIdx + 1 >= host.Ports.Length)
	                    {
	                        hostIdx++;
	                        portIdx = 0;
	                    }
	                    else
	                    {
	                        portIdx++;
	                    }
	                    port = host.Ports[portIdx];
                    }
                    
                    IPAddress[] addresses = Dns.GetHostAddresses(host.Hostname);
                    IPAddress address = addresses[rand.Next(0, addresses.Length - 1)];
                    IPEndPoint endPoint = new IPEndPoint(address, port);

                    Connect(endPoint, host.Ssl);

                    LoginInfo loginInfo = OnGetLoginInfo(connectInfo.LoginInfo);

                    if (loginInfo == null)
                        throw new InvalidOperationException("LoginInfo cannot be null, you must provide an event handler or a valid LoginInfo object to ConnectInfo.");

                    Login(loginInfo.Nicknames, loginInfo.Username, loginInfo.Realname, loginInfo.Usermode, loginInfo.Password);

                    reconnectCount = 0; // Reset reconnectCount

                    while (true)
                    {
                        Update(0, 0);
                        //Thread.Sleep(DataAvailable ? m_connectionKeeperActiveSleep : m_connectionKeeperIdleSleep);
                        Thread.Sleep(m_connectionKeeperIdleSleep);
                    }
                }
                catch (ThreadAbortException)
                {
                    Trace("ConnectionKeeper: Thread aborted");
                    break;
                }
                catch (InvalidOperationException ex)
                {
                	Trace(ex);
                    OnConnectionError(ex.Message);
                	throw;
                }
                catch (Exception ex)
                {
                    Trace(ex);
                    OnConnectionError(ex.Message);
                }

                if (!connectInfo.Keep)
                {
                    Trace("ConnectKeeper: Keep = false");
                    break;
                }

                reconnectCount++;

                if (connectInfo.ReconnectLimit > 0 && reconnectCount >= connectInfo.ReconnectLimit)
                {
                    Trace("ConnectionKeeper: ReconnectLimit reached (" + reconnectCount + " >= " + connectInfo.ReconnectLimit + ")");
                    break;
                }

                Thread.Sleep(connectInfo.ReconnectDelay * 1000);
            }
            
            IsAutoConnecting = false;
            
            Trace("ConnectionKeeper: Exit");
        }

        #endregion

        #region Connect

        /// <summary>
        /// Connect to an IRC server.
        /// </summary>
        /// <param name="address">A valid hostname or ip-address.</param>
        /// <param name="port">Server port, usually 6667.</param>
        public void Connect(string address, int port)
        {
            Connect(address, port, false);
        }

        /// <summary>
        /// Connect to an IRC server.
        /// </summary>
        /// <param name="address">A valid hostname or ip-address.</param>
        /// <param name="port">Server port, usually 6667.</param>
        /// <param name="ssl">Enable Secure Sockets Layer.</param>
        public void Connect(string address, int port, bool ssl)
        {
            IPAddress ipAddress = Dns.GetHostEntry(address).AddressList[0];
            IPEndPoint endPoint = new IPEndPoint(ipAddress, port);
            Connect(endPoint, ssl);
        }

        /// <summary>
        /// Connect to an IRC server.
        /// </summary>
        /// <param name="endPoint">Remote end-point.</param>
        /// <param name="ssl">Enable Secure Sockets Layer.</param>
        public void Connect(IPEndPoint endPoint, bool ssl)
        {
            if (IsDisposed)
                throw new ObjectDisposedException(Name);

            if (Status != ConnectionStatus.Disconnected)
                throw new AlreadyConnectedException();

            OnConnecting(new EventArgs());

            try
            {
                Trace("Connecting to " + endPoint.ToString() + "...");

                m_remoteEndPoint = endPoint;
                m_useSSL = ssl;
                
                CreateSocket();

                m_isConnectionError = false;
                m_sslStream = null;
                m_stream = null;

                m_sendQueue = new Queue<string>();
                m_readQueue = new Queue<string>();

                if (m_useSSL)
                {
                    if (OpenSslFallback)
                    {
                        try
                        {
                            SslConnect(false);
                        }
                        catch (Exception)
                        {
                            CreateSocket();
                            SslConnect(true);
                        }
                    }
                    else
                    {
                        SslConnect(UseOpenSsl);
                    }

                    Trace("SSL authentication OK");
                }
                else
                {
                    m_stream = m_socket.GetStream();
                }

                m_socket.ReceiveTimeout = m_socketReceiveTimeout * 1000;
                m_socket.SendTimeout = m_socketSendTimeout * 1000;

                ResetStreamWriters();

                StartThreads();

                OnConnected(null);

                Trace("Connected");
            }
            catch (Exception ex)
            {
                Trace(ex);

                m_isRegistered = false;
                m_isConnectionError = true;

                DestroyThreads();
                DestroySocket();

                m_sendQueue = null;
                m_readQueue = null;

                m_status = ConnectionStatus.Disconnected;

                OnStatusChanged(null);

                throw new CouldNotConnectException("Could not connect to " + endPoint.ToString() + ", " + ex.Message, ex);
            }
        }

        void CreateSocket()
        {
            m_socket = new IrcSocket(m_remoteEndPoint.AddressFamily);
            m_socket.ReceiveTimeout = 120 * 1000;
            m_socket.SendTimeout = 120 * 1000;
            m_socket.Connect(m_remoteEndPoint);
        }

        void SslConnect(bool openSsl)
        {
            try
            {
                if (openSsl)
                    OpenSslConnect();
                else
                    SslConnect();
            }
            catch (AuthenticationException ex)
            {
                Trace("SSL authentication failed: " + ex.Message);
                throw;
            }
            catch (IOException ex)
            {
                Trace("Connection failed: " + ex.Message);
                throw;
            }
        }

        void OpenSslConnect()
        {
            Trace("Using OpenSSL");
            Chipz.OpenSsl.OpenSslStream sslStream = new Chipz.OpenSsl.OpenSslStream(m_socket.Client, m_enabledProtocols, true);
            sslStream.Authenticate();
            m_stream = sslStream;
        }

        void SslConnect()
        {
            Trace("Using SSL");
            SslStream sslStream = new SslStream(m_socket.GetStream(), false, RemoteCertificateValidationCallback, LocalCertificateSelectionCallback);
            sslStream.AuthenticateAsClient(m_remoteEndPoint.Address.ToString(), m_clientCertificates, m_enabledProtocols, m_checkCertificateRevocation);
            m_sslStream = sslStream;
            m_stream = sslStream;
        }

        #endregion

        #region Disconnect

        /// <summary>
        /// Disconnect the client.
        /// </summary>
        public void Disconnect()
        {
            Disconnect(true);
        }
        
        /// <summary>
        /// Disconnect the client.
        /// </summary>
        /// <param name="flushOutput">Flush the output buffer</param>
        public void Disconnect(bool flushOutput)
        {
            if (IsAutoConnecting)
            {
                SafeAbort(ref m_connectionKeeper);
                IsAutoConnecting = false;
            }
            
            if (Status == ConnectionStatus.Disconnected)
            {
                return;
                //throw new NotConnectedException();
            }
            
            if (Status == ConnectionStatus.Disconnecting)
            {
                //throw new Exception("Already disconnecting!");
                return;
            }

            OnDisconnecting(null);

            try
            {
                if (m_writer != null)
                {
                    if (flushOutput)
                    {
                        lock (m_sendQueue)
                        {
                            while (m_sendQueue.Count > 0)
                            {
                                string data = m_sendQueue.Dequeue();
                                m_writer.WriteLine(data);
                            }
                        }
                    }

                    m_writer.Flush();

                    lock (m_sendQueue)
                        m_sendQueue.Clear();

                    lock (m_readQueue)
                        m_readQueue.Clear();
                }
            }
            finally
            {
                Trace("Disconnected");
                OnDisconnected(null);
            }
        }

        #endregion

        #region WriteLine

        /// <summary>
        /// Write raw data to the server. The data is enqueued and will be sent later.
        /// </summary>
        /// <param name="data">Data to send to the server, cannot be null.</param>
        public void WriteLine(string data)
        {
            if (!IsConnected)
                throw new NotConnectedException();

            WriteLineEventArgs e = new WriteLineEventArgs(data);
            OnLineWrite(e);

            lock (m_sendQueue)
                m_sendQueue.Enqueue(e.Line);
        }
        #endregion

        #region ListenOnce (Obsolete)

        /// <summary>
        /// Listen once. Non-blocking.
        /// </summary>
        [Obsolete("Use ProcessReadQueue.")]
        public void ListenOnce()
        {
            ListenOnce(false);
        }

        /// <summary>
        /// Listen once.
        /// </summary>
        /// <param name="blocking">Wait for data to be available.</param>
        [Obsolete("Use ProcessReadQueue.")]
        public void ListenOnce(bool blocking)
        {
            if (!IsConnected)
                throw new NotConnectedException();
            ReadLines(blocking);
        }

        #endregion

        #region ReadLines (Obsolete)

        /// <summary>
        /// Dequeue data from read buffer.
        /// </summary>
        /// <param name="blocking">Wait for data to be available.</param>
        [Obsolete("Use ProcessReadQueue.")]
        private void ReadLines(bool blocking)
        {
            if (blocking)
            {
                while (IsConnected)
                {
                    lock (m_readQueue)
                    {
                        if (m_readQueue.Count > 0)
                            break;
                    }
                    Thread.Sleep(m_readLinesBlockingSleep);
                }
            }

            ProcessReadQueue(1);
        }

        #endregion

        #region Update

        /// <summary>
        /// Updates everything.
        /// You can use ProcessReadQueue instead if you're using threaded reading & writing.
        /// </summary>
        /// <param name="readLimit">Read limit.</param>
        /// <param name="writeLimit">Write limit, has no effect when using threaded writing.</param>
        public void Update(int readLimit, int writeLimit)
        {
            if (!IsConnected)
                throw new NotConnectedException();

            if (!m_useReadThread)
                ReadAndProcessData(readLimit);

            if (!m_useWriteThread)
                WriteData(writeLimit);

            if (m_useReadThread)
                ProcessReadQueue(readLimit);
        }
          
        #endregion

        #region ReadAndProcessData

        /// <summary>
        /// Reads data from server and processes it.
        /// Should NOT be called if using a read-thread.
        /// </summary>
        /// <param name="limit">Max number of lines to read, 0 = infinite.</param>
        public void ReadAndProcessData(int limit)
        {
            if (!IsConnected)
                throw new NotConnectedException();

            while (true)
            {
                string data = m_reader.ReadLine();

                if (string.IsNullOrEmpty(data))
                    break;

                OnLineRead(new ReadLineEventArgs(data));

                if (limit > 0 && --limit < 1)
                    break;
            }

            if (m_isConnectionError)
                OnConnectionError("Read error");
        }

        #endregion

        #region WriteData (FIXME: IMPLEMENT!)

        /// <summary>
        /// Writes data from the write-queue.
        /// Should NOT be called if using a write-thread.
        /// </summary>
        /// <param name="limit">Max number of lines to write, 0 = entire queue.</param>
        public void WriteData(int limit)
        {
            if (!IsConnected)
                throw new NotConnectedException();

            throw new /* FIXME: NotImplemented!! */ NotImplementedException("Non threaded writing has not been implemented (IrcClient.WriteData)");
        }

        #endregion

        #region ProcessReadQueue

        /// <summary>
        /// Dequeues data from the read queue and processes them.
        /// Should only be called when using a read-thread.
        /// </summary>
        /// <param name="limit">Max number of items to dequeue, 0 = infinite.</param>
        /// <returns>Number of items processed.</returns>
        public int ProcessReadQueue(int limit)
        {
            if (!IsConnected)
                throw new NotConnectedException();

            int count = 0;

            if (!m_isConnectionError)
            {
                string[] data = null;

                lock (m_readQueue)
                {
                    if (m_readQueue.Count > 0)
                    {
                        count = limit > 0 && limit < m_readQueue.Count ? limit : m_readQueue.Count;

                        if (count != m_readQueue.Count)
                        {
                            data = new string[count];
                            for (int i = 0; i < count; i++)
                                data[i] = m_readQueue.Dequeue();
                        }
                        else
                        {
                            data = m_readQueue.ToArray();
                            m_readQueue.Clear();
                        }
                    }
                }

                if (data != null)
                {
                    /*if (m_debugReadQueue)
                    {
                        foreach (string dta in data)
                        {
                            if (dta.StartsWith(m_debugReadQueueStart))
                            {
                                string tmp = dta.Substring(m_debugReadQueueStart.Length);
                                int idx = tmp.IndexOf(m_debugReadQueueEnd);
                                string tmp2 = tmp.Substring(0, idx);
                                tmp = tmp.Substring(idx + m_debugReadQueueEnd.Length);

                                long x = 0;
                                if (long.TryParse(tmp2, out x))
                                {
                                    long delta = DateTime.Now.Ticks - x;
                                    TimeSpan ts = TimeSpan.FromTicks(delta);
                                    Trace("ReadQueue: TiQ=" + ts.ToString() + " - " + tmp);
                                    if (ts.TotalSeconds > 5)
                                        Trace("^^^ WARNING: >5 seconds in queue!");
                                }

                                OnLineRead(new ReadLineEventArgs(tmp));
                            }
                            else
                                OnLineRead(new ReadLineEventArgs(dta));
                        }
                        Thread.Sleep(m_processReadQueueSleep);
                    }
                    else*/
                    {
                        foreach (string dta in data)
                        {

                            OnLineRead(new ReadLineEventArgs(dta));
                            Thread.Sleep(m_processReadQueueSleep);
                        }
                    }
                }
            }

            if (m_isConnectionError)
                OnConnectionError("Read error");

            return count;
        }

        #endregion

        #region RemoteCertificateValidationCallback

        bool RemoteCertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            RemoteCertificateEventArgs e = new RemoteCertificateEventArgs(certificate, chain, sslPolicyErrors);

            OnRemoteCertificateValidation(e);

            if (e.AcceptSet)
            {
                if (e.Accept)
                {
                    Trace("SSL certificate accepted");
                    return true;
                }
                else
                {
                    Trace("SSL certificate validation aborted");
                    return false;
                }
            }

            if (sslPolicyErrors == SslPolicyErrors.None)
                return true;

            Trace("SSL Certificate error: " + sslPolicyErrors.ToString());

            if (m_acceptInvalidSslCertificate)
                return true;

            OnConnectionError("Invalid server certificate, errors: " + sslPolicyErrors.ToString());
            return false;
        }
  
        #endregion

        #region LocalCertificateSelectionCallback

        X509Certificate LocalCertificateSelectionCallback(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            LocalCertificateEventArgs e = new LocalCertificateEventArgs(targetHost, localCertificates, remoteCertificate, acceptableIssuers);

            OnLocalCertificateValidation(e);

            if (e.Certificate != null)
                return e.Certificate;

            if (acceptableIssuers != null && acceptableIssuers.Length > 0 && localCertificates != null && localCertificates.Count > 0)
            {
                foreach (X509Certificate certificate in localCertificates)
                {
                    string issuer = certificate.Issuer;
                    if (Array.IndexOf(acceptableIssuers, issuer) != -1)
                        return certificate;
                }
            }

            if (localCertificates != null && localCertificates.Count > 0)
                return localCertificates[0];
 
            return null;
        }

        #endregion

        #region Misc

        private void ResetStreamWriters()
        {
            if (m_encoding != null)
            {
                m_writer = new StreamWriter(m_stream, m_encoding);
                m_reader = new StreamReader(m_stream, m_encoding);
            }
            else
            {
                m_writer = new StreamWriter(m_stream);
                m_reader = new StreamReader(m_stream);
            }
        }

        private void StartThreads()
        {
            Debug.Assert(m_readThread == null);
            Debug.Assert(m_writeThread == null);

            if (m_useReadThread)
            {
                m_readThread = new Thread(new ThreadStart(ReadProc));
                m_readThread.IsBackground = true;
                m_readThread.Priority = ThreadPriority;
                m_readThread.Name = "ReadThread[" + Name + "]";
                m_readThread.Start();
            }

            if (m_useWriteThread)
            {
                m_writeThread = new Thread(new ThreadStart(WriteProc));
                m_writeThread.IsBackground = true;
                m_writeThread.Priority = ThreadPriority;
                m_writeThread.Name = "WriteThread[" + Name + "]";
                m_writeThread.Start();
            }
        }

        private void SafeAbort(ref Thread thread)
        {
            if (thread == null)
                return;

            try
            {
                thread.Abort();
            }
            finally
            {
                thread = null;
            }
        }

        private void DestroyThreads()
        {
            SafeAbort(ref m_readThread);
            SafeAbort(ref m_writeThread);
        }

        private void DestroySocket()
        {
            try
            {
                if (m_socket != null)
                    m_socket.Close();
            }
            finally
            {
                m_socket = null;
                m_stream = null;
                m_sslStream = null;
            }
        }

        #endregion

        #region ReadProc

        /// <summary>
        /// Reads data from the servers and enqueues it in m_readQueue.
        /// </summary>
        protected void ReadProc()
        {
            try
            {
                Trace("Thread " + Thread.CurrentThread.Name + " started");

                try
                {
                    while (IsConnected)
                    {
                        string data = m_reader.ReadLine();

                        if (!string.IsNullOrEmpty(data))
                        {
                            //if (m_debugReadQueue)
                            //    data = m_debugReadQueueStart + DateTime.Now.Ticks.ToString() + m_debugReadQueueEnd + data;

                            lock (m_readQueue)
                            {
                                m_readQueue.Enqueue(data);
                            }
                            //Trace("Read: " + data);
                        }

                        Thread.Sleep(m_readSleep);
                    }
                }
                catch (IOException ex)
                {
                    Trace(ex);
                }
                finally
                {
                    Trace("Connection lost");
                    m_isConnectionError = true;
                }
            }
            catch (ThreadAbortException)
            {
                Trace("Thread " + Thread.CurrentThread.Name + " aborted");
            }
            finally
            {
                Trace("Thread " + Thread.CurrentThread.Name + " stopped");
            }
        }

        #endregion        

        #region WriteProc

        /// <summary>
        /// Dequeues data from m_writeQueue and sends it to the server.
        /// </summary>
        protected void WriteProc()
        {
            try
            {
                Trace("Thread " + Thread.CurrentThread.Name + " started");

                try
                {
                    int sleep = 0;
                    string data = null;

                    Stopwatch stopWatch = new Stopwatch();
                    long sleepTimeout = 0;
                    
                    while (true)
                    {
                    	lock (m_syncRoot)
                    	{
                    		if (m_status != ConnectionStatus.Connected)
                    			break;
                    	}
                    
                        lock (m_sendQueue)
                        {
                            if (m_sendQueue.Count == 0)
                            {
                                Thread.Sleep(m_writeIdleSleep);
                                continue;
                            }
                        }

                        // Very advanced flood control ;) (FIXME)
                        if (m_floodControl && sleep > 0)
                        {
                            if (stopWatch.IsRunning && sleepTimeout > stopWatch.ElapsedMilliseconds)
                                Thread.Sleep(sleep);
                            stopWatch.Stop();
                            sleep = 0;
                        }

                        lock (m_sendQueue)
                        {
                            if (m_sendQueue.Count > 0)
                                data = m_sendQueue.Dequeue();
                        }

                        if (data != null)
                        {
                            m_writer.WriteLine(data);
                            m_writer.Flush();

                            sleep = (data.Length + 2) * m_writeFloodControlMult;
                            sleepTimeout = m_writeFloodControlTimeout;
                            stopWatch.Reset();
                            stopWatch.Start();
                        }

                        Thread.Sleep(m_writeSleep);
                    }
                }
                catch (IOException ex)
                {
                    Trace(ex);
                }
                finally
                {
                    Trace("Connection lost");
                    m_isConnectionError = true;
                }
            }
            catch (ThreadAbortException)
            {
                Trace("Thread " + Thread.CurrentThread.Name + " aborted");
            }
            finally
            {
                Trace("Thread " + Thread.CurrentThread.Name + " stopped");
            }
        }

        #endregion

        #region Login

        /// <summary>
        /// Login to an IRC server.
        /// </summary>
        /// <param name="nickname">Nickname.</param>
        public void Login(string nickname)
        {
            Login(new string[] { nickname }, null, nickname, 0, null);
        }

        /// <summary>
        /// Login to an IRC server.
        /// </summary>
        /// <param name="nickname">Nickname.</param>
        /// <param name="username">Username.</param>
        /// <param name="realname">Realname.</param>
        /// <param name="usermode">Usermode.</param>
        /// <param name="password">Password.</param>
        public void Login(string nickname, string username, string realname, int usermode, string password)
        {
            Login(new string[] { nickname }, username, realname, usermode, password);
        }

        /// <summary>
        /// Login to an IRC server.
        /// </summary>
        /// <param name="nicknames">Nicknames.</param>
        /// <param name="username">Username.</param>
        /// <param name="realname">Realname.</param>
        /// <param name="usermode">Usermode.</param>
        /// <param name="password">Password.</param>
        public void Login(string[] nicknames, string username, string realname, int usermode, string password)
        {
            m_nicknameIndex = 0;

            if (nicknames.Length > 0)
                m_nicknames = (string[])nicknames.Clone();
            else
                m_nicknames = new string[] { Environment.UserName };

            if (username != null && username.Length > 0)
                m_username = username.Replace(" ", "");
            else
                m_username = Environment.UserName.Replace(" ", "");

            if (realname != null && realname.Length > 0)
                m_realname = realname;
            else
                m_realname = m_nicknames[0];

            if (password != null && password.Length > 0)
            {
                m_password = password;
                m_rfc.Pass(m_password);
            }
            else
                m_password = string.Empty;

            m_nickname = string.Empty;
            m_usermode = usermode;

            m_rfc.Nick(m_nicknames[0]);
            m_rfc.User(m_username, m_usermode, m_realname);
        }

        #endregion //Login

        #region UserLeft
        
        /// <summary>
        /// Helper function that is called when a user has left or quit a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="who">Nickname.</param>
        protected void UserLeft(string channel, string who)
        {
            IrcChannel c = GetChannel(channel);
            
            if (c != null)
            {
                if (IsMe(who))
                {
                    m_channels.Remove(channel);
                    c.Clear();
                }
                else
                {
                    c.RemoveUser(who);
                }
            }
        }

        #endregion

        #region IsChannel

        /// <summary>
        /// Check if a string is a valid channel name on the current network.
        /// </summary>
        /// <param name="target">String</param>
        /// <returns>True is target is a valid channel name</returns>
        public bool IsChannel(string target)
        {
            if (target == null || target.Length < 1)
                return false;
            foreach (char c in ChannelTypes)
            {
                if (target[0] == c)
                    return true;
            }
            return false;
        }

        #endregion

        #region CreateFakeMessage

        /// <summary>
        /// Creates a faked message.
        /// </summary>
        /// <param name="target">Target.</param>
        /// <param name="message">Message.</param>
        /// <returns>Fake message.</returns>
        protected virtual MessageData CreateFakeMessage(string target, string message)
        {
            MessageData data = new MessageData(this);
            data.Nick = Nickname;
            data.Channel = target;
            data.From = Nickname;
            data.Host = "localhost";
            data.Ident = "null";
            data.Params = new string[] { Nickname, message };
            data.RawMessage = "fake";
            return data;
        }

        #endregion CreateFakeMessage

        #region SendMessage

        /// <summary>
        /// Sends a message to an ircuser or a channel
        /// </summary>
        /// <param name="target">Nickname or channel</param>
        /// <param name="message">Message to be sent</param>
        public void SendMessage(string target, string message)
        {
            SendMessage(target, message, false);
        }

        /// <summary>
        /// Sends a message to an ircuser or a channel
        /// </summary>
        /// <param name="target">Nickname or channel</param>
        /// <param name="message">Message to be sent</param>
        /// <param name="silent">Outgoing events will not be triggered if true.</param>
        public virtual void SendMessage(string target, string message, bool silent)
        {
            m_rfc.PrivMsg(target, message);

            if (silent)
                return;

            if (IsChannel(target))
                OnOutgoingChannelMessage(new ChannelMessageEventArgs(CreateFakeMessage(target, message)));
            else
                OnOutgoingQueryMessage(new QueryMessageEventArgs(CreateFakeMessage(target, message)));
        }

        #endregion

        #region SendNotice

        /// <summary>
        /// Send a notice to an ircuser or a channel
        /// </summary>
        /// <param name="target">Mickname or channel</param>
        /// <param name="message">Message to be sent</param>
        public void SendNotice(string target, string message)
        {
            SendNotice(target, message, false);
        }

        /// <summary>
        /// Send a notice to an ircuser or a channel
        /// </summary>
        /// <param name="target">Mickname or channel</param>
        /// <param name="message">Message to be sent</param>
        /// <param name="silent">Outgoing events will not be triggered if true.</param>
        public virtual void SendNotice(string target, string message, bool silent)
        {
            m_rfc.Notice(target, message);

            if (silent)
                return;

            if (IsChannel(target))
                OnOutgoingChannelNotice(new ChannelNoticeEventArgs(CreateFakeMessage(target, message)));
            else
                OnOutgoingQueryNotice(new QueryNoticeEventArgs(CreateFakeMessage(target, message)));
        }
        #endregion

        #region SendAction

        /// <summary>
        /// Send an action to a channel or user.
        /// </summary>
        /// <param name="target">Channel or nickname.</param>
        /// <param name="action">Action text.</param>
        public void SendAction(string target, string action)
        {
            SendAction(target, action, false);
        }

        /// <summary>
        /// Send an action to a channel or user.
        /// </summary>
        /// <param name="target">Channel or nickname.</param>
        /// <param name="action">Action text.</param>
        /// <param name="silent">Don't trigger outgoing events.</param>
        public virtual void SendAction(string target, string action, bool silent)
        {
            m_rfc.PrivMsg(target, '\x1' + "ACTION " + action + '\x1');
            
            if (silent)
                return;

            MessageData data = CreateFakeMessage(target, action);

            if (IsChannel(target))
                OnOutgoingChannelAction(new ChannelActionEventArgs(data, data.Message));
            else
                OnOutgoingQueryAction(new QueryActionEventArgs(data, data.Message));
        }
        #endregion

        #region SendCtcpRequest

        /// <summary>
        /// Sends a CTCP request.
        /// </summary>
        /// <param name="target">Username or channel.</param>
        /// <param name="message">Message.</param>
        public virtual void SendCtcpRequest(string target, string message)
        {
            m_rfc.PrivMsg(target, '\x1' + message + '\x1');

            string[] tmp = message.Split(new char[] { ' ' }, 2);
            OnOutgoingCtcpRequest(new CtcpRequestEventArgs(tmp[0], tmp.Length > 1 ? tmp[1] : "", CreateFakeMessage(target, message)));
        }

        #endregion

        #region SendCtcpReply

        /// <summary>
        /// Sends a CTCP reply.
        /// </summary>
        /// <param name="target">Target.</param>
        /// <param name="message">Message.</param>
        public virtual void SendCtcpReply(string target, string message)
        {
            m_rfc.Notice(target, '\x1' + message + '\x1');

            string[] tmp = message.Split(new char[] { ' ' }, 2);
            OnOutgoingCtcpReply(new CtcpReplyEventArgs(tmp[0], tmp.Length > 1 ? tmp[1] : "", CreateFakeMessage(target, message)));
        }

        #endregion

        #region IsMe

        /// <summary>
        /// IsMe
        /// </summary>
        /// <param name="nick">Nickname</param>
        /// <returns>Returns true if nick is the current Nickname</returns>
        public bool IsMe(string nick)
        {
            return string.Compare(nick, m_nickname, true) == 0;
        }

        #endregion

        #region IsOn

        /// <summary>
        /// IsOn
        /// </summary>
        /// <param name="channel"></param>
        /// <returns>Returns true if the user is on that channel</returns>
        public bool IsOn(string channel)
        {
            return m_channels.ContainsKey(channel);
        }

        #endregion

        #region Mode

        /// <summary>
        /// This function allows you to set multiple modes at once
        /// Ex: Mode("#mychannel", '+', 'o', new string[]{ "Nick1", "Nick2" });
        /// Would set operator status for Nick1 and Nick2
        /// </summary>
        /// <param name="target">Target.</param>
        /// <param name="prefix">Prefix.</param>
        /// <param name="mode">Mode.</param>
        /// <param name="args">Arguments.</param>
        public void Mode(string target, char prefix, char mode, string[] args)
        {
            Debug.Assert(prefix == '-' || prefix == '+' || prefix == ' ');

            string a = prefix.ToString();
            string b = string.Empty;

            foreach (string arg in args)
            {
                if (a.Length > m_modes)
                {
                    Rfc.Mode(target, a + " " + b);
                    a = prefix.ToString();
                    b = string.Empty;
                }

                a += mode;
                b += arg + " ";
            }

            if (b.Length > 1)
                Rfc.Mode(target, a + " " + b);
        }

        #endregion //Mode

        #region Op

        /// <summary>
        /// Give op to a user.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nickname">Nickname.</param>
        public void Op(string channel, string nickname)
        {
            Rfc.Mode(channel, "+o " + nickname);
        }

        /// <summary>
        /// Give op to multiple users.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nicknames">Nicknames.</param>
        public void Op(string channel, string[] nicknames)
        {
            Mode(channel, '+', 'o', nicknames);
        }

        #endregion

        #region DeOp

        /// <summary>
        /// DeOp a user.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nickname">Nickname.</param>
        public void DeOp(string channel, string nickname)
        {
            Rfc.Mode(channel, "-o " + nickname);
        }

        /// <summary>
        /// DeOp multiple users.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nicknames">Nicknames.</param>
        public void DeOp(string channel, string[] nicknames)
        {
            Mode(channel, '-', 'o', nicknames);
        }

        #endregion

        #region HalfOp

        /// <summary>
        /// Give half-op to a user.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nickname">Nickname.</param>
        public void HalfOp(string channel, string nickname)
        {
            Rfc.Mode(channel, "+h " + nickname);
        }

        /// <summary>
        /// Give half-op to multiple users.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nicknames">Nicknames.</param>
        public void HalfOp(string channel, string[] nicknames)
        {
            Mode(channel, '+', 'h', nicknames);
        }

        #endregion

        #region DeHalfOp

        /// <summary>
        /// Remove half-op from a user.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nickname">Nickname.</param>
        public void DeHalfOp(string channel, string nickname)
        {
            Rfc.Mode(channel, "-h " + nickname);
        }

        /// <summary>
        /// Remove half-op from multiple users..
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nicknames">Nicknames.</param>
        public void DeHalfOp(string channel, string[] nicknames)
        {
            Mode(channel, '-', 'h', nicknames);
        }

        #endregion

        #region Voice

        /// <summary>
        /// Remove voice from a user.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nickname">Nickname.</param>
        public void Voice(string channel, string nickname)
        {
            Rfc.Mode(channel, "+v " + nickname);
        }

        /// <summary>
        /// Remove voice from multiple users.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nicknames">Nicknames.</param>
        public void Voice(string channel, string[] nicknames)
        {
            Mode(channel, '+', 'v', nicknames);
        }

        #endregion

        #region DeVoice

        /// <summary>
        /// Devoice a user.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nickname">Nickname.</param>
        public void DeVoice(string channel, string nickname)
        {
            Rfc.Mode(channel, "-v " + nickname);
        }

        /// <summary>
        /// Devoice multiple users.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="nicknames">Nicknames.</param>
        public void DeVoice(string channel, string[] nicknames)
        {
            Mode(channel, '-', 'v', nicknames);
        }

        #endregion

        #region Ban

        /// <summary>
        /// Ban a hostmask from a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="hostmask">Hostmask.</param>
        public void Ban(string channel, string hostmask)
        {
            Rfc.Mode(channel, "+b " + hostmask);
        }

        #endregion

        #region Unban

        /// <summary>
        /// Remove a ban from a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="hostmask">Hostmask.</param>
        public void UnBan(string channel, string hostmask)
        {
            Rfc.Mode(channel, "-b " + hostmask);
        }

        #endregion

        #region Kick

        /// <summary>
        /// Kick a user from a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="user">Nickname.</param>
        public virtual void Kick(string channel, string user)
        {
            Rfc.Kick(channel, user);
        }

        /// <summary>
        /// Kick a user from a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="user">Nickname.</param>
        /// <param name="comment">Kick message.</param>
        public virtual void Kick(string channel, string user, string comment)
        {
            Rfc.Kick(channel, user, comment);
        }
        
        /// <summary>
        /// Kick multiple users from multiple channels.
        /// </summary>
        /// <param name="channels">Channels.</param>
        /// <param name="users">Nicknames.</param>
        public virtual void Kick(string[] channels, string[] users)
        {
            Rfc.Kick(channels, users);
        }

        /// <summary>
        /// Kick multiple users from multiple channels.
        /// </summary>
        /// <param name="channels">Channels.</param>
        /// <param name="users">Nicknames.</param>
        /// <param name="comment">Kick message.</param>
        public virtual void Kick(string[] channels, string[] users, string comment)
        {
            Rfc.Kick(channels, users, comment);
        }

        #endregion

        #region SetTopic

        /// <summary>
        /// Sets a topic in a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        /// <param name="topic">Topic.</param>
        public virtual void SetTopic(string channel, string topic)
        {
            Rfc.Topic(channel, topic);
        }

        #endregion

        #region GetTopic

        /// <summary>
        /// Requests topic from a channel.
        /// </summary>
        /// <param name="channel">Channel.</param>
        public virtual void GetTopic(string channel)
        {
            Rfc.Topic(channel);
        }

        #endregion

        #region GetChannel

        /// <summary>
        /// Get a joined channel
        /// </summary>
        /// <param name="name">Channel name</param>
        /// <returns>null if channels is not found</returns>
        public IrcChannel GetChannel(string name)
        {
            lock (m_channels.SyncRoot)
            {
                if (name != null && m_channels.ContainsKey(name))
                    return m_channels[name];
            }
            return null;
        }

        #endregion

        #region GetChannelUser

        /// <summary>
        /// Get a ChannelUser
        /// </summary>
        /// <param name="channel">Channel name</param>
        /// <param name="nick">Nickname</param>
        /// <returns>null if user is not found</returns>
        public IrcChannelUser GetChannelUser(string channel, string nick)
        {
            if (channel != null && nick != null)
            {
                IrcChannel c = GetChannel(channel);
                if (c != null)
                    return c.GetUser(nick);
            }
            return null;
        }

        #endregion

        #region GetUser

        /// <summary>
        /// Get a known ircuser
        /// </summary>
        /// <param name="nick">Nickname</param>
        /// <returns>Returns null if user doesn't exist</returns>
        public IrcUser GetUser(string nick)
        {
            lock(SyncRoot)
            {
            	IrcUser result = null;
            	m_ircUsers.TryGetValue(nick, out result);
            	return result;
            }
        }

        #endregion

        #region ClearUsers

        private void ClearUsers()
        {
            lock (m_ircUsers.SyncRoot)
                m_ircUsers.Clear();
        }

        #endregion

        #region ClearChannels

        private void ClearChannels()
        {
            lock (m_channels.SyncRoot)
                m_channels.Clear();
        }

        #endregion

        #region ParseMessage

        /// <summary>
        /// Parses a raw IRC message.
        /// </summary>
        /// <param name="message">IRC message.</param>
        /// <returns>MessageData.</returns>
        protected virtual MessageData ParseMessage(string message)
        {
            //TODO: This method is really ugly and should be fixed.

            try
            {
                MessageData msg = new MessageData(this);
                msg.RawMessage = message;

                //Trace(message);

                string tmp = message;
                int index = tmp.IndexOf(' ');

                if (index < 1) // Invalid Message
                    return null;

                if (message.StartsWith(":"))
                {
                    msg.From = tmp.Substring(1, index - 1);

                    int identIndex = msg.From.IndexOf('!');
                    int hostIndex = msg.From.IndexOf('@');
                    if (identIndex > -1 && hostIndex > -1 && hostIndex + 1 < msg.From.Length)
                    {
                        msg.Nick = msg.From.Substring(0, identIndex);
                        msg.Ident = msg.From.Substring(identIndex + 1, hostIndex - identIndex - 1);
                        msg.Host = msg.From.Substring(hostIndex + 1);
                    }

                    tmp = tmp.Substring(index).Trim();
                }

                const string msgSeparator = " :";
                index = tmp.IndexOf(msgSeparator);
                string lastParam = null;

                if (index > 0)
                {
                    lastParam = tmp.Substring(index + msgSeparator.Length);
                    tmp = tmp.Substring(0, index).Trim();
                }

                string[] items = tmp.Split(new char[] { ' ' });

                if (lastParam != null)
                {
                    msg.Params = new string[items.Length + 1];
                    if (items.Length > 0)
                        items.CopyTo(msg.Params, 0);
                    msg.Params[items.Length] = lastParam;
                }
                else
                    msg.Params = items;

                msg.Command = items[0];

                if (msg.Nick == null && items.Length > 1)
                    msg.Nick = items[1];

                int replyCode;

                if (int.TryParse(msg.Params[0], out replyCode))
                {
                    msg.ReplyCode = (ReplyCode)replyCode;
                    msg.Type = GetMessageType(msg.ReplyCode);
                }
                else
                {
                    msg.ReplyCode = ReplyCode.Null;
                    msg.Type = GetMessageType(msg.Command);
                }

                if (msg.Params.Length > 1)
                {
                    if (IsChannel(msg.Params[1]))
                    {
                        msg.Channel = msg.Params[1];
                    }
                    else if (msg.ReplyCode != ReplyCode.Null)
                    {
                        if (msg.Params.Length > 2 && IsChannel(msg.Params[2]))
                            msg.Channel = msg.Params[2];
                        else if (msg.Params.Length > 3 && IsChannel(msg.Params[3]))
                            msg.Channel = msg.Params[3];
                    }                    
                    //else if(IsChannel(msg.Params[msg.Params.Length-1]))
                    //    msg.Channel = msg.Params[msg.Params.Length-1];
                }

                return msg;
            }
            catch (Exception ex)
            {
                Trace(ex);
                return null;
            }
        }
        #endregion

        #region GetMessageType

        private MessageType GetMessageType(string command)
        {
            switch (command.Trim().ToUpper())
            {
                case "JOIN":
                    return MessageType.Join;
                case "KICK":
                    return MessageType.Kick;
                case "MODE":
                    return MessageType.Mode;
                case "NICK":
                    return MessageType.Nick;
                case "NOTICE":
                    return MessageType.Notice;
                case "PART":
                    return MessageType.Part;
                case "PING":
                    return MessageType.Ping;
                case "PONG":
                    return MessageType.Pong;
                case "PRIVMSG":
                    return MessageType.Message;
                case "TOPIC":
                    return MessageType.Topic;
                case "QUIT":
                    return MessageType.Quit;
                case "ERROR":
                    return MessageType.Error;
            }
            return MessageType.Unknown;
        }
        #endregion

        #region GetMessageType
        private MessageType GetMessageType(ReplyCode code)
        {
            switch (code)
            {
                case ReplyCode.Welcome:
                    return MessageType.Welcome;
                case ReplyCode.Names:
                case ReplyCode.EndOfNames:
                    return MessageType.Names;
                case ReplyCode.BanList:
                case ReplyCode.EndOfBanList:
                    return MessageType.BanList;
                case ReplyCode.Who:
                case ReplyCode.EndOfWho:
                    return MessageType.Who;
                default:
                    break;
            }

            if (code >= ReplyCode.ErrorNoSuchNickname)
                return MessageType.Error;
            return MessageType.Unknown;
        }
        #endregion

        #region OnHandleMessage_Internal
        private void OnHandleMessage_Internal(MessageData data)
        {
            switch (data.Type)
            {
                case MessageType.Join:
                    OnJoin(new JoinEventArgs(data));
                    break;
                case MessageType.Kick:
                    OnKick(new KickEventArgs(data));
                    break;
                case MessageType.Message:
                    OnMessage(data);
                    break;
                case MessageType.Mode:
                    OnModeChanged(new ModeChangedEventArgs(data));
                    break;
                case MessageType.Nick:
                    OnNickChanged(new NickChangedEventArgs(data));
                    break;
                case MessageType.Notice:
                    OnNotice(data);
                    break;
                case MessageType.Part:
                    OnPart(new PartEventArgs(data));
                    break;
                case MessageType.Quit:
                    OnQuit(new QuitEventArgs(data));
                    break;
                case MessageType.Ping:
                    OnPing(new PingEventArgs(data));
                    break;
                case MessageType.Pong:
                    OnPong(new PongEventArgs(data));
                    break;
                case MessageType.Topic:
                    OnChannelTopicChanged(new ChannelTopicChangedEventArgs(data));
                    break;
                case MessageType.Error:
                    OnError(new ErrorEventArgs(data));
                    break;
                default:
                    Trace("Unimplemented MessageType: " + data.Type + " {" + data.RawMessage + "}");
                    break;
            }
        }
        #endregion

        #region OnHandleReplyCode_Internal

        private void OnHandleReplyCode_Internal(MessageData data)
        {
            OnNumericReply(new IrcEventArgs(data));

            switch (data.ReplyCode)
            {
                case ReplyCode.Support:
                    OnSupport(data);
                    break;
                case ReplyCode.Welcome:
                    OnWelcome(data);
                    break;
                case ReplyCode.Names:
                    OnNames(data);
                    break;
                case ReplyCode.EndOfNames:
                    OnEndOfNames(data);
                    break;
                case ReplyCode.BanList:
                    OnBanlist(data);
                    break;
                case ReplyCode.EndOfBanList:
                    OnEndOfBanList(data);
                    break;
                case ReplyCode.Who:
                    OnWho(data);
                    break;
                case ReplyCode.EndOfWho:
                    OnEndOfWho(data);
                    break;
                case ReplyCode.ChannelModeIs:
                    OnChannelModeIs(new ChannelModeIsEventArgs(data));
                    break;
                case ReplyCode.UserModeIs:
                    OnUserModeIs(data);
                    break;
                case ReplyCode.ErrorNicknameInUse:
                    OnNicknameInUse(data);
                    OnError(new ErrorEventArgs(data));
                    break;
                case ReplyCode.ChannelCreated:
                    OnChannelCreated(new ChannelCreatedEventArgs(data));
                    break;
                case ReplyCode.Topic:
                    OnChannelTopic(new ChannelTopicEventArgs(data));
                    break;
                case ReplyCode.NoTopic:
                    OnChannelTopicNotSet(new IrcEventArgs(data));
                    break;
                case ReplyCode.TopicSetBy:
                    OnChannelTopicSetBy(new ChannelTopicSetByEventArgs(data));
                    break;
                case ReplyCode.MotdStart:
                case ReplyCode.Motd:
                    OnMotd(data);
                    break;
                case ReplyCode.EndOfMotd:
                    OnEndOfMotd(data);
                    break;
                case ReplyCode.WhoIsChannels:
                    OnWhoIsChannels(data);
                    break;
                case ReplyCode.WhoIsIdle:
                    OnWhoIsIdle(data);
                    break;
                case ReplyCode.WhoIsOperator:
                    OnWhoIsOperator(data);
                    break;
                case ReplyCode.WhoIsServer:
                    OnWhoIsServer(data);
                    break;
                case ReplyCode.WhoIsUser:
                    OnWhoIsUser(data);
                    break;
                case ReplyCode.EndOfWhoIs:
                    OnEndOfWhoIs(data);
                    break;
                case ReplyCode.Away:
                    OnAway(new AwayEventArgs(data));
                    break;
                case ReplyCode.UnAway:
                    OnUnAway(new IrcEventArgs(data));
                    break;
                case ReplyCode.NowAway:
                    OnNowAway(new IrcEventArgs(data));
                    break;
               // case ReplyCode.ListStart:
                case ReplyCode.List:
                    OnChannelList(new ChannelListEventArgs(data));
                    break;
                case ReplyCode.ListEnd:
                    OnChannelListEnd(new IrcEventArgs(data));
                    break;
                default:
                    if(data.ReplyCode <= ReplyCode.MyInfo)
                    {
                        OnServerMessage(new IrcEventArgs(data));
                    }
                    else if(data.ReplyCode >= ReplyCode.LuserClient && data.ReplyCode <= ReplyCode.LuserMe)
                    {
                        OnServerMessage(new IrcEventArgs(data));
                    }
                    else if (data.ReplyCode >= ReplyCode.ErrorNoSuchNickname)
                    {
                        Trace("ErrorReply: " + data.Message);
                        OnError(new ErrorEventArgs(data, data.Message));
                    }
                    else
                        Trace("Unimplemented ReplyCode: " + (int)data.ReplyCode + " {" + data.RawMessage + "}");
                    break;
            }
        }

        #endregion

        #region OnMessage

        private void OnMessage(MessageData data)
        {
            try
            {
                //if (data.Message == null)
                //    throw new Exception("data.Message is null");
                //if (data.Message.Length < 0)
                //    throw new Exception("data.Message is empty");

                if (data.Message.Length > 0 && data.Message[0] == '\x1')
                {
                    string tmp = data.Message.Trim(new char[] { '\x1' });
                    string command = string.Empty;
                    string message = string.Empty;

                    int index = tmp.IndexOf(' ');
                    if (index > -1)
                    {
                        command = tmp.Substring(0, index);
                        message = tmp.Substring(index + 1);
                    }
                    else
                        command = tmp;

                    if (command.Length > 0)
                    {
                    	CtcpRequestEventArgs e = new CtcpRequestEventArgs(command, message, data);
                        OnIncomingCtcpRequest(e);
                        OnSpecificCtcpRequest(command, e);
                    }
                }
                else
                {
                    if (data.Channel == null)
                    {
                        OnQueryMessage(new QueryMessageEventArgs(data));
                    }
                    else
                    {
                        OnChannelMessage(new ChannelMessageEventArgs(data));
                    }
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        #endregion

        #region OnNotice

        private void OnNotice(MessageData data)
        {
            try
            {
                if (data.Message.Length > 0 && data.Message[0] == '\x1')
                {
                    string tmp = data.Message.Trim(new char[] { '\x1' });
                    string command = string.Empty;
                    string message = string.Empty;

                    int index = tmp.IndexOf(' ');
                    if (index > -1)
                    {
                        command = tmp.Substring(0, index);
                        message = tmp.Substring(index + 1);
                    }
                    else
                        command = tmp;

                    if (command.Length > 0)
                    {
                    	CtcpReplyEventArgs e = new CtcpReplyEventArgs(command, message, data);
                        OnIncomingCtcpReply(e);
                        OnSpecificCtcpReply(command, e);
                    }
                }
                else
                {
                    if (data.Channel == null)
                    {
                        OnQueryNotice(new QueryNoticeEventArgs(data));
                    }
                    else
                    {
                        OnChannelNotice(new ChannelNoticeEventArgs(data));
                    }
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        #endregion

        #region OnWelcome

        private void OnWelcome(MessageData data)
        {
            m_isRegistered = true;
            m_nickname = data.Params[1];
            OnRegistered(new RegisteredEventArgs(data));
        }

        #endregion

        #region OnNames

        void OnNames(MessageData data)
        {
            try
            {
                if(data.Channel != null)
                {
                    IrcChannel channel = GetChannel(data.Channel);

                    if(channel != null && !channel.IsSynced)
                    {
                        string userModes = string.Empty;

                        foreach(KeyValuePair<char, char> kv in m_channelUserPrefixes)
                            userModes += kv.Value;

                        foreach(string str in data.Message.Split(' '))
                        {
                            string name = str.Trim();
                            if (name.Length < 1)
                                continue;

                            string mode = string.Empty;
                            string nick = string.Empty;

                            int i;
                            for (i = 0; i < name.Length && userModes.IndexOf(name[i]) > -1; ++i)
                                mode += name[i];

                            nick = name.Substring(i);

                            IrcUser ircUser = GetUser(nick);
                            if (ircUser == null)
                                ircUser = new IrcUser(this, nick);

                            IrcChannelUser user = channel.GetUser(ircUser.Nick);
                            if (user == null)
                                channel.AddUser(ircUser, mode);
                            else
                                user.SetModeEnabled(mode, true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
            finally
            {
                OnNames(new NamesEventArgs(data));
            }
        }

        #endregion

        #region OnEndOfNames

        private void OnEndOfNames(MessageData data)
        {
            try
            {
                IrcChannel channel = GetChannel(data.Channel);

                if (channel != null && !channel.IsSynced)
                {
                    m_rfc.Who(data.Channel);	// request who data (for hostmask and stuff)
                    channel.IsSynced = true;
                    OnChannelSynced(new IrcEventArgs(data));
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
            finally
            {
                OnNamesEnd(new NamesEndEventArgs(data));
            }
        }

        #endregion

        #region OnBanList

        private void OnBanlist(MessageData data)
        {
            try
            {
                IrcChannel channel = GetChannel(data.Channel);

                BanlistEventArgs e = new BanlistEventArgs(data);

                if (channel != null)
                {
                    if (!channel.IsUpdatingBanList)
                    {
                        channel.ClearBans();
                        channel.IsUpdatingBanList = true;
                    }

                    channel.AddBan(e.BanInfo);
                }

                OnBanlist(e);
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        #endregion

        #region OnEndOfBanList

        private void OnEndOfBanList(MessageData data)
        {
            try
            {
                IrcChannel channel = GetChannel(data.Channel);

                if (channel != null)
                {
                    channel.IsUpdatingBanList = false;
                }

                OnBanlistEnd(new BanlistEndEventArgs(data));
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        #endregion

        #region OnWho

        private void OnWho(MessageData data)
        {
            try
            {
                WhoEventArgs e = new WhoEventArgs(data);

                IrcUser ircUser = GetUser(e.Data.Nick);
                
                GetUser(e.Data.Nick, e.Data);

                OnWho(e);
            }
            catch (Exception ex)
            {
                Trace("Who failed: " + data.RawMessage);
                Trace(ex);
            }
        }

        #endregion

        #region OnEndOfWho
        
        private void OnEndOfWho(MessageData data)
        {
        }
        
        #endregion

        #region OnUserModeIs
        
        private void OnUserModeIs(MessageData data)
        {
            IrcUser user = GetUser(data.Nick);
            if (user != null)
                user.Mode = data.Message;
            OnUserMode(new UserModeEventArgs(data));
        }
        
        #endregion

        #region OnNicknameInUse

        private void OnNicknameInUse(MessageData data)
        {
            try
            {
                if (string.IsNullOrEmpty(m_nickname))
                {
                    string nickname = null;

                    if (m_nicknameIndex < m_nicknames.Length && m_nicknames != null && m_nicknames.Length > 0)
                    {
                        nickname = m_nicknames[m_nicknameIndex];
                        m_nicknameIndex++;
                    }

                    if (nickname == null)
                    {
                        Random random = new Random();
                        string rand = random.Next(1000).ToString();

                        if (m_nicknames != null && m_nicknames.Length > 0)
                        {
                            string nick = m_nicknames[random.Next(m_nicknames.Length - 1)];
                            int maxLength = MaxNickLength > 0 ? MaxNickLength : 8;

                            if (nick.Length + rand.Length > maxLength)
                            {
                                if (maxLength - rand.Length > 1)
                                    nick = nick.Substring(0, maxLength - rand.Length);
                            }

                            nickname = nick + rand;

                            m_nicknameIndex = 0;
                        }
                        else
                            nickname = "NoName_" + rand;
                    }

                    Rfc.Nick(nickname);
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }
        
        #endregion

        #region OnMotd
        
        private List<string> m_motdTemp = new List<string>();

        private void OnMotd(MessageData data)
        {
            m_motdTemp.Add(data.Message);
        }

        private void OnEndOfMotd(MessageData data)
        {
            OnMotd(new MotdEventArgs(m_motdTemp, data));
            m_motdTemp.Clear();
        }
        
        #endregion

        #region OnWhoIs*

        IDictionary<string, WhoIsInfo> m_whoIsTemp = new Dictionary<string, WhoIsInfo>(StringComparer.InvariantCultureIgnoreCase);

        private WhoIsInfo _GetWhoIs(MessageData data)
        {
            if (!m_whoIsTemp.ContainsKey(data.Params[2]))
                m_whoIsTemp[data.Params[2]] = new WhoIsInfo(data.Params[2]);
            return m_whoIsTemp[data.Params[2]];
        }

        private void OnWhoIsChannels(MessageData data)
        {
            try
            {
                WhoIsInfo info = _GetWhoIs(data);
                info.Channels += data.Message;
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        private void OnWhoIsIdle(MessageData data)
        {
            try
            {
                WhoIsInfo info = _GetWhoIs(data);

                int index = 3;
                string[] temp = data.Message.Split(',');
                foreach (string s in temp)
                {
                    if (index >= data.Params.Length)
                        return;

                    string str = s.Trim();
                    if (string.Compare(str, "seconds idle", true) == 0)
                    {
                        int idle;
                        if (int.TryParse(data.Params[index], out idle))
                            info.Idle = idle;
                    }
                    else if (string.Compare(str, "signon time", true) == 0)
                    {
                        long signOn;
                        if (!long.TryParse(data.Params[index], out signOn))
                            info.SignOn = signOn;
                    }
                    index++;
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        private void OnWhoIsOperator(MessageData data)
        {
            try
            {
                WhoIsInfo info = _GetWhoIs(data);
                info.IsOperator = true;
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        private void OnWhoIsServer(MessageData data)
        {
            try
            {
                WhoIsInfo info = _GetWhoIs(data);
                info.Server = data.Params[3] + " (" + data.Message + ")";
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        private void OnWhoIsUser(MessageData data)
        {
            try
            {
                WhoIsInfo info = _GetWhoIs(data);
                string user = data.Params[3];
                string host = data.Params[4];
                info.User = user;
                info.Hostname = host;
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        private void OnEndOfWhoIs(MessageData data)
        {
            try
            {
                if (m_whoIsTemp.ContainsKey(data.Params[2]))
                {
                    WhoIsInfo info = m_whoIsTemp[data.Params[2]];
                    m_whoIsTemp.Remove(data.Params[2]);

                    OnWhoIs(new WhoIsEventArgs(info, data));
                }
            }
            catch (Exception ex)
            {
                Trace(ex);
            }
        }

        #endregion

        #region OnSupport

        private void OnSupport(MessageData data)
        {
        	try
        	{
	            //FIXME: Don't think this split is needed anymore, data.Params should be safe now.
	            string[] p = data.RawMessage.Split(' ');
	
	            for (int i = 4; i < p.Length; ++i)
	            {
	                string str = p[i].Trim();
	                if (str.StartsWith(":"))
	                    break;
	
	                string[] tmp = str.Split(new char[] { '=' }, 2);
	
	                if (tmp.Length == 2)
	                {
	                    string key = tmp[0].ToUpper();
	                    string value = tmp[1];
	
	                    switch (key)
	                    {
	                        case "PREFIX":
	                            m_channelUserPrefixes.Clear();
	
	                            if (value.Length > 0)
	                            {
	                                string[] tmp2 = value.Substring(1).Split(new char[] { ')' }, 2);
	
	                                if (tmp2.Length == 2)
	                                {
	                                    string keys = tmp2[0];
	                                    string values = tmp2[1];
	
	                                    if (keys.Length == values.Length)
	                                    {
	                                        for (int j = 0; j < keys.Length; ++j)
	                                            m_channelUserPrefixes[keys[j]] = values[j];
	                                    }
	                                    else
	                                        OnConnectionError("Invalid PREFIX reply, arguments length does not match");
	                                }
	                                else
	                                    Trace("Invalid PREFIX reply");
	                            }
	                            break;
	                        case "CHANTYPES":
	                            m_channelTypes = value.ToCharArray();
	                            break;
	                        case "CHANMODES":
	                            m_channelModesA.Clear();
	                            m_channelModesB.Clear();
	                            m_channelModesC.Clear();
	                            m_channelModesD.Clear();
	
	                            string[] types = value.Split(',');
	
	                            for (int j = 0; j < types.Length; ++j)
	                            {
	                                SyncList<char> list = null;
	
	                                if (j == 0)
	                                    list = m_channelModesA;
	                                else if (j == 1)
	                                    list = m_channelModesB;
	                                else if (j == 2)
	                                    list = m_channelModesC;
	                                else if (j == 3)
	                                    list = m_channelModesD;
	                                else
	                                    Trace("CHAMODES out of range");
	
	                                if (list != null)
	                                {
	                                    foreach (char mode in types[j])
	                                        list.Add(mode);
	                                }
	                            }
	                            break;
	                        case "MODES":
	                            if (!int.TryParse(value, out m_modes) || m_modes < 1)
	                                m_modes = 3; // assume 3, // FIXME?
	                            break;
	                        case "MAXCHANNELS":
	                            if (!int.TryParse(value, out m_maxChannels))
	                                Trace("MAXCHANNELS is not an integer"); 
	                            break;
	                        //case "CHANLIMIT":
	                        //  break;
	                        case "NICKLEN":
	                        case "MAXNICKLEN":
	                            if (!int.TryParse(value, out m_maxNickLength))
	                                Trace("NICKLEN/MAXNICKLEN is not an integer"); 
	                            break;
	                        case "MAXBANS":
	                            if (!int.TryParse(value, out m_maxBans))
	                                Trace("MAXBANS is not an integer"); 
	                            break;
	                        case "MAXLIST":
	                            break;
	                        case "NETWORK":
	                            m_network = value;
	                            OnNetworkSet(null);
	                            break;
	                        //case "EXCEPTS":
	                        //    break;
	                        //case "INVEX":
	                        //break;
	                        //case "WALLCHOPS":
	                        //      break;
	                        //case "WALLVOICES":
	                        //break;
	                        //case "STATUSMSG":
	                        //break;
	                        case "CASEMAPPING":
	                            m_caseMapping = value;
	                            break;
	                        //case "ELIST":
	                        //break;
	                        case "TOPICLEN":
	                            if (!int.TryParse(value, out m_maxTopicLength))
	                                Trace("TOPICLEN is not an integer");
	                            break;
	                        case "KICKLEN":
	                            if(!int.TryParse(value, out m_maxKickLength))
	                                Trace("KICKLEN is not an integer");
	                            break;
	                        //case "CHANNELLEN":
	                        //break;
	                        //case "CHIDLEN":
	                        //break;
	                        //case "IDCHAN":
	                        //break;
	                        //case "STD":
	                        //break;
	                        case "SILENCE":
	                            if (!int.TryParse(value, out m_silence))
	                                Trace("SILENCE is not an integer");
	                            break;
	                        //case "RFC2812":
	                        //break;
	                        //case "PENALTY":
	                        //break;
	                        //case "FNC":
	                        //break;
	                        //case "SAFELIST":
	                        //break;
	                        case "AWAYLEN":
	                            if (!int.TryParse(value, out m_maxAwayLength))
	                                Trace("AWAYLEN is not an integer");
	                            break;
	                        //case "NOQUIT":
	                        //break;
	                        //case "USERIP":
	                        //break;
	                        //case "CPRIVMSG":
	                        //break;
	                        //case "CNOTICE":
	                        //break;
	                        //case "MAXTARGETS":
	                        //break;
	                        //case "KNOCK":
	                        //break;
	                        //case "VCHANS":
	                        //break;
	                        //case "WATCH":
	                        //break;
	                        //case "WHOX":
	                        //break;
	                        //case "CALLERID":
	                        //break;
	                        //case "ACCEPT":
	                        //break;
	                        //case "LANGUAGE":
	                        //break;
	                        default:
	                            Trace("Unimplemented ISUPPORT key " + key);
	                            break;
	                    }
	                }
	            }
	        }
	        catch(Exception ex)
	        {
	        	Trace(ex);
	        }

            OnSupport(new SupportEventArgs(data));
        }
        #endregion //OnSupport
        
        #region IDisposable Members

        /// <summary>
        /// Disposes this IrcClient.
        /// </summary>
        public virtual void Dispose()
        {
            if (m_isDisposed)
                return;

            SafeAbort(ref m_connectionKeeper);

            DestroyThreads();
            DestroySocket();

            OnDisposed(null);

            m_isDisposed = true;
        }

        bool m_isDisposed = false;

        public bool IsDisposed
        {
            get { return m_isDisposed; }
        }

        #endregion

        #region IComponent Members

        public event EventHandler Disposed;

        protected void OnDisposed(EventArgs e)
        {
            if (Disposed != null)
                Disposed(this, e);
        }

        ISite m_site = null;

        public ISite Site
        {
            get { return m_site; }
            set { m_site = value; }
        }

        #endregion
    }
}
