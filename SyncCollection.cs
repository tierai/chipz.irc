using System;
using System.Collections;
using System.Collections.Generic;

namespace Chipz.IRC.Collections
{
    /// <summary>
    /// Synchronized Collection.
    /// You should lock the SyncRoot when enumerating the collection.
    /// </summary>
    /// <typeparam name="T">Collection type.</typeparam>
    public class SyncCollection<T> : ICollection<T>, ISyncEnumerable<T>
    {
        /// <summary>
        /// Collection
        /// </summary>
        protected ICollection<T> collection;

        /// <summary>
        /// Sync object.
        /// </summary>
        public object SyncRoot
        {
            get { return collection; }
        }

        /// <summary>
        /// Creates a new instance of the SyncCollection class.
        /// </summary>
        /// <param name="collection">ICollection.</param>
        public SyncCollection(ICollection<T> collection)
        {
            this.collection = collection;
        }

        #region ICollection<T> Members

        /// <summary>
        /// Adds an item to this collection.
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            lock (SyncRoot)
                collection.Add(item);
        }

        /// <summary>
        /// Clears this collection.
        /// </summary>
        public void Clear()
        {
            lock (SyncRoot)
                collection.Clear();
        }

        /// <summary>
        ///  Determines whether the collection contains a specific value.
        /// </summary>
        /// <param name="item"></param>
        /// <returns>True if item is found in the collection; otherwise, false.</returns>
        public bool Contains(T item)
        {
            lock (SyncRoot)
                return collection.Contains(item);
        }

        /// <summary>
        /// Copies the elements of the collection to an System.Array, starting at a particular System.Array index.
        /// </summary>
        /// <param name="array">The one-dimensional System.Array that is the destination of the elements.</param>
        /// <param name="arrayIndex">The zero-based index in array at which copying begins.</param>
        public void CopyTo(T[] array, int arrayIndex)
        {
            lock (SyncRoot)
                collection.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of elements contained in the collection.
        /// </summary>
        public int Count
        {
            get
            {
                lock (SyncRoot)
                    return collection.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether the collection is readonly.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                lock (SyncRoot)
                    return collection.IsReadOnly;
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the collection.
        /// </summary>
        /// <param name="item">The object to remove from the collection.</param>
        /// <returns>True if item was successfully removed from the collection.</returns>
        public bool Remove(T item)
        {
            lock (SyncRoot)
                return collection.Remove(item);
        }

        #endregion

        #region IEnumerable<T> Members

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            lock (SyncRoot)
                return new SyncEnumerator<T>(this, collection.GetEnumerator());
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An System.Collections.IEnumerator object that can be used to iterate through the collection.</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (SyncRoot)
                return (IEnumerator)new SyncEnumerator<T>(this, collection.GetEnumerator());
        }

        #endregion
    }
}
