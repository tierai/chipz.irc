﻿using System;
using System.Text;
using System.Collections.Generic;

// FIXME!
namespace Chipz.IRC
{
	/// <summary>
	/// Irc message data.
	/// </summary>
	public class MessageData
	{
		/// <summary>
		/// From.
		/// </summary>
		public string From;
		
		/// <summary>
		/// Host.
		/// </summary>
		public string Host;
		
		/// <summary>
		/// Identity.
		/// </summary>
		public string Ident;
		
		/// <summary>
		/// Nickname.
		/// </summary>
		public string Nick;
		
		/// <summary>
		/// Command.
		/// </summary>
		public string Command;
		
		/// <summary>
		/// Channel.
		/// </summary>
		public string Channel;
		
		/// <summary>
		/// Raw message.
		/// </summary>
		public string RawMessage;
		
		/// <summary>
		/// Params.
		/// </summary>
		public string[] Params;
		
		/// <summary>
		/// MessageType.
		/// </summary>
		public MessageType Type;
		
		/// <summary>
		/// ReplyCode.
		/// </summary>
		public ReplyCode ReplyCode;
		
		/// <summary>
		/// Hostmask.
		/// </summary>
		public string Hostmask
		{
			get
			{
			    try
			    {
			        return Nick + "!" + Ident + "@" + Host;
			    }
			    catch
			    {
			        return "?!?@?";
			    }
			}
		}
		
		/// <summary>
		/// Message.
		/// </summary>
		public string Message
		{
			get
			{
				if(Params != null && Params.Length > 0)
					return Params[Params.Length-1];
				return null;
			}
		}
	
	    /// <summary>
	    /// Get The IrcClient that created this message.
	    /// </summary>
	    public IrcClient IrcClient { get { return m_ircClient; } }
	    IrcClient m_ircClient;
	
	    /// <summary>
	    /// MessageData constructor.
	    /// </summary>
	    /// <param name="ircClient">IrcClient</param>
	    public MessageData(IrcClient ircClient)
	    {
	        m_ircClient = ircClient;
	    }
	}
}
