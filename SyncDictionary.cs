using System;
using System.Collections;
using System.Collections.Generic;

namespace Chipz.IRC.Collections
{
    /// <summary>
    /// Synchronized Dictionary.
    /// You should lock the SyncRoot when enumerating the collection.
    /// </summary>
    /// <typeparam name="KeyT">Key type.</typeparam>
    /// <typeparam name="ValueT">Value type.</typeparam>
    public class SyncDictionary<KeyT, ValueT> : IDictionary<KeyT, ValueT>, ISyncEnumerable<KeyValuePair<KeyT, ValueT>>
    {
        IDictionary<KeyT, ValueT> dictionary;
        
        /// <summary>
        /// Sync object. Should be locked when enumerating this dictionary.
        /// </summary>
        public object SyncRoot
        {
            get { return dictionary; }
        }

        /// <summary>
        /// Creates a new object of the SyncDictionary class.
        /// </summary>
        /// <param name="dictionary">Dictionary.</param>
        public SyncDictionary(IDictionary<KeyT, ValueT> dictionary)
        {
            this.dictionary = dictionary;
        }

        #region IDictionary<KeyT,ValueT> Members

        /// <summary>
        /// Adds an element with the provided key and value to the dictionary.
        /// </summary>
        /// <param name="key">The object to use as the key of the element to add.</param>
        /// <param name="value">The object to use as the value of the element to add.</param>
        public void Add(KeyT key, ValueT value)
        {
            lock (SyncRoot)
                dictionary.Add(key, value);
        }
        
        /// <summary>
        /// Determines whether the dictionary contains an element with the specified key.
        /// </summary>
        /// <param name="key">The key to locate in the dictionary.</param>
        /// <returns>True if the dictionary contains an element with the key; otherwise, false.</returns>
        public bool ContainsKey(KeyT key)
        {
            lock (SyncRoot)
                return dictionary.ContainsKey(key);
        }

        /// <summary>
        /// 
        /// </summary>
        public ICollection<KeyT> Keys
        {
            get
            {
                lock (SyncRoot)
                    return new SyncCollection<KeyT>(dictionary.Keys);
            }
        }

        /// <summary>
        /// Removes the element with the specified key from the dictionary.
        /// </summary>
        /// <param name="key">The key of the element to remove.</param>
        /// <returns>True if the element is successfully removed; otherwise, false.</returns>
        public bool Remove(KeyT key)
        {
            lock (SyncRoot)
                return dictionary.Remove(key);
        }

        /// <summary>
        /// TryGetValue.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        /// <returns>True if the key exists.</returns>
        public bool TryGetValue(KeyT key, out ValueT value)
        {
            lock (SyncRoot)
                return dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// Values.
        /// </summary>
        public ICollection<ValueT> Values
        {
            get
            {
                lock (SyncRoot)
                    return new SyncCollection<ValueT>(dictionary.Values);
            }
        }

        /// <summary>
        /// Gets or sets the element with the specified key.
        /// </summary>
        /// <param name="key">The key of the element to get or set.</param>
        /// <returns>The element with the specified key.</returns>
        public ValueT this[KeyT key]
        {
            get
            {
                lock (SyncRoot)
                    return dictionary[key];
            }
            set
            {
                lock (SyncRoot)
                    dictionary[key] = value;
            }
        }

        #endregion

        #region ICollection<KeyValuePair<KeyT,ValueT>> Members

        /// <summary>
        /// Adds an item to this dictionary.
        /// </summary>
        /// <param name="item">Item to add.</param>
        public void Add(KeyValuePair<KeyT, ValueT> item)
        {
            lock (SyncRoot)
                dictionary.Add(item);
        }

        /// <summary>
        /// Clears this dictionary.
        /// </summary>
        public void Clear()
        {
            lock (SyncRoot)
                dictionary.Clear();
        }

        /// <summary>
        /// Returns true if this dictionary contains the item.
        /// </summary>
        /// <param name="item">Item to search for.</param>
        /// <returns>True if found.</returns>
        public bool Contains(KeyValuePair<KeyT, ValueT> item)
        {
            lock (SyncRoot)
                return dictionary.Contains(item);
        }

        /// <summary>
        /// Copies the KeyValuePairs in this dictionary to an array.
        /// </summary>
        /// <param name="array">Array to copy to.</param>
        /// <param name="arrayIndex">Start index.</param>
        public void CopyTo(KeyValuePair<KeyT, ValueT>[] array, int arrayIndex)
        {
            lock (SyncRoot)
                dictionary.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// Gets the number of items in this dictionary.
        /// </summary>
        public int Count
        {
            get
            {
                lock (SyncRoot)
                    return dictionary.Count;
            }
        }

        /// <summary>
        /// Gets wether this dictionary is readonly.
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                lock (SyncRoot)
                    return dictionary.IsReadOnly;
            }
        }

        /// <summary>
        /// Removes an item from the dictionary.
        /// </summary>
        /// <param name="item">Item to remove.</param>
        /// <returns>True if the item was removed.</returns>
        public bool Remove(KeyValuePair<KeyT, ValueT> item)
        {
            lock (SyncRoot)
                return dictionary.Remove(item);
        }

        #endregion

        #region IEnumerable<KeyValuePair<KeyT,ValueT>> Members

        IEnumerator<KeyValuePair<KeyT, ValueT>> IEnumerable<KeyValuePair<KeyT, ValueT>>.GetEnumerator()
        {
            lock(SyncRoot)
                return new SyncEnumerator<KeyValuePair<KeyT, ValueT>>(this, dictionary.GetEnumerator());
        }

        #endregion

        #region IEnumerable Members

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns>An System.Collections.IEnumerator object that can be used to iterate through the collection.</returns>
        public IEnumerator GetEnumerator()
        {
            lock (SyncRoot)
                return (IEnumerator)new SyncEnumerator<KeyValuePair<KeyT, ValueT>>(this, dictionary.GetEnumerator());
        }

        #endregion
    }
}
