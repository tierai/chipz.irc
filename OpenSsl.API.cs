﻿using System;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Chipz.OpenSsl
{
	/// <summary>
	/// OpenSSL methods.
	/// </summary>
	static class API
	{
	    const string libssl = "ssleay32";
	    const string libeay = "libeay32";
	    static bool s_initialized = false;
	
	    /// <summary>
	    /// Initializes the OpenSSL library.
	    /// </summary>
	    public static void Initialize()
	    {
	        if (!s_initialized)
	        {
	            SSL_load_error_strings();
	            SSL_library_init();
	            s_initialized = true;
	        }
	    }
	
	    /// <summary>
	    /// Converts an OpenSSL error code to a string.
	    /// </summary>
	    /// <param name="error">Error code.</param>
	    /// <returns>Error string.</returns>
	    public static string GetErrorString(ulong error)
	    {
	        StringBuilder sb = new StringBuilder();
	        ERR_error_string(error, sb);
	        return sb.ToString();
	    }
	    
	    [DllImport(libeay, CharSet=CharSet.Ansi)]
	    public extern static void ERR_error_string(ulong e, StringBuilder buffer);
	
	    [DllImport(libeay)]
	    public extern static ulong ERR_get_error();
	
	    [DllImport(libssl)]
	    public extern static int SSL_library_init();
	
	    [DllImport(libssl)]
	    public extern static void SSL_load_error_strings();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSLv2_client_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSLv2_server_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSLv3_client_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSLv3_server_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr TLSv1_client_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr TLSv1_server_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSLv23_client_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSLv23_server_method();
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSL_CTX_new(IntPtr method);
	
	    [DllImport(libssl)]
	    public extern static void SSL_CTX_free(IntPtr ctx);
	
	    [DllImport(libssl)]
	    public extern static int SSL_CTX_use_certificate_ASN1(IntPtr ctx, byte[] d, int len);
	
	    [DllImport(libssl)]
	    public extern static int SSL_CTX_use_RSAPrivateKey_ASN1(IntPtr ctx, byte[] d, int len);
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSL_new(IntPtr ctx);
	
	    [DllImport(libssl)]
	    public extern static void SSL_free(IntPtr ssl);
	
	    [DllImport(libssl)]
	    public extern static void SSL_set_bio(IntPtr ssl, IntPtr rbio, IntPtr wbio);
	
	    [DllImport(libssl)]
	    public extern static int SSL_connect(IntPtr ssl);
	
	    [DllImport(libssl)]
	    public extern static int SSL_accept(IntPtr ssl);
	
	    [DllImport(libssl)]
	    public extern static int SSL_shutdown(IntPtr ssl);
	
	    [DllImport(libssl)]
	    public extern static int SSL_read(IntPtr ssl, byte[] buf, int num);
	
	    [DllImport(libssl)]
	    public extern static int SSL_write(IntPtr ssl, byte[] buf, int num);
	
	    [DllImport(libssl)]
	    public extern static IntPtr SSL_get_peer_certificate(IntPtr ssl);
	
	    [DllImport(libeay)]
	    public extern static IntPtr BIO_new_socket(int sock, int close_flag);
	
	    [DllImport(libeay)]
	    public extern static int i2d_X509(IntPtr x509, IntPtr buf);
	
	    [DllImport(libeay)]
	    public extern static int i2d_X509(IntPtr x509, ref IntPtr buf);
	
	    [DllImport(libeay)]
	    public extern static void X509_free(IntPtr x509);
	}
}
