using System;
using System.Net.Sockets;

namespace Chipz.IRC
{
	/// <summary>
	/// Summary description for IrcSocket.
	/// </summary>
    public sealed class IrcSocket : TcpClient
	{
        /// <summary>
        /// Creates a new instance of the IrcSocket class.
        /// </summary>
		public IrcSocket(AddressFamily addressFamily) : base(addressFamily)
		{
			NoDelay = true;
            Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, 1);
		}
	}
}
