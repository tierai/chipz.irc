using System;

namespace Chipz.IRC
{
    /// <summary>
    /// Summary description for Rfc2812.
    /// </summary>
    public class Rfc2812
    {
    	#region Public constants
    	
    	public const int DefaultPort = 6667;
    	public const int DefaultSSLPort = 6697;
    	
    	#endregion
    	
        #region Variables

        private IrcClient m_client;

        #endregion //Variables

        #region Rfc2812

        public Rfc2812(IrcClient client)
        {
            m_client = client;
        }

        #endregion

        #region 3.1 Connection Registration

        public void Pass(string password)
        {
            m_client.WriteLine("PASS " + password);
        }

        public void Nick(string nickname)
        {
            m_client.WriteLine("NICK " + nickname);
        }

        public void User(string username, int usermode, string realname)
        {
            m_client.WriteLine("USER " + username + " " + usermode + " * :" + realname);
        }

        public void Oper(string name, string password)
        {
            m_client.WriteLine("OPER " + name + " " + password);
        }

        public void Mode(string target)
        {
            m_client.WriteLine("MODE " + target);
        }

        public void Mode(string target, string mode)
        {
            m_client.WriteLine("MODE " + target + " " + mode);
        }

        //public void Service()

        public void Quit()
        {
            m_client.WriteLine("QUIT");
        }

        public void Quit(string message)
        {
            m_client.WriteLine("QUIT :" + message);
        }

        public void Squit(string server, string comment)
        {
            m_client.WriteLine("SQUIT " + server + " :" + comment);
        }

        #endregion //3.2 Channel operations

        #region 3.2 Channel operations

        #region 3.2.1 Join message
        public void Join(string channel)
        {
            m_client.WriteLine("JOIN " + channel);
        }

        public void Join(string channel, string password)
        {
            m_client.WriteLine("JOIN " + channel + " " + password);
        }

        public void Join(string[] channels)
        {
            Join(string.Join(",", channels));
        }

        public void Join(string[] channels, string[] passwords)
        {
            Join(string.Join(",", channels), string.Join(",", passwords));
        }
        #endregion // 3.2.1 Join message

        #region 3.2.2 Part message

        public void Part(string channel)
        {
            m_client.WriteLine("PART " + channel);
        }

        public void Part(string channel, string message)
        {
            m_client.WriteLine("PART " + channel + " :" + message);
        }

        public void Part(string[] channels)
        {
            m_client.WriteLine("PART " + string.Join(",", channels));
        }

        public void Part(string[] channels, string message)
        {
            m_client.WriteLine("PART " + string.Join(",", channels) + " :" + message);
        }

        #endregion // 3.2.2 Part message

        // 3.2.3 Channel mode message - same as: 3.1.5 User mode message

        #region 3.2.4 Topic message

        public void Topic(string channel)
        {
            m_client.WriteLine("TOPIC " + channel);
        }

        public void Topic(string channel, string topic)
        {
            m_client.WriteLine("TOPIC " + channel + " :" + topic);
        }

        #endregion // 3.2.4 Topic message

        #region 3.2.5 Names message

        public void Names()
        {
            m_client.WriteLine("NAMES");
        }

        public void Names(string channel)
        {
            m_client.WriteLine("NAMES " + channel);
        }

        public void Names(string channel, string target)
        {
            m_client.WriteLine("NAMES " + channel + " " + target);
        }

        public void Names(string[] channels)
        {
            m_client.WriteLine("NAMES " + string.Join(",", channels));
        }

        public void Names(string[] channels, string target)
        {
            m_client.WriteLine("NAMES " + string.Join(",", channels) + " " + target);
        }

        #endregion // 3.2.5 Names message

        #region 3.2.6 List message

        public void List(string channel)
        {
            m_client.WriteLine("LIST " + channel);
        }

        public void List(string channel, string target)
        {
            m_client.WriteLine("LIST " + channel + " " + target);
        }

        public void List(string[] channels)
        {
            List(string.Join(",", channels));
        }

        public void List(string[] channels, string target)
        {
            List(string.Join(",", channels), target);
        }

        #endregion //

        #region 3.2.7 Invite message

        public void Invite(string nickname, string channel)
        {
            m_client.WriteLine("INVITE " + nickname + " " + channel);
        }

        #endregion

        #region 3.2.8 Kick command

        public void Kick(string channel, string user)
        {
            m_client.WriteLine("KICK " + channel + " " + user);
        }

        public void Kick(string channel, string user, string comment)
        {
            m_client.WriteLine("KICK " + channel + " " + user + " :" + comment);
        }

        public void Kick(string[] channels, string[] users)
        {
            Kick(string.Join(",", channels), string.Join(",", users));
        }

        public void Kick(string[] channels, string[] users, string comment)
        {
            Kick(string.Join(",", channels), string.Join(",", users), comment);
        }

        #endregion // 3.2.8 Kick command

        #endregion

        #region 3.3 Sending messages
        public void PrivMsg(string target, string text)
        {
            m_client.WriteLine("PRIVMSG " + target + " :" + text);
        }

        public void Notice(string target, string text)
        {
            m_client.WriteLine("NOTICE " + target + " :" + text);
        }
        #endregion // 3.3 Sending messages

        #region 3.4 Server queries and commands

        public void Motd()
        {
            m_client.WriteLine("MOTD");
        }

        public void Motd(string target)
        {
            m_client.WriteLine("MOTD " + target);
        }

        public void Lusers(string mask, string target)
        {
            m_client.WriteLine("LUSERS " + mask + " " + target);
        }

        public void Version()
        {
            m_client.WriteLine("VERSION");
        }

        public void Version(string target)
        {
            m_client.WriteLine("VERSION " + target);
        }

        public void Stats(string query, string target)
        {
            m_client.WriteLine("START " + query + " " + target);
        }

        public void Links(string remote, string mask)
        {
            m_client.WriteLine("LINKS " + remote + " " + mask);
        }

        public void Time(string target)
        {
            m_client.WriteLine("TIME " + target);
        }

        public void Connect(string target, int port, string remote)
        {
            m_client.WriteLine("CONNECT " + target + " " + port + " " + remote);
        }

        public void Trace(string target)
        {
            m_client.WriteLine("TRACE " + target);
        }

        public void Admin(string target)
        {
            m_client.WriteLine("ADMIN " + target);
        }

        public void Info(string target)
        {
            m_client.WriteLine("INFO " + target);
        }

        #endregion // 3.4 Server queries and commands

        #region 3.5 Service Query and Commands
        #region 3.5.1 Serverlist
        public void Serverlist()
        {
            m_client.WriteLine("SERVERLIST");
        }
        public void Serverlist(string mask)
        {
            m_client.WriteLine("SERVERLIST " + mask);
        }
        public void Serverlist(string mask, string type)
        {
            m_client.WriteLine("SERVERLIST " + mask + " " + type);
        }
        #endregion // 3.5.1 Serverlist

        #region 3.5.2 Squery
        public void Squery(string serviceName, string text)
        {
            m_client.WriteLine("SQUERY " + serviceName + " :" + text);
        }
        #endregion // 3.5.2 Squery
        #endregion // 3.5 Service Query and Commands

        #region 3.6 User based queries

        #region 3.6.1 Who query
        public void Who()
        {
            m_client.WriteLine("WHO");
        }
        public void Who(string mask)
        {
            m_client.WriteLine("WHO " + mask);
        }
        public void Who(string mask, bool isOperator)
        {
            m_client.WriteLine("WHO " + mask + ": o");
        }
        #endregion // 3.6.1 Who query

        #region 3.6.2 Whois query
        public void Whois(string target)
        {
            m_client.WriteLine("WHOIS " + target);
        }

        public void Whois(string target, string mask)
        {
            m_client.WriteLine("WHOIS " + target + " " + mask);
        }
        #endregion // 3.6.2 Whois query

        #region 3.6.3 Whowas

        public void Whowas(string nickname)
        {
            m_client.WriteLine("WHOWAS " + nickname);
        }

        public void Whowas(string nickname, int count)
        {
            m_client.WriteLine("WHOWAS " + nickname + " " + count);
        }

        public void Whowas(string nickname, int count, string target)
        {
            m_client.WriteLine("WHOWAS " + nickname + " " + count + " " + target);
        }

        #endregion // 3.6.3 Whowas

        #endregion // 3.6 User based queries

        #region 3.7 Miscellaneous messages

        #region 3.7.1 Kill message

        public void Kill(string nickname, string comment)
        {
            m_client.WriteLine("KILL " + nickname + " :" + comment);
        }

        #endregion // 3.7.1 Kill message

        #region 3.7.2 Ping message

        public void Ping(string server)
        {
            m_client.WriteLine("PING " + server);
        }

        public void Ping(string server1, string server2)
        {
            m_client.WriteLine("PING " + server1 + " " + server2);
        }

        #endregion // 3.7.2 Ping message

        #region 3.7.3 Pong message

        public void Pong(string server)
        {
            m_client.WriteLine("PONG " + server);
        }

        public void Pong(string server1, string server2)
        {
            m_client.WriteLine("PONG " + server1 + " " + server2);
        }

        #endregion // 3.7.3 Pong message

        #region 3.7.4 Error
        public void Error(string error)
        {
            m_client.WriteLine("ERROR :" + error);
        }
        #endregion // 3.7.4 Error

        #endregion // 3.7 Miscellaneous messages

        #region 4. Optional features

        #region 4.1 Away

        public void Away()
        {
            m_client.WriteLine("AWAY");
        }

        public void Away(string text)
        {
            m_client.WriteLine("AWAY :" + text);
        }

        #endregion // 4.1 Away

        #region 4.2 Rehash message

        public void Rehash()
        {
            m_client.WriteLine("REHASH");
        }

        #endregion // 4.2 Rehash message

        #region 4.3 Die message

        public void Die()
        {
            m_client.WriteLine("DIE");
        }

        #endregion // 4.2 Die message

        #region 4.4 Restart message

        public void Restart()
        {
            m_client.WriteLine("RESTART");
        }

        #endregion // 4.4 Restart message

        #region 4.5 Summon message

        public void Summon(string user)
        {
            m_client.WriteLine("SUMMON " + user);
        }

        public void Summon(string user, string target)
        {
            m_client.WriteLine("SUMMON " + user + " " + target);
        }

        public void Summon(string user, string target, string channel)
        {
            m_client.WriteLine("SUMMON " + user + " " + target + " " + channel);
        }

        #endregion // 4.5 Summon message

        #region 4.6 Users

        public void Users()
        {
            m_client.WriteLine("USERS");
        }

        public void Users(string target)
        {
            m_client.WriteLine("USERS " + target);
        }

        #endregion // 4.6 Users

        #region 4.7 Operwall message

        public void Wallops(string text)
        {
            m_client.WriteLine("WALLOPS :" + text);
        }

        #endregion // 4.7 Operwall message

        #region 4.8 Userhost message

        public void Userhost(string nickname)
        {
            m_client.WriteLine("USERHOST " + nickname);
        }

        public void Userhost(string[] nicknames)
        {
            Userhost(string.Join(" ", nicknames));
        }

        #endregion // 4.8 Userhost message

        #region 4.9 Ison message

        public void Ison(string nickname)
        {
            m_client.WriteLine("ISON " + nickname);
        }

        public void Ison(string[] nicknames)
        {
            Ison(string.Join(" ", nicknames));
        }

        #endregion // 4.9 Ison message

        #endregion // 4. Optional features
    }
}
