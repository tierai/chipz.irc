using System;
using System.Diagnostics;
using System.Collections.Generic;
using Chipz.IRC.Collections;

namespace Chipz.IRC
{
	/// <summary>
    /// IrcUser.
	/// </summary>
    public sealed class IrcUser
    {
    	public object SyncRoot
    	{
    		get { return Client.SyncRoot; }
    	}
    
        /// <summary>
        /// Gets the IrcClient that created this user.
        /// </summary>
        public Chipz.IRC.IrcClient Client
        {
            get { return m_client; }
        }

        /// <summary>
        /// Gets the nickname.
        /// </summary>
        public string Nick
        {
            get { return m_nick; }
            internal set { m_nick = value; UpdateHostmask(); }
        }

        /// <summary>
        /// Gets the ident (username).
        /// </summary>
        public string Ident
        {
            get { return m_ident; }
        }

        /// <summary>
        /// Gets the hostname.
        /// </summary>
        public string Host
        {
            get { return m_host; }
        }

        /// <summary>
        /// Gets the hostmask (nick!ident@host).
        /// </summary>
        public string Hostmask
        {
            get { return m_hostmask; }
        }
        string m_hostmask;

        /// <summary>
        /// Returns true if this user is synced (host info is set).
        /// </summary>
        public bool IsSynced
        {
            get { return m_isSynced; }
        }

        /// <summary>
        /// Gets user mode.
        /// </summary>
        public string Mode
        {
            get { return m_mode; }
            internal set { m_mode = value; }
        }

        /// <summary>
        /// Gets a list of known channels that this user is on.
        /// </summary>
        public IrcChannel[] Channels
        {
            get
            {
            	lock(SyncRoot)
            	{
            		IrcChannel[] result = new IrcChannel[m_channels.Count];
            		m_channels.Values.CopyTo(result, 0);
            		return result;
            	}
            }
        }
        Dictionary<string, IrcChannel> m_channels = new Dictionary<string, IrcChannel>(StringComparer.InvariantCultureIgnoreCase);

        IrcClient m_client;
        int m_refCount = 0;
        string m_nick;
        string m_ident;
        string m_host;
        bool m_isSynced;
        string m_mode = string.Empty;

        internal IrcUser(IrcClient client, string nick) : this(client, nick, null, null)
        {
        }

        internal IrcUser(IrcClient client, string nick, string ident, string host)
        {
        	Debug.Assert(client != null);
        	Debug.Assert(nick != null);
        	
            m_client = client;
            m_nick = nick.Trim(client.ChannelUserPrefixes);
            
            if(ident != null)
            {
            	Debug.Assert(host != null);
            	Sync(ident, host);
            }
            else
            {
            	Debug.Assert(host == null);
            	m_isSynced = false;
            	UpdateHostmask();
            }
        }
        
        // TODO: How to deal with unsynced users?
        internal void UpdateHostmask()
        {
        	if(m_isSynced)
        		m_hostmask = m_nick + "!" + m_ident + "@" + m_host;
        	else
            	m_hostmask = m_nick + "-is!not@synced";
        }
        
        internal void Sync(string ident, string host)
        {
        	Debug.Assert(ident != null);
            Debug.Assert(host != null);
        	m_ident = ident;
        	m_host = host;
        	m_isSynced = true;
        	UpdateHostmask();
        }
        
        internal void Clear()
        {
        	foreach(IrcChannel channel in m_channels.Values)
        	{
        		channel.RemoveUser(m_nick);
        	}
        	m_channels.Clear();
        }
        
        internal void Rename(string oldNick, string newNick)
        {
        	m_nick = newNick;
        	
        	foreach(IrcChannel channel in m_channels.Values)
        	{
        		channel.RenameUser(oldNick, newNick);
        	}
        }
        
        internal void AddChannel(IrcChannel channel)
        {
        	m_channels[channel.Name] = channel;
        }
        
        internal bool RemoveChannel(IrcChannel channel)
        {
        	return m_channels.Remove(channel.Name);
        }

		// TODO: Remove and replace with some garbage collector that removes inactive users (remove user if channels == 0 && inactive for X hours)
        internal void AddRef()
        {
            m_refCount++;
        }

        internal void Release()
        {
            if (m_refCount > 0)
                m_refCount--;
            if (m_refCount == 0)
                Destroy();
        }

        /// <summary>
        /// Destroy this user, called when this user isn't in any channels that you are in.
        /// </summary>
        internal void Destroy()
        {
            m_client.IrcUsers.Remove(this.Nick);
        }
    }
}
