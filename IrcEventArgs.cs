﻿using System;

namespace Chipz.IRC
{
	/// <summary>
    /// IrcEventArgs.
    /// </summary>
    public class IrcEventArgs : EventArgs, IIrcEventArgs
    {
    	#region IIrcEventArgs
    	
        /// <summary>
        /// Gets the MessageData.
        /// </summary>
        public MessageData Data { get { return m_data; } }
        protected MessageData m_data;

        /// <summary>
        /// Gets the IrcClient that created this event.
        /// </summary>
        public IrcClient IrcClient { get { return m_data.IrcClient; } }

		#endregion // IIrcEventArgs
		
        /// <summary>
        /// Creates a new instance of IrcEventArgs.
        /// </summary>
        public IrcEventArgs()
        {
        }
        
        /// <summary>
        /// Creates a new instance of IrcEventArgs. 
        /// </summary>
        /// <param name="data">MessageData.</param>
        public IrcEventArgs(MessageData data)
        {
            m_data = data;
        }
    }
}
