using System;

namespace Chipz.IRC
{
    public class AlreadyConnectedException : Exception
	{
		public AlreadyConnectedException() : base("Already connected") { }
	}

    public class NotConnectedException : Exception
	{
		public NotConnectedException() : base("Not connected") { }
	}

	public class CouldNotConnectException : Exception
	{
        public CouldNotConnectException(string message) : base(message) { }
        public CouldNotConnectException(string message, Exception inner) : base(message, inner) { }
	}
}
