using System;

namespace Chipz.IRC
{
	public enum MessageType
	{
		None = 0,
		BanList,
		Error,
		Join,
		Kick,
		Message,
		Mode,
		Names,
		Nick,
		Notice,
		Part,
		Ping,
		Pong,
		Quit,
		Topic,
		Unknown,
		Welcome,
		Who,
	}
}

