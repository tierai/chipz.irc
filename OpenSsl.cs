using System;
using System.IO;
using System.Text;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;

namespace Chipz.OpenSsl
{
    /// <summary>
    /// Very basic OpenSSL wrapper for the servers that are using some funny OpenSSL version that doest't work with the SslStream.
    /// TODO: Support for local certificates & callbacks.
    /// </summary>
    public class OpenSslStream : Stream
    {
    	#region Public properties
    	
        /// <summary>
        /// Returns true if this is a client stream.
        /// </summary>
        public bool IsClient
        {
            get { return m_isClient; }
        }
        bool m_isClient;

        /// <summary>
        /// Returns the remote certificate.
        /// </summary>
        public X509Certificate RemoteCertificate
        {
            get { return m_remoteCertificate; }
        }
        X509Certificate m_remoteCertificate;
        
        #endregion // Public properties
        
        #region Private
        
        Socket m_socket;
        IntPtr m_ssl;
        IntPtr m_ctx;

		#endregion // Private
        
        #region Public
        
        /// <summary>
        /// Creates a new instance of the OpenSslStream class.
        /// </summary>
        /// <param name="socket">Socket should be a valid System.Net.Sockets.Socket object.</param>
        /// <param name="protocol">Protocol to use, note that mixed protocols aren't supported.</param>
        /// <param name="isClient">Specifies wether this is a client or server stream.</param>
        public OpenSslStream(Socket socket, SslProtocols protocol, bool isClient)
        {
            if (socket == null)
                throw new ArgumentNullException("socket");

            API.Initialize();

            m_socket = socket;
            m_isClient = isClient;

            IntPtr method = IntPtr.Zero;

            if (isClient)
            {
                if ((protocol & SslProtocols.Default) == SslProtocols.Default)
                    method = API.SSLv23_client_method();
                else if ((protocol & SslProtocols.Tls) == SslProtocols.Tls)
                    method = API.TLSv1_client_method();
                else if ((protocol & SslProtocols.Ssl3) == SslProtocols.Ssl3)
                    method = API.SSLv3_client_method();
                else if ((protocol & SslProtocols.Ssl2) == SslProtocols.Ssl2)
                    method = API.SSLv2_client_method();
                else
                    method = API.SSLv23_client_method();
            }
            else
            {
                if ((protocol & SslProtocols.Default) == SslProtocols.Default)
                    method = API.SSLv23_server_method();
                else if ((protocol & SslProtocols.Tls) == SslProtocols.Tls)
                    method = API.TLSv1_server_method();
                else if ((protocol & SslProtocols.Ssl3) == SslProtocols.Ssl3)
                    method = API.SSLv3_server_method();
                else if ((protocol & SslProtocols.Ssl2) == SslProtocols.Ssl2)
                    method = API.SSLv2_server_method();
                else
                    method = API.SSLv23_server_method();
            }

            if (method == IntPtr.Zero)
                throw new ArgumentException("failed to find a suitable method for protocol: " + protocol.ToString(), "protocol");

            /// Create a new OpenSSL context.
            m_ctx = API.SSL_CTX_new(method);
        }

        /// <summary>
        /// Attempts to authenticate with the server.
        /// </summary>
        public virtual void Authenticate()
        {
            if (m_ssl != IntPtr.Zero)
                throw new InvalidOperationException("m_ssl already initialized");

            if (m_socket == null)
                throw new InvalidOperationException("m_socket cannot be null");

            if (m_ctx == IntPtr.Zero)
                throw new InvalidOperationException("m_ctx cannot be null");

            /// Get socket handle
            int fd = m_socket.Handle.ToInt32();

            if (fd == -1)
                throw new ArgumentException("invalid socket handle");

            /// Create a new ssl structure.
            m_ssl = API.SSL_new(m_ctx);

            if (m_ssl == IntPtr.Zero)
                throw new NotSupportedException("m_ssl == zero");

            /// Create socket BIO.
            IntPtr bio = API.BIO_new_socket(fd, 0);
            if (bio == IntPtr.Zero)
                throw new NotSupportedException("bio == zero");

            /// Select bio.
            API.SSL_set_bio(m_ssl, bio, bio);

            /// Connect to the server or accept an incoming connection.
            int result = IsClient ? API.SSL_connect(m_ssl) : API.SSL_accept(m_ssl);

            if (result != 1)
                throw new AuthenticationException();

            /// Get the remote certificate.
            IntPtr cert = API.SSL_get_peer_certificate(m_ssl);
            m_remoteCertificate = null;

            /// Copy certificate data into a X509Certificate object.
            if (cert != IntPtr.Zero)
            {
                int length = API.i2d_X509(cert, IntPtr.Zero);

                if (length > 0)
                {
                    IntPtr tmp = Marshal.AllocHGlobal(length);
                    IntPtr tmp2 = tmp;
                    API.i2d_X509(cert, ref tmp2);
                    byte[] bytes = new byte[length];
                    Marshal.Copy(tmp, bytes, 0, length);
                    Marshal.FreeHGlobal(tmp);

                    m_remoteCertificate = new X509Certificate(bytes);
                }

                API.X509_free(cert);
            }
        }
        
        #endregion // Public

		#region Stream overrides

        public override bool CanRead
        {
            get { return true; }
        }

        public override bool CanSeek
        {
            get { return false; }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {
        }

        public override long Length
        {
            get { throw new NotSupportedException(); }
        }

        public override long Position
        {
            get { throw new NotSupportedException(); }
            set { throw new NotSupportedException(); }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            if (m_ssl == IntPtr.Zero)
                throw new InvalidOperationException("m_ssl cannot be null");

            if (offset == 0)
                return API.SSL_read(m_ssl, buffer, count);

            byte[] tmp = new byte[count];

            int result = API.SSL_read(m_ssl, tmp, count);

            if(result > 0)
                Array.Copy(tmp, 0, buffer, offset, result);

            return result;
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException();
        }

        public override void SetLength(long value)
        {
            throw new NotSupportedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            if (m_ssl == IntPtr.Zero)
                throw new InvalidOperationException("m_ssl cannot be null");

            if (offset == 0)
                API.SSL_write(m_ssl, buffer, count);

            byte[] tmp = new byte[count];
            Array.Copy(buffer, offset, tmp, 0, count);
            API.SSL_write(m_ssl, tmp, count);
        }
        
        #endregion // Stream overrides
        
        #region IDisposable
        
		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
		            if (m_ssl != IntPtr.Zero)
		            {
		                API.SSL_free(m_ssl);
		                m_ssl = IntPtr.Zero;
		            }
		            if (m_ctx != IntPtr.Zero)
		            {
		                API.SSL_CTX_free(m_ctx);
		                m_ctx = IntPtr.Zero;
		            }
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}
		
		#endregion // IDisposable
    }
}
