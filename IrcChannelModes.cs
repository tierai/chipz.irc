﻿using System;
using System.Text;
using System.Collections.Generic;
using Chipz.IRC.Collections;

namespace Chipz.IRC
{
	/// <summary>
	/// Some common channel modes.
	/// FIXME: Are all these standard?
	/// </summary>
	public class IrcChannelModes
	{
	    /// <summary>
	    /// Ban.
	    /// </summary>
	    public const char Ban = 'b';
	
	    /// <summary>
	    /// Key.
	    /// </summary>
	    public const char Key = 'k';
	
	    /// <summary>
	    /// Userlimit.
	    /// </summary>
	    public const char UserLimit = 'l';
	
	    /// <summary>
	    /// Moderated.
	    /// </summary>
	    public const char Moderated = 'm';
	
	    /// <summary>
	    /// Only operators can set topic.
	    /// </summary>
	    public const char OpsSetTopic = 't';
	
	    /// <summary>
	    /// Invite.
	    /// </summary>
	    public const char Invite = 'i';
	
	    /// <summary>
	    /// Private.
	    /// </summary>
	    public const char Private = 'p';
	
	    /// <summary>
	    /// Secret.
	    /// </summary>
	    public const char Secret = 's';
	
	    /// <summary>
	    /// No external messages.
	    /// </summary>
	    public const char NoExternalMessages = 'n';
	}
}
