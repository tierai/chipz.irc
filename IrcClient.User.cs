﻿using System;
using System.Diagnostics;

namespace Chipz.IRC
{
	/// <summary>
	/// User related functions
	/// </summary>
	public partial class IrcClient
	{
		protected virtual IrcUser GetUser(string nick, MessageData data)
		{
			Debug.Assert(nick != null);
			Debug.Assert(data != null);
		
            IrcUser user = GetUser(nick);
            
            if(user == null)
            {
                user = new IrcUser(this, data.Nick, data.Ident, data.Host);
               	AddUser(user);
            }
            else if(!user.IsSynced)
            {
            	user.Sync(data.Ident, data.Host);
            }
            
            return user;
		}
	
        protected virtual void AddUser(IrcUser user)
        {
        	m_ircUsers.Add(user.Nick, user);
        }
        
        protected virtual bool RemoveUser(string name)
        {
        	return m_ircUsers.Remove(name);
        }
        
        protected virtual void RenameUser(string oldNick, string newNick)
        {
        	Debug.Assert(oldNick != null);
        	Debug.Assert(newNick != null);
        
            IrcUser user = null;
            
            if(m_ircUsers.TryGetValue(oldNick, out user))
            {
            	user.Rename(oldNick, newNick);
            }
            else
            {
                Trace("Cannot find IrcUser: " + oldNick + " (new: " + newNick + ")");        
            }
            
            // TODO: Have an IrcUser that's is Me?
            if(IsMe(oldNick))
            {
                m_nickname = newNick;
            }
        }
	}
}
