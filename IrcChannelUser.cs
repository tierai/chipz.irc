using System;
using System.Collections.Generic;

namespace Chipz.IRC
{
	/// <summary>
	/// Summary description for ChannelUser.
	/// </summary>
    public sealed class IrcChannelUser : IComparable
    {
        /// <summary>
        /// IrcUser associated with this user.
        /// </summary>
        public IrcUser IrcUser
        {
            get { return m_ircUser; }
            private set { m_ircUser = value; }
        }
        IrcUser m_ircUser;

        /// <summary>
        /// IrcChannel associated with this user.
        /// </summary>
        public IrcChannel Channel
        {
            get { return m_channel; }
            private set {
            	m_channel = value;
            }
        }
        IrcChannel m_channel;

        /// <summary>
        /// Nickname.
        /// TODO: Name?
        /// </summary>
        public string Nick
        {
            get { return IrcUser.Nick; }
        }

        /// <summary>
        /// True is user is channel-operator.
        /// </summary>
        public bool IsOp
        {
            get { return Mode.IndexOf('o') > -1; }
        }

        /// <summary>
        /// True is user is half-op.
        /// </summary>
        public bool IsHalfOp
        {
            get { return Mode.IndexOf('h') > -1; }
        }

        /// <summary>
        /// True is user has voice.
        /// </summary>
        public bool IsVoice
        {
            get { return Mode.IndexOf('v') > -1; }
        }

        /// <summary>
        /// User channel-mode. (o=op, v=voice...)
        /// </summary>
        public string Mode
        {
            get { return m_mode; }
            private set { m_mode = value; }
        }
        string m_mode;

        /// <summary>
        /// IrcClient.
        /// </summary>
        public IrcClient IrcClient
        {
            get { return IrcUser.Client; }
        }

        /// <summary>
        /// Checks wether a mode is set.
        /// </summary>
        /// <param name="mode">Mode to check.</param>
        /// <returns>True if the mode is set.</returns>
        public bool IsModeSet(char mode)
        {
            return m_mode.IndexOf(mode) > -1;
        }

        /// <summary>
        /// User mode-prefix. (@, +, ...)
        /// </summary>
        public string Prefix
        {
            get
            {
                lock (m_ircUser.Client.m_channelUserPrefixes.SyncRoot)
                {
                    foreach (KeyValuePair<char, char> kv in m_ircUser.Client.m_channelUserPrefixes)
                    {
                        if (m_mode.IndexOf(kv.Key) > -1)
                            return kv.Value.ToString();
                    }
                }
                return string.Empty;
            }
        }

        internal void SetModeEnabled(string modes, bool enabled)
        {
            foreach (KeyValuePair<char, char> kv in m_ircUser.Client.m_channelUserPrefixes)
            {
                modes = modes.Replace(kv.Value, kv.Key);
            }
            
            foreach(char mode in modes)
            {
                if(enabled)
                {
                    if(m_mode.IndexOf(mode) < 0)
                        m_mode += mode;
                }
                else
                    m_mode = m_mode.Replace(mode.ToString(), string.Empty);
            }
        }

        internal IrcChannelUser(IrcChannel channel, IrcUser user, string mode)
        {
            foreach(KeyValuePair<char, char> kv in user.Client.m_channelUserPrefixes)
            {
            	mode = mode.Replace(kv.Value, kv.Key);
            }
            
            Channel = channel;
        	IrcUser = user;
        	Mode = mode;
        }

        /// <summary>
        /// Destroy the user, called when this user is not longer in the channel.
        /// </summary>
        internal void Destroy()
        {
            m_ircUser.Release();
        }

        /// <summary>
        /// returns Prefix + Nick.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Prefix + Nick;
        }

        /// <summary>
        /// Compares this object to another.
        /// </summary>
        /// <param name="obj">Object.</param>
        /// <returns>Int.</returns>
        public int CompareTo(object obj)
        {
            char[] prefixes = m_ircUser.Client.ChannelUserPrefixes;

            string a = AddWhitespace(this.ToString(), prefixes);
            string b = AddWhitespace(obj.ToString(), prefixes);
            
            return string.Compare(a, b);
        }

        // Ugly compare-hack. 
        static string AddWhitespace(string s, char[] prefixes)
        {
            if (s == null || s.Length < 1)
                return s;

            for (int i = 0; i < prefixes.Length; ++i)
            {
                char p = prefixes[i];
                if (s[0] == p)
                {
                    string tmp = new string(' ', prefixes.Length - i + 1);
                    return tmp + s;
                }
            }

            return s;
        }
    }
}
